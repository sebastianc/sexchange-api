<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->longText('description');
            $table->integer('seller')->unsigned();
            $table->foreign('seller')->references('id')->on('users');
            $table->integer('size')->unsigned();
            $table->foreign('size')->references('id')->on('shoe_sizes');
            $table->integer('cost');
            $table->integer('condition');
            $table->string('delivery');
            $table->boolean('visibility');
            $table->boolean('sold');
            $table->boolean('marked_as_sold');
            $table->boolean('wanted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shoes');
    }
}
