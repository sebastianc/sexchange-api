<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFromStripeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stripe_transactions', function (Blueprint $table) {
            $table->dropForeign('stripe_transactions_recipient_id_foreign');
            $table->dropColumn('amount');
            $table->dropColumn('recipient_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripe_transactions', function (Blueprint $table) {
            //
        });
    }
}
