<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function (Blueprint $table) {
            //Drop
            $table->dropForeign('notifications_user_id_foreign');
            $table->dropForeign('notifications_sender_id_foreign');
            //Add back with cascade
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');

        });

        Schema::table('purchases', function (Blueprint $table) {
            //Drop
            $table->dropForeign('purchases_buyer_id_foreign');
            $table->dropForeign('purchases_shoe_id_foreign');
            //Add back with cascade
            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('shoe_id')->references('id')->on('shoes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
