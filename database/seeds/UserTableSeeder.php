<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'test@test.com',
            'username' => 'username1',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'email' => 'test2@test.com',
            'username' => 'username2',
            'password' => bcrypt('password'),
        ]);
    }
}
