<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);

        $this->call(BrandTableSeeder::class);
        $this->call(BrandTableSeeder::class);
        $this->call(BrandTableSeeder::class);

        factory(App\Color::class, 100)->create();
        factory(App\ShoeSize::class, 20)->create();
        factory(App\Shoe::class, 100)->create();
    }
}
