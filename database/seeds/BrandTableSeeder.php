<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'name' => 'Brand-'.str_random(10),
            'slug' => 'Slug-'.str_random(10),
            'image' => '/img',
        ]);
    }
}
