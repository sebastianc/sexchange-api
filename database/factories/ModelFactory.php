<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//$factory->define(App\User::class, function (Faker\Generator $faker) {
$factory->define(App\ShoeSize::class, function ($faker) {
    return [
        'gender' => $faker->randomElement(['Mens', 'Womans', 'Junior']),
        'size' => 'UK ' . mt_rand(3, 12)
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email(),
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'image' => '/images/original/nmd.jpg',
        'username' => $faker->userName(),

    ];
});
$factory->define(App\Shoe::class, function ($faker) {
    return [
        'name' => $faker->text(mt_rand(5, 15)),
        'slug' => $faker->slug(),
        'description' => $faker->sentence(mt_rand(4, 8)),
        'cost' => $faker->randomFloat(50, 300),
        'condition' => mt_rand(1, 5),
        'delivery' => mt_rand(1, 5),
        'size' => mt_rand(1, 20),
        'seller' => 2,
        'brand' => mt_rand(1, 3),
        'sold' => 0,
        // 'view_count' => 0
    ];
});

$factory->define(App\ShoeImage::class, function (Faker\Generator $faker) {
    return [
        'shoe_id' => function () {
            return factory(App\Shoe::class)->create()->id;
        },
        'base_url' => 'local.se.uk',
        'url' => '#',
        'public_url' => 'images/original/nmd.jpg',
        'original_filename' => 'img.jpg'

    ];
});

$factory->define(App\Color::class, function ($faker) {
   return [
       'name' => $faker->colorName()
   ];
});


$factory->define(App\Notification::class, function ($faker) {
    return [
        'user_id' => mt_rand(1,3),
        'notification_id' => '1',
        'notification_type' => 'Comment',
        'sender_id' => mt_rand(1,3)
    ];
});

$factory->define(App\Comment::class, function ($faker) {
    return [
        'text' => $faker->sentence(mt_rand(4, 8)),
        'user_id' => mt_rand(1,3),
        'shoe_id' => mt_rand(1,30),
    ];
});

$factory->define(App\Follow::class, function ($faker) {
    return [
        'user_id' => mt_rand(1,3),
        'follows' => mt_rand(1,3),
    ];
});

$factory->define(App\Purchase::class, function ($faker) {
    return [
        'shoe_id' => mt_rand(1,30),
        'buyer_id' => mt_rand(1,3),
        'payment_sent' => 1,
        'status' => 'pending',
        'price' => 10.00,
    ];
});

$factory->define(App\ShoeOffer::class, function ($faker) {
    return [
        'shoe_id' => mt_rand(1,30),
        'user_id' => mt_rand(1,3),
        'amount' => mt_rand(12,90),
        'accepted' => 0,
    ];
});

