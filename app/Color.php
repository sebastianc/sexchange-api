<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['name'];

    public function hasShoe() {
        return $this->belongsToMany('colors_shoes', 'color_id', 'shoe_id');
    }
}
