<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/02/16
 * Time: 17:22
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ShoeLike extends Model
{
    protected $with = ['shoe', 'shoeWeb'];

    /*
     * Morph notification type, to a notification.
     */
    public function notification ()
    {
        return $this->morphMany('Notification', 'notification');
    }

    function user(){
        return $this->belongsTo('App\User');
    }
    function shoe(){
        return $this->belongsTo('App\Shoe')->where('sold', 0)->where('marked_as_sold', 0);
    }

    function shoeWeb(){
        return $this->belongsTo('App\Shoe', 'shoe_id');
    }
}