<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 12:18
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    function shoe(){
        return $this->belongsTo('App\Shoe', 'shoe_id')->with('images');
    }


    /*
     * Morph notification type, to a notification.
     */
    public function notification ()
    {
        return $this->morphMany('Notification', 'notification');
    }
}