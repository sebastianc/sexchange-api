<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/02/16
 * Time: 16:36
 */

namespace App;


use App\Traits\Sharable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class Shoe extends Model
{
    use SoftDeletes;
    use Sharable;

    protected $with = ['colors', 'sizes', 'brand_info', 'images', 'seller', 'acceptedOffer', 'reviewByBuyer', 'reviewBySeller'];
    protected $appends = ['comment_count', 'like_count', 'views'];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();

        if(isset($array['review_by_buyer'])){
            if(count($array['review_by_buyer'])){
                $array['review_by_buyer'] = true;
            }else{
                $array['review_by_buyer'] = false;
            }
        }else{
            $array['review_by_buyer'] = false;
        }
        if(isset($array['review_by_seller'])){
            if(count($array['review_by_seller'])){
                $array['review_by_seller'] = true;
            }else{
                $array['review_by_seller'] = false;
            }
        }else{
            $array['review_by_seller'] = false;
        }
        if(isset($array['accepted_offer'])){
            if($array['accepted_offer']){
                $array['cost'] = $array['accepted_offer']['amount'];
                $array['accepted_offer'] = true;
            }else{
                $array['accepted_offer'] = false;
            }
        }else{
            $array['accepted_offer'] = false;
        }

        if(isset($array['offers'])){
            $array['offers'] = count($array['offers']);
        }
        if(isset($array['liked_by'])){
            if($array['liked_by']){
                $array['liked_by'] = true;
            }else{
                $array['liked_by'] = false;
            }
         }

        if(isset($array['images'])){
            foreach($array['images'] as $index => $image){
                $url = $image['base_url'].'/'.$image['public_url'];
                $dimensions = [
                    'thumb' => str_replace('/original/', '/medium/', $url),
                    'large' => str_replace('/original/', '/ios/', $url),
                    'full' => $url,
                ];
                $array['images'][$index]['dimensions'] = $dimensions;
            }
        }
        return $array;
    }


    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 10,
        'brand_info.name' => 8,
        'description' => 2,
        'colors.name' => 10,
    ];

    protected $fillable = ['color', 'view_count'];

    function likes(){
        return $this->hasMany('App\ShoeLike')->with('user');
    }

    function likedBy(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasMany('App\ShoeLike')->where('user_id', $userId);
    }

    function comments(){
        return $this->hasMany('App\Comment')->with('user')->orderBy('created_at', 'desc')->take(2);
    }

    function webComments(){
        return $this->hasMany('App\Comment')->with('user')->orderBy('created_at', 'desc')->take(10);
    }


    function commentCount(){
        return $this->hasMany('App\Comment');
    }

    function brand_info(){
        return $this->belongsTo('App\Brand', 'brand');
    }

    function images()
    {
        return $this->hasMany('App\ShoeImage', 'shoe_id')->orderBy('created_at', 'asc');
    }

    function seller()
    {
        return $this->belongsTo('App\UserSearchable', 'seller', 'id');
    }

    function sellerUser()
    {
        return $this->seller();
    }

    function colors() {
        return $this->belongsToMany('App\Color', 'colors_shoes', 'shoe_id', 'color_id');
    }

    function sizes() {
        return $this->belongsTo('App\ShoeSize', 'size');
    }
    function views() {
        return $this->hasMany('App\ShoeView');
    }
    function reviewByBuyer() {
        return $this->hasOne('App\ReviewIndicator')->where('is_seller', 0);
    }
    function reviewBySeller() {
        return $this->hasOne('App\ReviewIndicator')->where('is_seller', 1);
    }
    function purchase() {
        return $this->hasOne('App\Purchase', 'shoe_id')->with('user');
    }
    function offers(){
        return $this->hasMany('App\ShoeOffer')->with('user');
    }
    function offersByCurrent(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasMany('App\ShoeOffer')->where('user_id' , $userId);
    }
    function acceptedOffer(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasOne('App\ShoeOffer')->where('user_id', $userId)->where('accepted', 1)->where('updated_at', '>', Carbon::now()->subWeek());
    }

    public function getHasLikedAttribute()
    {
        if(Auth::check()){
            return $this->likedBy->count() > 0;
        }else{
            return false;
        }
    }

    public function getCommentCountAttribute()
    {
        return $this->commentCount()->count();
    }
    public function getLikeCountAttribute()
    {
        return $this->likes()->count();
    }
    public function getViewsAttribute()
    {
        return $this->views()->count();
    }

    public function setSlugAttribute($slug)
    {
        $count = Shoe::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        $this->attributes['slug'] =  $count ? "{$slug}-{$count}" : $slug;
    }

    public function getImage($i)
    {
        $i = (int) $i - 1;
        $images = $this->images()->get();
        if(isset($images[$i])){
            return $images[$i];
        }else{
            return null;
        }
    }

    public function belongsToCurrentUser(){
        if($user = Auth::user()){
            if($user->id == $this->seller){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }




}