<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 02/03/16
 * Time: 14:09
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ShoeOffer extends Model
{
    function shoe()
    {
        return $this->belongsTo('App\Shoe', 'shoe_id');
    }
    function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

}