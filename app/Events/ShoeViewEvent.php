<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 06/09/16
 * Time: 16:51
 */

namespace App\Events;



use App\Shoe;
use App\ShoeView;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;

class ShoeViewEvent extends Event
{

    public function __construct(Request $request, Shoe $shoe)
    {
        //TODO: Clear views after x minutes

        // Let Laravel inject the session Store instance,
        // and assign it to our $session variable.
        $viewed = $request->session()->get('viewed_shoes', []);
        // Check the viewed posts array for the existance
        // of the id of the post
        $recent =  array_key_exists($shoe->id, $viewed);

        if(!$recent){
            $user = Auth::user()? Auth::user()->id : NULL;
            $view = New ShoeView;
            $view->user_id = $user;
            $view->shoe_id = $shoe->id;
            $view->save();
            $request->session()->put('viewed_shoes.'.$shoe->id, time() );

        }
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}