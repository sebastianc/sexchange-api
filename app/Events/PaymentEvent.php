<?php

namespace App\Events;

use App\Shoe;
use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PaymentEvent extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $user;
    public $shoe;
    public $seller;
    public $token;
    public $offer;

    /**
     * Create a new event instance.
     *
     * @return User
     */
    public function __construct($user, $shoe, $sellerID, $token, $offer)
    {
        $this->user = $user;
        $this->shoe = $shoe;
        $this->seller = User::find($sellerID);
        $this->token = $token;
        $this->offer = $offer;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
