<?php

namespace App\Console\Commands;

use App\Purchase;
use App\Shoe;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearPurchases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:clear-purchases';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $purchases = Purchase::where('payment_sent', 0)->where('created_at', '<', Carbon::now()->subMinutes(15))->get();
        foreach($purchases as $purchase){
            $shoe = DB::table('shoes')->select('sold')->where('id', $purchase->shoe_id)->update(['sold' => 0]);
            $purchase->delete();
        }
    }
}
