<?php

namespace App\Console\Commands;

use App\Shoe;
use Illuminate\Console\Command;

class UpdateShares extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shares:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the facebook, twitter, gplus.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $closure = function ($items) {
            foreach ($items as $item) {
                $item->updateShares();
//                var_dump($item->toArray());
                $item->save();
                sleep(1);
            }
        };

        Shoe::chunk(50, $closure);
    }
}