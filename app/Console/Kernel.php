<?php

namespace App\Console;

use App\Http\Controllers\API\V1\PurchaseController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\ClearPurchases::class,
         Commands\UpdateShares::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Clear purchases
        $schedule->command('cron:clear-purchases')->everyMinute();
        $schedule->command('shares:update')->cron('0 */2 * * *');
    }
}
