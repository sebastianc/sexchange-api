<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class NotificationWeb extends Notification
{
    public function getMessageAttribute()
    {
        try {
            $type = $this->notification_type;

            $message = '<strong><a href="' . url('profile/' . $this->sender[0]->username) .
                '" class="black">' . $this->sender[0]->username . '</a></strong> ';


            $shoeLink = function (Shoe $shoe) {
                return '<strong><a href="' . url('shoes/' . $shoe->slug) .
                '" class="black">' . $shoe->name . '</a></strong>';
            };

            if ($type == 'Follow') {
                $user = User::find($this->notification_id);
                $message .= ' started following you';
                $image = $user->image;
            } elseif ($type == 'Purchase') {
                $purchase = Purchase::find($this->notification_id);
                $message .= 'purchased <a href="' . url('account/transactions/sold') . '" class="black">' . $purchase->shoe->name . '</a> for <span class="red">£'
                    . number_format($purchase->price, 2) . '</span>';
                $image = $purchase->shoe->images->first()->small();
            } elseif ($type == 'ShoeLike') {
                $shoe = ShoeLike::find($this->notification_id);
                $message .= 'liked your ' . $shoeLink($shoe->shoeWeb);
                $image = $shoe->shoeWeb->images->first()->small();
            } elseif ($type == 'Comment') {
                $comment = Comment::find($this->notification_id);
                $message .= ' commented on ' . $shoeLink($comment->shoe);
                $image = $comment->shoe->images->first()->small();
            } elseif ($type == 'Offer') {
                $offer = ShoeOffer::find($this->notification_id);
                $amount = '<span class="red">£' . number_format($offer->amount, 2) . '</span>';
                $message .= ' made an offer of ' . $amount . ' for <a href="' . url('account/offers/received') . '">' . $offer->shoe->name . '</a>';
                $image = $offer->shoe->images->first()->small();
            }  elseif ($type == 'OfferAccepted') {
                $offer = ShoeOffer::find($this->notification_id);
                $amount = '<span class="red">£' . number_format($offer->amount, 2) . '</span>';
                $message .= ' accepted your offer of ' . $amount . ' for <a href="' . url('account/offers/sent') . '">' . $offer->shoe->name . '</a>';
                $image = $offer->shoe->images->first()->small();
            }  elseif ($type == 'OfferDeclined') {
                $offer = ShoeOffer::find($this->notification_id);
                $amount = '<span class="red">£' . number_format($offer->amount, 2) . '</span>';
                $message .= ' declined your offer of ' . $amount . ' for <a href="' . url('account/offers/sent') . '">' . $offer->shoe->name . '</a>';
                $image = $offer->shoe->images->first()->small();
            } elseif ($type == 'Dispatched') {
                $purchase = Purchase::find($this->notification_id);
                $message .= ' dispatched <a href="' . url('account/transactions/purchases') . '">' . $purchase->shoe->name . '</a>';
                $image = $purchase->shoe->images->first()->small();
            } elseif ($type == 'Wanted') {
                $shoe = Shoe::find($this->notification_id);
                $message .= ' uploaded ' . $shoeLink($shoe) . ' which you wanted';
                $image = $shoe->images->first()->small();
            } elseif ($type == 'Review') {
                $review = Review::with('shoe')->find($this->notification_id);
                $type = Auth::user()->id == $review->seller_id ? 'sold' : 'purchased';
                $url = 'profile/' . Auth::user()->username . '/reviews/' . $type;
                $message .= ' reviewed your ' . $type . ' shoe <a href="' . url($url) . '">' . $review->shoe->name . '</a>';
                $image = $review->shoe->images->first()->small();
            } else {
                $message .= ' sent you a notification';
                $image = '';
            }

            $message .= '.';

            return ['message' => $message, 'image' => $image];
        } catch (\Exception $ex) {
            return ['message' => '', 'image' => ''];
        }
    }

    public function getIconAttribute()
    {
        switch ($this->notification_type) {
            case 'Comment':
                return url('assets/images/notifications_comment.png');
            case 'ShoeLike':
                return url('assets/images/notifications_like.png');
        }

        return url('assets/images/notifications_shoe.png');
    }

    public function getImageClassAttribute()
    {
        if ($this->notification_type == 'Follow') {
            return 'notification-image notification-profile-image';
        }

        return 'notification-image';
    }

    public function getTimeAgoAttribute()
    {
        //If today
        if ($this->created_at->diffInDays(Carbon::tomorrow()) == 0) {
            return 'Today at ' . $this->created_at->format('H:i');
        }

        //If yesterday
        if ($this->created_at->diffInDays(Carbon::tomorrow()) == 1) {
            return 'Yesterday at ' . $this->created_at->format('H:i');
        }

        //If less than a week ago
        if ($this->created_at->diffInHours(Carbon::now()) < (60 * 24 * 7)) {
            return $this->created_at->format('l \a\\t H:i');
        }

        return $this->created_at->format('d/m/y \a\\t H:i');
    }
}
