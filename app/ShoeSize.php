<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoeSize extends Model
{
    protected $table = 'shoe_sizes';
    protected $appends = ['description'];

    public function getDescriptionAttribute()
    {
        //Removed if statement for gender, correct gender string will be set in DB
        return $this->gender .' '. $this->size;
    }

}
