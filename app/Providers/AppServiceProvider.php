<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Relation::morphMap([
            'comment' => App\Comment::class,
            'ShowLike' => App\ShowLike::class,
            'Follow' => App\Follow::class,
            'Purchase' => App\Purchase::class,
        ]);
    }
}
