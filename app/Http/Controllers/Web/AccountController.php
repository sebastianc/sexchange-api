<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 04/08/16
 * Time: 15:59
 */

namespace App\Http\Controllers\Web;


use App\Dispute;
use App\Jobs\SendAdminDisputeEmail;
use App\Purchase;
use App\Review;
use App\Shoe;
use App\ShoeImage;
use App\ShoeOffer;
use App\UserSearchable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Namshi\JOSE\SimpleJWS;
use App\Http\Controllers\API\V1\NotificationController;


class AccountController extends BaseController
{
    public function index(Request $request)
    {
        if ($request->has('account_created')) {
            return redirect('account/listings')->with('success_message', 'Your account has been created.');
        }

        return redirect('account/listings');
    }

    public function listings()
    {
        $data['user'] = UserSearchable::with(['listings', 'likes', 'wanted'])->findOrFail(Auth::user()->id);
        $data['listings'] = $data['user']->listings()->paginate(16);
        $data['type'] = 'listings';
        
        return view('pages.account.listings', $data);
    }

    public function likes()
    {
        $data['user'] = UserSearchable::with(['listings', 'likes', 'wanted'])->findOrFail(Auth::user()->id);
        $data['likedShoes'] = $data['user']->likes()->paginate(16);
        $data['type'] = 'likes';
        return view('pages.account.likes', $data);
    }

    public function wanted()
    {
        $data['user'] = UserSearchable::with(['listings', 'likes', 'wanted'])->findOrFail(Auth::user()->id);
        $data['wantedShoes'] = $data['user']->wanted()->paginate(16);
        $data['type'] = 'wanted';
        return view('pages.account.wanted', $data);
    }

    public function complete()
    {
        return view('pages.account-complete')->with('user', Auth::user());
    }

    /**
     * View a list of purchases
     *
     * @return \Illuminate\View\View
     */
    public function listPurchases()
    {
        $data['shoes'] = $shoes = Shoe::whereHas('purchase' , function($purchase) {
            $purchase->where('buyer_id', Auth::user()->id)->where('status', '<>', 'pending');
        })->where('wanted', 0)->where('sold', 1)->paginate(10);

        return view('pages.account.purchases', $data);
    }

    /**
     * View a list of sold shoes
     *
     * @return \Illuminate\View\View
     */
    public function listSold()
    {
        $shoes = Shoe::where('seller', Auth::user()->id)
                    ->where('sold', true)
                    ->whereHas('purchase', function($purchase) {
                        $purchase->where('status', '<>', 'pending');
                    })
                    ->orderBy('created_at', 'desc')
                    ->paginate(16);
        $data['shoes'] = $shoes;

        return view('pages.account.sold', $data);
    }

    public function ajaxDispatchSold($id)
    {
        $shoe = Shoe::where('seller', Auth::user()->id)->where('sold', true)->where('id', $id)->first();

        if (!$shoe) {
            return response()->json(['status' => false], 500);
        }

        if ($shoe->purchase->status == 'paid') {
            $shoe->purchase->status = 'dispatched';
            $img = ShoeImage::where('shoe_id', $id)->first();
            $img_url = $img['base_url'].'/'.$img['public_url'];
            $img_url  = str_replace('/original/', '/medium/', $img_url );
            $pushData = [
                'notification_id' => $shoe->purchase->id,
                'notification_type' => 'Dispatched',
                'channel' => 'user_'.$shoe->purchase->buyer_id,
                'recipient' => $shoe->purchase->buyer_id,
                'message' => 'Marked as dispatched',
                'snippet' => $shoe->name,
                'extra_id' => $shoe->id,
                'img' => $img_url,
            ];
            $push = new NotificationController;
            $push->sendNotification($pushData);
        } else {
            $shoe->purchase->status = 'paid';
        }

        $shoe->purchase->save();

        return response()->json(['status' => true, 'shoe_status' => $shoe->purchase->status]);
    }

    public function postReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shoe_id' => 'required',
            'rating' => 'required|numeric|between:0,5',
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error_message', $validator->errors()->first());
        }

        $id = $request->input('shoe_id');
        $shoe = Shoe::findOrFail($id);
        $isSeller = Auth::user()->id == $shoe->sellerUser->id;

        if (Review::where('shoe_id', $id)->where('is_seller', $isSeller)->count() != 0) {
            return redirect()->back()->with('error_message', 'You have already reviewed this transaction.');
        }

        $review = new Review();
        $review->user_id = $shoe->purchase->user->id;
        $review->seller_id = $shoe->sellerUser->id;
        $review->rating = $request->input('rating');
        $review->text = $request->input('text');
        $review->is_seller = $isSeller;
        $review->shoe_id = $shoe->id;
        $review->save();

        $user = UserSearchable::find($isSeller ? $review->user_id : $review->seller_id);
        $user->calculateRating();
        $user->save();

        $pushData = [
            'notification_id' => $review->id,
            'notification_type' => 'Review',
            'channel' => 'user_' . ($isSeller ? $review->user_id : $review->seller_id),
            'recipient' => ($isSeller ? $review->user_id : $review->seller_id),
            'message' => 'Someone reviewed your transaction!',
            'snippet' => substr($review->text, 0, 100),
            'extra_id' => $review->id,

        ];
        $push = new NotificationController;
        $push->sendNotification($pushData);
        
        return redirect()->back()->with('success_message', 'Your review has been saved.');
    }

    public function postDispute(Request $request)
    {
        $this->validate($request, [
            'shoe_id' => 'required',
            'message' => 'required|between:1,10000'
        ]);

        $purchase = Purchase
            ::where('refunded', 0)
            ->where('shoe_id', $request->input('shoe_id'))
            ->firstOrFail();

        $dispute = new Dispute();
        $dispute->message = $request->input('message');
        $dispute->purchase_id = $purchase->id;
        $dispute->disputer_id = Auth::user()->id;
        $dispute->save();

        $this->dispatch(new SendAdminDisputeEmail($dispute));

        return redirect()->back()->with('success_message', 'Your dispute has been created. You will be contacted shortly.');
    }

    public function addUsername(Request $request)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), ['username' => 'required|unique:users|min:6']);
        if ($validation->fails()) {
            return redirect('/account/complete')->with('error', $validation->messages()->first());
        } else {
            $user->username = $request->username;
            $user->save();
            return redirect('/account');
        }
    }


    public function edit()
    {
        $user = UserSearchable::with(['listings', 'likes', 'wanted'])->findOrFail(Auth::user()->id);

        $data['user'] = $user;

        return view('pages.account.edit', $data);
    }


    public function postEdit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bio' => 'string|max:255',
            'city' => 'string|max:255',
            'county' => 'string|max:255',
            'phone' => 'string|min:7|max:15'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error_message', $validator->errors()->all()[0]);
        }

        $user = Auth::user();
        $user->bio = $request->input('bio');
        $user->city = $request->input('city');
        $user->county = $request->input('county');
        $user->image = $request->input('image');
        if ($request->input('phone') !== $user->phone) {
            $user->phone = $request->input('phone');
            $user->phone_verified = 0;
        }
        $user->save();

        return redirect('account/listings')->with('success_message', 'Account details updated.');

    }

    public function postEditPicture(Request $request)
    {
        if (!$request->hasFile('file')) {
            return response()->json(['status' => false], 500);
        }

        $user = Auth::user();
        $file = $request->file('file');

        $newName = 'user_profile_' . $user->id . '_' . md5(time()) . '.' . $file->getClientOriginalExtension();
        $folder = 'user_' . $user->id;
        $file->move(public_path() . '/uploads/users/' . $folder, $newName);
        Image::make(public_path() . '/uploads/users/' . $folder . '/' . $newName)->save();
        $url = env('BASE_URL') . '/uploads/users/' . $folder . '/' . $newName;

        return response()->json(['status' => true, 'url' => $url]);
    }

    /**
     * View a list of received offers
     *
     * @return \Illuminate\View\View
     */
    public function offersReceived()
    {
        $shoes = Shoe
            ::where('seller', Auth::user()->id)
            ->where('wanted', 0)
            ->where('sold', 0)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        $data['shoes'] = $shoes;

        return view('pages.account.offers-received', $data);
    }

    /**
     * View list of sent offers
     *
     * @return \Illuminate\View\View
     */
    public function offersSent()
    {
        $offers = ShoeOffer
            ::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        $data['offers'] = $offers;

        return view('pages.account.offers-sent', $data);
    }

    /**
     * Accept an offer on a shoe and decline all other offers on the same shoe
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxAcceptOffer($id)
    {
        $offer = ShoeOffer::where('id', $id)->with(['shoe' => function($shoe) {
            $shoe->where('seller', Auth::user()->id);
        }])->first();

        if (!$offer) {
            return response()->json(['status' => false], 500);
        }

        $offers = $offer->shoe->offers;
        $shoe = $offer->shoe;
        $declineIds = [];

        //Decline other offers on the shoe
        foreach ($offers as $declineOffer) {
            if ($offer->id != $declineOffer->id) {
                $declineOffer->accepted = false;
                $declineOffer->declined = true;
                $declineOffer->save();
                $declineIds[] = $declineOffer->id;
            }
        }

        //Accept the remaining offer
        $offer->accepted = true;
        $offer->declined = false;
        $offer->save();

        $pushData = [
            'notification_id' => $offer->id,
            'notification_type' => 'OfferAccepted',
            'channel' => 'user_'.$offer->user_id,
            'recipient' => $offer->user_id,
            'message' => 'You have a new offer!',
            'extra_id' => $shoe->id,
        ];
        $push = new NotificationController;
        $push->sendNotification($pushData);


        return response()->json([
            'status' => true,
            'declined' => $declineIds,
            'accepted' => [$offer->id]
        ]);
    }

    /**
     * Decline an offer on a shoe
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxDeclineOffer($id)
    {
        $offer = ShoeOffer::where('id', $id)->with(['shoe' => function($shoe) {
            $shoe->where('seller', Auth::user()->id);
        }])->first();

        if (!$offer) {
            return response()->json(['status' => false], 500);
        }

        $offer->declined = true;
        $offer->accepted = false;
        $offer->save();

        $pushData = [
            'notification_id' => $offer->id,
            'notification_type' => 'OfferDeclined',
            'channel' => 'user_'.$offer->user_id,
            'recipient' => $offer->user_id,
            'message' => 'You have a new offer!',
            'extra_id' => $offer->shoe->id,
        ];
        $push = new NotificationController;
        $push->sendNotification($pushData);

        return response()->json(['status' => true]);
    }
}