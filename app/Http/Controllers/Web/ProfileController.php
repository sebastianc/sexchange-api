<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 12/08/16
 * Time: 16:24
 */

namespace App\Http\Controllers\Web;


use App\BlockedUser;
use App\Follow;
use App\Http\Controllers\API\V1\NotificationController;
use App\Review;
use App\User;
use App\UserSearchable;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class ProfileController extends BaseController
{
    function index($slug)
    {
        return redirect('/profile/' . $slug . '/listings');
    }

    function listings($slug)
    {
        $user = UserSearchable::with(['listings', 'likes', 'wanted'])->where('username', $slug)->firstOrFail();
        $data['type'] = 'listings';
        $data['can_report'] = BlockedUser::currentUserHasBlocked($user->id)->count() == 0;
        $data['current_user'] = Auth::check() ? $user->id == Auth::user()->id : false;
        $data['user'] = $user;
        $data['shoes'] = $user->listings()->paginate(16);

        return view('pages.profile.index', $data);
    }

    function likes($slug)
    {
        $user = UserSearchable::with(['listings', 'likes', 'wanted'])->where('username', $slug)->firstOrFail();

        $data['type'] = 'likes';
        $data['can_report'] = BlockedUser::currentUserHasBlocked($user->id)->count() == 0;
        $data['current_user'] = Auth::check() ? $user->id == Auth::user()->id : false;
        $data['user'] = $user;
        $data['shoes'] = $user->likes()->paginate(16);

        return view('pages.profile.index', $data);
    }

    function reviews($slug)
    {
        return redirect()->to('profile/' . $slug . '/reviews/purchased');
    }

    function reviewsPurchased($slug)
    {
        $user = UserSearchable::where('username' , $slug)->firstOrFail();

        $data['user'] = $user;
        $data['property'] = 'seller';
        $data['type'] = 'purchased';
        $data['reviews'] = Review
            ::where('user_id', $user->id)
            ->with('user')
            ->where('is_seller', true)
            ->orderBy('created_at', 'desc')
            ->paginate(16);

        $groupedReviews = [];
        foreach ($data['reviews'] as $index => $review) {
            $groupedReviews[strval(floor($index/2))][] = $review;
        }

        $data['reviewGroups'] = $groupedReviews;

        return view('pages.profile.reviews', $data);
    }

    function reviewsSold($slug)
    {
        $user = UserSearchable::where('username' , $slug)->firstOrFail();

        $data['user'] = $user;
        $data['property'] = 'user';
        $data['type'] = 'sold';
        $data['reviews'] = Review
            ::where('seller_id', $user->id)
            ->with('user')
            ->where('is_seller', false)
            ->orderBy('created_at', 'desc')
            ->paginate(16);

        $groupedReviews = [];
        foreach ($data['reviews'] as $index => $review) {
            $groupedReviews[strval(floor($index/2))][] = $review;
        }

        $data['reviewGroups'] = $groupedReviews;

        return view('pages.profile.reviews', $data);
    }

    function followers($slug)
    {
        $user = UserSearchable::where('username', $slug)->firstOrFail();
        $data['user'] = $user;
        $data['type'] = 'followers';
        $data['property'] = 'users';
        $data['follows'] = $user->followers()->paginate(16);
        $data['title'] = 'Followers';

        return view('pages.profile.followers', $data);
    }

    function following($slug)
    {
        $user = UserSearchable::where('username', $slug)->firstOrFail();
        $data['user'] = $user;
        $data['type'] = 'following';
        $data['property'] = 'following';
        $data['follows'] = $user->follows()->paginate(16);
        $data['title'] = 'Following';

        return view('pages.profile.followers', $data);
    }

    function ajaxFollow($id)
    {
        $follow = Follow::where('user_id', Auth::user()->id)->where('follows', $id)->first();
        $user = User::find($id);

        if (!$user) {
            return response()->json(['status' => false], 500);
        }

        if ($follow) {
            $follow->delete();
            return response()->json(['status' => true, 'action' => 'unfollowed']);
        } else {
            $follow = new Follow();
            $follow->user_id = Auth::user()->id;
            $follow->follows = $user->id;
            $follow->save();

            $pushData = [
                'notification_id' => $id,
                'notification_type' => 'Follow',
                'channel' => 'user_'.$id,
                'recipient' => $id,
                'message' => 'You have a new follower!',
                'extra_id' => $id,
            ];
            $push = new NotificationController();
            $push->sendNotification($pushData);

            return response()->json(['status' => true, 'action' => 'followed']);
        }
    }

    /**
     * Report a user and block them
     *
     * @param string $slug The users username
     * @return \Illuminate\Http\RedirectResponse
     */
    function report($slug)
    {
        $user = User::where('username', $slug)->first();

        if (!$user) {
            return redirect()->back()->with('error_message', 'Could not report user.');
        }

        if (BlockedUser::currentUserHasBlocked($user->id)->count() != 0) {
            return redirect()->back()->with('error_message', 'Could not report user.');
        }

        $block = new BlockedUser();
        $block->user_id = Auth::user()->id;
        $block->blocked = $user->id;
        $block->save();

        $blockCount = BlockedUser::where('blocked', $user->id)->count();
        if ($blockCount > 15) {
            $user->delete();
        }

        return redirect()->back()->with('success_message', 'User reported.');
    }
}