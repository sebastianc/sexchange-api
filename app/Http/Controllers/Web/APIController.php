<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/08/2016
 * Time: 14:54
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\API\V1\ShoeController;
use App\Http\Controllers\Controller;

class APIController extends Controller
{
    public function likeShoe($id) {
        $controller = new ShoeController();
        return $controller->like($id);
    }

    public function unlikeShoe($id) {
        $controller = new ShoeController();
        return $controller->unlike($id);
    }
}