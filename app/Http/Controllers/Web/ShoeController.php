<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 04/08/16
 * Time: 17:25
 */

namespace App\Http\Controllers\Web;


use App\BlockedUser;
use App\ColorsShoe;
use App\Comment;
use App\Events\ShoeViewEvent;
use App\Http\Controllers\API\V1\NotificationController;
use App\Shoe;

use App\ShoeImage;
use App\ShoeOffer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ShoeController extends BaseController
{
    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';

    public function viewShoe(Request $request, $slug)
    {
        $shoe = Shoe::where('slug', $slug)->firstOrFail();


        //Increment views
//        Event::fire('ShoeView', $shoe);
        Event::Fire(new ShoeViewEvent($request, $shoe));

        if (Auth::check()) {
            $offer = ShoeOffer
                ::where('shoe_id', $shoe->id)
                ->where('user_id', Auth::user()->id)
                ->where('accepted', true)
                ->first();
        } else {
            $offer = null;
        }

        $canReport = BlockedUser::currentUserHasBlocked($shoe->seller)->count() == 0;
        $isCurrentUser = Auth::check() ? $shoe->seller == Auth::user()->id : false;

        return view('pages.shoe', [
            'shoe' => $shoe,
            'offer' => $offer,
            'can_report' => $canReport,
            'current_user' => $isCurrentUser
        ]);
    }

    public function editShoe($slug)
    {
        if (!Auth::user()->stripe_connected) {
            return redirect('/payment/edit-bank');
        }

        $shoe = Shoe::where('slug', $slug)->first();
        $selected_colors = $shoe->colors->pluck('id')->all();
        $color_string = implode(', ' , $shoe->colors->pluck('name')->all());
        $wanted_id = null;
        $search = new SearchController();
        $filters = $search->getSearchFilters();

        return view('pages.edit-shoe')->with(compact('filters', 'shoe', 'selected_colors', 'color_string' ));
    }
    function postEditShoe(){
        $info = Input::all();
        $allowed = ['name', 'description', 'cost', 'condition', 'delivery', 'size', 'brand', 'min_offer', 'wanted'];

        if (!Auth::user()->stripe_connected) {
            return redirect('/payment/edit-bank');
        }

        $shoe = Shoe::find($info['id']);

        foreach($info as $field => $val){
            if(in_array($field, $allowed)){
                $shoe->$field = $val;
            }
        }
        $shoe->save();
        try{
            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            $i = 1;
            foreach($images as $index => $image){
                if($image) {
                    $file = $image;
                    $newName = 'shoe_upload_'.$shoe->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                    $folder = 'user_'.Auth::user()->id;
                    $file->move($this->uploadsFolder.$folder, $newName);
                    Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                    if($image = $shoe->getImage($i)){
                      //If image exists update it
                    }else{
                        $image = new ShoeImage();
                    }
                    $image->shoe_id = $shoe->id;
                    $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                    $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                    $image->base_url = env('BASE_URL');
                    $image->original_filename = $file->getClientOriginalName();
                    try{
                        $image->save();
                    }catch (QueryException $e){
                        return redirect()->back()->withInput()->with('error_message', 'Something went wrong, please try again later');
                    }
                }
                $i ++;
            }

            if($colors = Input::get('colours')){
                $colors = json_decode($colors);
                //clear colors and add newly selected for simplicity
                ColorsShoe::where('shoe_id', $shoe->id)->delete();
                foreach($colors as $color){
                    $shoe->colors()->attach($color->value);
                }
            }

            return redirect('/shoes/'.$shoe->slug)->with('success_message', 'Your shoe has been successfully updated.');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->with('error_message', 'Something went wrong, please try again later');
        }

    }

    //TODO: Post comments
    public function ajaxComments(){
        try{
            $comments = Comment::where('shoe_id', Input::get('shoe'))->orderBy('created_at', 'desc')->paginate(10);
            return view('ajax.comments')->with('comments', $comments);
        }catch (\Exception $e){
            dd($e);
        }
    }
    public function ajaxComment(){
        try{
            $comment = new Comment;
            $comment->user_id = Auth::user()->id;
            $comment->shoe_id = Input::get('shoe');
            $comment->text = Input::get('comment');
            $comment->save();


            $shoe = Shoe::find(Input::get('shoe'));

            if($comment->user_id !== $shoe->seller){
                $pushData = [
                    'notification_id' => $comment->id,
                    'notification_type' => 'Comment',
                    'channel' => 'user_'.$shoe->seller,
                    'recipient' => $shoe->seller,
                    'message' => 'Someone commented on your shoe!',
                    'snippet' => substr($comment->text, 0, 100),
                    'extra_id' => $shoe->id,

                ];
                $push = new NotificationController;
                $push->sendNotification($pushData);
            }


            return view('ajax.new-comment')->with('comment', $comment);
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function ajaxDelete($id)
    {
        $shoe = Shoe::where('seller', Auth::user()->id)->where('id', $id)->firstOrFail();

        if ($shoe) {
            $shoe->delete();
            return response()->json(['status'=>true]);
        }

        return response()->json(['status'=>false]);
    }

    public function ajaxMarkSold($id)
    {
        $shoe = Shoe::where('seller', Auth::user()->id)->where('id', $id)->firstOrFail();

        if ($shoe) {
            $shoe->marked_as_sold = $shoe->marked_as_sold == 1 ? 0 : 1;
            $shoe->save();
            return response()->json(['status'=>true]);
        }

        return response()->json(['status'=>false]);
    }

    /**
     * Make an offer on a shoe.
     *
     * @param Request $request The HTTP request
     * @param int $id The shoe ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxOffer(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric'
        ]);
        $shoe = Shoe::where('id', $id)->where('sold', false)->where('wanted', false)->firstOrFail();

        if ($validator->fails() || !$shoe) {
            return response()->json(['status' => false, 'message' => false], 500);
        }


        if ($request->input('amount') > $shoe->cost) {
            return response()->json(['status' => false, 'message' => 'Offer is higher than price'], 400);
        }


        if ($shoe->min_offer > $request->input('amount')) {
            return response()->json(['status' => false, 'message' => 'Your offer cannot be lower than £' . number_format($shoe->min_offer) . '.'], 400);
        }


        $offer = new ShoeOffer();
        $offer->shoe_id = $shoe->id;
        $offer->user_id = Auth::user()->id;
        $offer->amount = $request->input('amount');
        $offer->accepted = 0;
        $offer->declined = 0;
        $offer->save();

        $pushData = [
            'notification_id' => $offer->id,
            'notification_type' => 'Offer',
            'channel' => 'user_'.$shoe->seller,
            'recipient' => $shoe->seller,
            'message' => 'You have a new offer!',
            'extra_id' => $shoe->id,
        ];
        $push = new NotificationController;
        $push->sendNotification($pushData);

        return response()->json(['status' => true]);
    }
}