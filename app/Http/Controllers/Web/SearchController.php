<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 05/08/16
 * Time: 14:13
 */

namespace App\Http\Controllers\Web;


use App\Brand;
use App\Color;
use App\Shoe;
use App\ShoeSize;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;


class SearchController extends BaseController
{

    /**
     * Accept get param from search form and redirect to nice search URL
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function searchRedirect(Request $request){
        if(Input::get('s')){
            $term = Input::get('s');
            return redirect('/search/'.$term)->with('term', $term);
        }else{
//            dd($request);
            return redirect('/');
        }
    }

    /**
     * Search page with default search results
     * @param $term
     * @return $this
     */
    public function search($term, Request $request){
        $user_url = '/search/users/'.$term;
        $param = Input::all();
        if(isset($param['filter'])){
            $param['term'] = $term;
            $shoes = $this->filterShoes($param);
        }else{
            $shoes = Shoe
                ::search($term)
                ->where('sold', 0)
                ->where('marked_as_sold', 0)
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        }
        $filters = $this->getSearchFilters();
        $request->session()->flash('term', $term);
        return view('pages.search')->with(compact('shoes', 'filters', 'user_url'));

    }


    /**
     * Search page with default search results
     * @param $term
     * @param Request $request
     * @return $this
     */
    public function searchUsers($term, Request $request){
        $shoe_url = '/search/'.$term;
        $param = Input::all();

        $users = UserSearchable::search($term)->paginate(20);

        $request->session()->flash('term', $term);
        return view('pages.search-users')->with(compact('users', 'shoe_url'));

    }

    /**
     * Ajax function to return all shoes for requested search/filter
     * @param Request $request
     * @return $this
     */
    public function ajax_search(Request $request){
        try{
            $param = Input::all();
            $user_url = '/search/users/'.$param['term'];
            $shoes = $this->filterShoes($param);
            $request->session()->flash('term', $param['term']);
//            var_dump($param);
            return view('ajax.search')->with(compact('shoes', 'user_url'));
        }catch (\Exception $e){
            var_dump($e);
        }

    }

    public function explore(Request $request, $wanted = 0 )
    {
        try {
            $request->session()->flash('term', '');
            //Setting paged amount
            $paged = 20;
            if ($custom = Input::get('per_page')) {
                $paged = $custom;
            }
            //Get Shoes for sale
            $param = Input::all();
            if (isset($param['filter'])) {
                $param['term'] = '';
                $shoes = $this->filterShoes($param);
            } else {
               $shoes = Shoe::
                where('wanted', $wanted)
                   ->where('sold', 0)
                   ->where('marked_as_sold', 0)
                   ->where('deleted_at', null)
                   ->orderBy('created_at', 'desc')
                   ->paginate($paged);
            }

            $filters = $this->getSearchFilters();

            return view('pages.explore')->with(compact('shoes', 'wanted', 'filters'));

        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function wanted(Request $request){
        return $this->explore($request, 1);
    }

    /**
     * Collect all filter params for use on front end
     * @return array
     */
    public function getSearchFilters(){
        $brands = Brand::all();
        $colours = Color::orderBy('name')->get();
        $sizes = ShoeSize::all();
        $prices = [
            [0,50],
            [50,100],
            [100,150],
            [150,200],
            [200,250],
            [250,300],
            [300,350],
            [350,400],
            [400,450],
            [450,500],
            [500,550],
            [550,600],
            [600,650],
            [650,700],
            [700,750],
            [750,800],
            [800,850],
            [850,'plus'],

        ];
        return compact('brands', 'colours', 'sizes', 'prices');
    }

    public function filterShoes($param){
            try {
                //Get shoes and search if needed
                $shoes = Shoe::where('deleted_at', null)->where('marked_as_sold', 0)->whereSold(0);
                $url = explode('?', URL::previous())[0] . '?filter=true';
                if($param['term']){
                    $shoes = $shoes->search($param['term']);
                }

                //Build URL of filter params
                $i = 0;
                foreach($param as $p => $val){
                    if($p !== "filter" && $p !== "term" && $p !== "_token" && $p !== "page") {
                        if (is_array($val)) {
                            $url = $url . '&' . $p . '=' . urlencode(implode(',', $val));
                        } else {
                            $url = $url . '&' . $p . '=' . urlencode($val);
                        }
                    }
                    $i++;
                }

                if(isset($param['buyType'], $param['wantedType'])) {
                    $param['buyType'] = filter_var($param['buyType'], FILTER_VALIDATE_BOOLEAN);
                    $param['wantedType'] = filter_var($param['wantedType'], FILTER_VALIDATE_BOOLEAN);
                    if ($param['buyType'] && !$param['wantedType']) {
                        $shoes = $shoes->where('wanted', 0);
                    } elseif ($param['wantedType'] && !$param['buyType']) {
                        $shoes = $shoes->where('wanted', 1);
                    }
                }

                //Filter
                if(isset($param['brand'])){
                    if(is_string($param['brand'])){
                        $param['brand'] = explode(',', $param['brand']);
                    }
                    $shoes = $shoes->whereIn('brand', $param['brand']);
                }

                if(isset($param['size'])){
                    if(is_string($param['size'])){
                        $param['size'] = explode(',', $param['size']);
                    }
                    $shoes = $shoes->whereIn('size', $param['size']);
                }

                if(isset($param['condition'])){
                    $shoes = $shoes->where('condition', '>=' , $param['condition']);
                }

                if(isset($param['colours'])){
                    if(is_string($param['colours'])){
                        $param['colours'] = explode(',', $param['colours']);
                    }
                    $shoes = $shoes->whereHas('colors', function($q) use($param)  {
                        $q->whereIn('colors.id', $param['colours']);
                    });
                }

                if(isset($param['price'])){
                    if(is_string($param['price'])){
                        $param['price'] = explode(',', $param['price']);
                    }

                    $shoes = $shoes->where(function($q) use($param){
                        foreach($param['price'] as $price){
                            $range = explode('_', $price);
                            if($range[1] !== 'plus'){
                                $q->orWhereBetween('cost', $range);
                            }else{
                                $min = $range[0];
                                $q->orWhere('cost','>', $min);
                            }
                        }
                    });
                }

                //Sort Results
                if(isset($param['sortBy'])){
                    $sort = $param['sortBy'];
                    switch ($sort){
                        case 'price_low_high':
                            if(!$param['term']){
                                $shoes = $shoes->orderBy('cost', 'asc');
                            }else{
                                $query = $shoes->getQuery();
                                $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'ASC']);
                                $shoes = $shoes->setQuery($query);
                            }

                            break;
                        case 'price_high_low':
                            if(!$param['term']){
                                $shoes =$shoes->orderBy('cost', 'desc');
                            }else{
                                $query = $shoes->getQuery();
                                $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'DESC']);
                                $shoes = $shoes->setQuery($query);
                            }
                            break;
                        case 'views':
                            $date = Carbon::now()->subMonth();
                            $shoes = $shoes
                                ->leftJoin('shoe_views', 'shoes.id', '=', 'shoe_views.shoe_id')
                                ->select(DB::raw('shoes.*, count(case when shoe_views.created_at > "'.$date.'" then 1 else 0 end) as recent_views'))
                                ->groupBy('shoes.id')
                                ->orderBy('recent_views' , 'desc');
                            if($param['term']){
                                $query = $shoes->getQuery();
                                $query->orders = array_prepend($query->orders, ['column' => 'recent_views', 'direction' => 'DESC']);
                                $shoes = $shoes->setQuery($query);
                            }
                            break;
                        case 'recent':
                            if(!$param['term']){
                                $shoes = $shoes->orderBy('created_at', 'desc');
                            }else{
                                $query = $shoes->getQuery();
                                $query->orders = array_prepend($query->orders, ['column' => 'created_at', 'direction' => 'DESC']);
                                $shoes = $shoes->setQuery($query);
                            }
                            break;

                        default:
                            if(!$param['term']){
                                $shoes = $shoes->orderBy('created_at', 'desc');
                            }else{
                                $query = $shoes->getQuery();
                                $query->orders = array_prepend($query->orders, ['column' => 'created_at', 'direction' => 'DESC']);
                                $shoes = $shoes->setQuery($query);
                            }
                    }
                }

        }catch (\Exception $e){
            var_dump($e);
        }
        $paged = 20;
        if ($custom = $param['per_page']) {
            $paged = $custom;
        }
//        var_dump($shoes->toSql());
        $shoes = $shoes->paginate($paged);
        $shoes->setPath($url);
        return $shoes;
    }


}