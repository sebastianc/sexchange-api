<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/08/2016
 * Time: 14:54
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\API\V1\ShoeController;
use App\Http\Controllers\Controller;
use App\Jobs\SendContactFormEmail;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class AboutController extends Controller
{
    public function postContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'number' => 'required',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error_message', $validator->errors()->first());
        }

        $this->dispatch(
            new SendContactFormEmail(
                $request->input('name'), $request->input('email'),
                $request->input('number'), $request->input('message')
            )
        );
        
        return redirect()->back()->with('success_message', 'Your request has been sent. We\'ll be in touch soon.');
    }
}