<?php

namespace App\Http\Controllers\Web;
use App\ShoeWeb;
use Illuminate\Support\Facades\Input;


/**
 * Created by PhpStorm.
 * User: ed
 * Date: 01/08/16
 * Time: 15:01
 */
class ExploreController extends BaseController
{

    public function index(){
        try{
            //Setting paged amount
            $paged = 20;
            if($custom = Input::get('per_page')){
                $paged = $custom;
            }
            //Get Shoes for sale
            $shoes =
                ShoeWeb::
                where('wanted', 0)
                ->where('sold', 0)
                ->where('marked_as_sold', 0)
                ->where('deleted_at', null)
                ->orderBy('created_at', 'desc')
                ->paginate($paged);
            //Get wanted shoes
            $wanted =
                ShoeWeb::
                where('wanted', 1)
                    ->where('sold', 0)
                    ->where('marked_as_sold', 0)
                    ->where('deleted_at', null)
                    ->orderBy('created_at', 'desc')
                    ->paginate($paged);

            $search = new SearchController();
            $filters = $search->getSearchFilters();
            
            return view('pages.explore')->with(compact('shoes', 'wanted', 'filters'));

        }catch (\Exception $e){
            dd($e);
        }
    }

}