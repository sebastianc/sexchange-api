<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 02/08/16
 * Time: 17:36
 */

namespace App\Http\Controllers\Web;


use App\Follow;
use App\UserSearchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends BaseController
{
    use ThrottlesLogins, AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
       try{
           $data = Input::only('email', 'password', 'username');


           $validator = Validator::make($request->all(), [
               'email' => 'required|email|unique:users|max:255',
               'password' => 'required|min:8',
               'username' => 'required|min:4|max:36|unique:users'
           ]);
           if ($validator->fails()) {
               return json_encode(['status' => 'failed', 'error' => $validator->errors()->first()]);
           } else {
               $new_user = new User;
               $new_user->email = $data['email'];
               $new_user->password = bcrypt($data['password']);
               $new_user->username = $data['username'];
               $new_user->image = url('assets/images/default_profile.png');
               $new_user->average_rating = 5;

               if($new_user->save()) {
                   $follow = new Follow;
                   $follow->user_id = $new_user->id;
                   $follow->follows = env('GEORGE_ID', 1);
                   $follow->save();
                   Auth::login($new_user);
                   return json_encode(['status' => 'success']);
               }

           }
       }catch (\Exception $e){
           dd($e);
       }
    }

    function registerPage(){
        return view('pages.register');
    }

    /**
     * Upload profile picture after registration and redirect to correct page
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upload_profile_image(Request $request){
        $user = Auth::user();
        $image = $request->file('image');
        if($image){
            $newName = 'user_profile_'.Auth::user()->id.'_'.md5(time()).'.'.$image->getClientOriginalExtension();
            $folder = 'user_'.Auth::user()->id;
            $image->move($this->uploadsFolder.$folder, $newName);
            Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
            $user->image = env('BASE_URL').'/'.$this->imageCacheRoute.$folder.'/'.$newName;
            $user->save();
            return redirect('/account?account_created=');
        }else{
            return redirect('/');
        }
    }

    /**
     * Login user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login( Request $request)
    {
        // grab credentials from the request
        $credentials = Input::only('email', 'password');

        try {
            //If too many login attempts block IP
            if ($this->hasTooManyLoginAttempts($request)) {
                return redirect('/login')->with('error' , 'Too many failed login attempts');
            }
            // attempt to verify the credentials for the user
            if (!Auth::attempt($credentials)) {
                $user = User::where('email', $credentials['email'])->where('parse_id', '!=', 'null')->where('initial_pass_reset', 0)->first();
                if($user){
                    return redirect('/login')->with(['parse_user_login' => true , 'error' => 'Please reset you\'re password.']);
                }
                //Failed so increment attempts
                $this->incrementLoginAttempts($request);
                return redirect('/login')->with('error' , 'Incorrect email or password')->with('attempts_remaining' , $this->retriesLeft($request));
            }
        } catch (\Exception $e) {
            // something went wrong whilst attempting to login user
            return redirect('/login')->with('error' , 'Something went wrong, please try again later.');
//            dd($e);

        }
        // all good so return the user and reset login attempts
        $this->clearLoginAttempts($request);
        $user = UserSearchable::with('follows', 'followers')->find(Auth::user()->id);
        $user['phone_number'] = $user->phone();
        return redirect()->intended('account')->with('user', $user);

    }

    /**
     * View login page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginPage(Request $request){
        return view('pages.login');
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');

    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook and login/register user.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {

        $fb_user = Socialite::driver('facebook')->user();

        if($exists = User::where('facebook_id', $fb_user->id)->first()){
            Auth::login($exists);
            return redirect('/account')->with('user', $exists);
        }else{
            if($non_fb = User::where('email', $fb_user->email )->first()){
                $non_fb->facebook_id = $fb_user->id;
                $non_fb->save();
                Auth::login($non_fb);
                return redirect('/account');
            }else {
                $name = explode(' ', $fb_user->name);
                $last = count($name) - 1;
                $user = new User;
                $user->facebook_id = $fb_user->id;
                $user->first_name = $name[0];
                $user->last_name = $name[$last];
                $user->email = $fb_user->email;
                $user->password = bcrypt(str_random(12));
                $user->image = $fb_user->avatar_original;
                $user->average_rating = 5;
                $user->save();
                Auth::login($user);
                return redirect()->intended('account')->with('user', $user);
            }

        }
    }

}