<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 22/08/16
 * Time: 15:38
 */

namespace App\Http\Controllers\Web;


use App\ColorsShoe;
use App\Shoe;
use App\ShoeImage;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ListingController extends BaseController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';


    function index(){

        if (!Auth::user()->stripe_connected) {
            return redirect('/payment/edit-bank');
        }


        if($slug = Input::get('wanted')){
            $wanted_id = Shoe::where('slug', $slug)->first()->id;
        }else{
            $wanted_id = null;
        }


        $search = new SearchController();
        $filters = $search->getSearchFilters();

        return view('pages.upload')->with(compact('filters', 'wanted_id' ));
    }

    function addShoe(){
        $info = Input::all();
        $allowed = ['name', 'description', 'cost', 'condition', 'delivery', 'size', 'brand', 'min_offer', 'wanted'];
        $back_url = $info['wanted']? url('/upload/shoe#wanted') : url('/upload/shoe');

        if (!Auth::user()->stripe_connected) {
            return redirect('/payment/edit-bank');
        }

        $wanted_id = Input::get('wanted_id');
        $shoe = new Shoe;
        $shoe->seller = Auth::user()->id;
        $shoe->slug = str_replace(' ', '-', $info['name']);

        foreach($info as $field => $val){
            if(in_array($field, $allowed)){
                $shoe->$field = $val;
            }
        }

        try{
            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            if($images['image_1']){
                $shoe->save();
                if(isset($wanted_id)){
                    $wanted = Shoe::find($wanted_id);
                    if($wanted){
                        $pushData = [
                            'notification_id' => $shoe->id,
                            'notification_type' => 'Wanted',
                            'channel' => 'user_'.$wanted->seller,
                            'recipient' => $wanted->seller,
                            'message' => 'Someone is selling a shoe you want!',
                            'snippet' => $shoe->name,
                            'extra_id' => $shoe->id,
                        ];
                        $push = new NotificationController;
                        $push->sendNotification($pushData);
                    }
                }
            }else{
                return redirect($back_url)->withInput()->with('error_message', 'Please add at least one image.');
            }
            foreach($images as $index => $image){
                if($image) {
                    $file = $image;
                    $newName = 'shoe_upload_'.$shoe->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                    $folder = 'user_'.Auth::user()->id;
                    $file->move($this->uploadsFolder.$folder, $newName);
                    Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                    $image = new ShoeImage();
                    $image->shoe_id = $shoe->id;
                    $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                    $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                    $image->base_url = env('BASE_URL');
                    $image->original_filename = $file->getClientOriginalName();
                    try{
                        $image->save();
                    }catch (QueryException $e){
                        return redirect($back_url)->withInput()->with('error_message', 'Something went wrong, please try again later');
                    }
                }
            }

            if($colors = Input::get('colours')){
                $colors = json_decode($colors);
                foreach($colors as $color){
                    $shoe->colors()->attach($color->value);
                }
            }

            return redirect('/shoes/'.$shoe->slug)->with('success_message', 'Your shoe has been successfully uploaded.');
        }catch (QueryException $e){
            return redirect($back_url)->withInput()->with('error_message', 'Something went wrong, please try again later');
        }



    }

}