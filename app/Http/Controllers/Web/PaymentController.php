<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 05/09/2016
 * Time: 14:47
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\API\V1\NotificationController;
use App\Http\Controllers\Controller;
use App\Models\ManagedStripe;
use App\Purchase;
use App\Shoe;
use App\ShoeOffer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function ajaxPay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address_line_one' => 'required',
            'address_line_two' => '',
            'address_county' => 'required',
            'address_postcode' => 'required',
            'stripe_token' => 'required',
            'shoe_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()->first()], 500);
        }

        $shoe = Shoe::whereSold(0)->whereNull('deleted_at')->whereMarkedAsSold(0)->whereReserved(0)->where('id', $request->input('shoe_id'))->first();
        $offer = ShoeOffer
            ::where('user_id', Auth::user()->id)
            ->where('accepted', 1)
            ->where('shoe_id', $shoe->id)
            ->first();
        $price = $offer ? $offer->amount : $shoe->cost;

        if (!$shoe) {
            return response()->json(['status' => false, 'error' => 'This shoe has already been purchased.'], 500);
        }

        $purchase = new Purchase;
        $purchase->shoe_id = $shoe->id;
        $purchase->buyer_id = Auth::user()->id;
        $purchase->status = 'pending';
        $purchase->payment_sent = 0;
        $purchase->price = $price;
        $purchase->address_line1 = $request->input('address_line_one');
        $purchase->address_line2 = $request->input('address_line_two');
        $purchase->address_state = $request->input('address_county');
        $purchase->address_zip = $request->input('address_postcode');
        $purchase->ship_address_line1 = $request->input('address_line_one');
        $purchase->ship_address_line2 = $request->input('address_line_two');
        $purchase->ship_address_county = $request->input('address_county');
        $purchase->ship_address_postcode = $request->input('address_postcode');
        $purchase->save();

        $seller = User::find($shoe->seller);
        $stripeToken = $request->input('stripe_token');
        $stripe = new ManagedStripe();
        $paid = $stripe->payToUser($seller, $price, $stripeToken, $purchase->id);

        if (!$paid) {
            $purchase->delete();
            return response()->json(['status' => false, 'error' => 'Unable to charge card.'], 500);
        }

        $purchase->status = 'paid';
        $purchase->payment_sent = 1;
        $purchase->save();
        $shoe->sold = true;
        $shoe->save();

        $img_url = $shoe->getImage(1)->small();
        $pushData = [
            'notification_id' => $purchase->id,
            'notification_type' => 'Purchase',
            'channel' => 'user_'.$seller->id,
            'recipient' => $seller->id,
            'message' => 'Your shoe has been sold!',
            'snippet' => $shoe->name,
            'extra_id' => $shoe->id,
            'img' => $img_url,
        ];
        $push = new NotificationController();
        $push->sendNotification($pushData);

        return response()->json(['status' => true, 'purchaseId' => $purchase->id], 200);
    }

    /**
     * Page to enter address and pay for a shoe
     * @param int $id Shoe id
     * @return \Illuminate\View\View
     */
    public function payShoe($id)
    {
        $shoe = Shoe::where('sold', 0)->where('id', $id)->firstOrFail();

        return view('pages.payment.pay', [
            'shoe' => $shoe,
            'offer' => null,
        ]);
    }

    /**
     * Page to enter address and pay for a shoe
     * @param int $id Offer id
     * @return \Illuminate\View\View
     */
    public function payOffer($id)
    {
        $offer = ShoeOffer
            ::where('user_id', Auth::user()->id)
            ->where('accepted', true)
            ->where('id', $id)
            ->firstOrFail();

        return view('pages.payment.pay', [
            'shoe' => $offer->shoe,
            'offer' => $offer,
        ]);
    }

    /**
     * Purchase complete page.
     *
     * @param int $id The purchase ID
     * @return \Illuminate\View\View
     */
    public function purchaseComplete($id)
    {
        $purchase = Purchase::where('buyer_id', Auth::user()->id)->where('id', $id)->firstOrFail();

        return view('pages.payment.complete', ['purchase' => $purchase]);
    }

    /**
     * Page with an update bank details form.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function bankDetails(Request $request)
    {
        //Always redirect back to the previous page
        if (!$request->session()->has('bank_redirect')) {
            $request->session()->set('bank_redirect', URL::previous());
        }

        $firstSetup = Auth::user()->stripeAccount === null;

        if (!$firstSetup) {
            $stripe = new ManagedStripe();
            $remoteDetails = $stripe->getAccount(Auth::user());
        }

        $details = [];
        $parameters = [
            'first_name', 'last_name', 'address_line_one', 'address_line_two',
            'address_city', 'address_county', 'address_postal_code', 'date_of_birth',
            'account_holder_name', 'account_number', 'sort_code'
        ];

        foreach ($parameters as $parameter) {
            if ($request->old($parameter) !== null) {
                $details[$parameter] = $request->old($parameter);
            } elseif (isset($remoteDetails[$parameter])) {
                $details[$parameter] = $remoteDetails[$parameter];
            } else {
                $details[$parameter] = '';
            }
        }

        if (!empty($details['date_of_birth']) && strpos($details['date_of_birth'], '/') !== false) {
            try {
                $details['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $details['date_of_birth'])->format('d/m/Y');
            } catch (\Exception $ex) {
                $details['date_of_birth'] = '';
            }
        }

        return view('pages.payment.bank', [
            'first_setup' => $firstSetup,
            'details' => $details
        ]);
    }

    /**
     * Save users bank details and mark them as a seller.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postBankDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address_line_one' => 'required',
            'address_city' => 'required',
            'address_county' => 'required',
            'address_postal_code' => 'required',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'account_number' => 'required_with:sort_code,account_holder_name',
            'sort_code' => 'required_with:account_number,account_holder_name',
            'account_holder_name' => 'required_with:sort_code,account_number',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error_message', $validator->errors()->first());
        }

        $stripe = new ManagedStripe();
        $user = Auth::user();

        //Create the stripe account if required

        if (!$user->stripeAccount) {

            $created = $stripe->createAccount($user, $request->ip(), $request->input('first_name'), $request->input('last_name'));

            $user = $user->fresh();

            if (!$created) {
                return redirect()->back()->withInput()->with('error_message', 'Unable to save bank account details.');
            }
        }


        $user = $user->fresh();

        $addressUpdate = $stripe->updatePersonalDetails($user,
            Carbon::createFromFormat('d/m/Y', $request->input('date_of_birth')),
            $request->input('address_line_one'),
            $request->input('address_line_two'),
            $request->input('address_city'),
            $request->input('address_county'),
            'GB',
            $request->input('address_postal_code'),
            $request->input('first_name'),
            $request->input('last_name')
        );

        if ($request->has('account_number') && empty($request->input('account_number'))) {
            $bankUpdate = $stripe->updateBankAccount(
                $user,
                $request->input('account_number'),
                $request->input('sort_code'),
                $request->input('account_holder_name')
            );

            $user->stripe_connected = true;
            $user->save();
        } else {
            $bankUpdate = true;
        }

        if (!$bankUpdate || !$addressUpdate) {
            return redirect()->back()->withInput()->with('error_message', 'Unable to save bank account details.');
        }

        $user->stripe_connected = true;
        $user->save();

        return redirect()->intended($request->session()->pull('bank_redirect'))->with('success_message', 'Payment details saved successfully.');
    }
}