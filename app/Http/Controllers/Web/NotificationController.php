<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 31/08/16
 * Time: 08:24
 */

namespace App\Http\Controllers\Web;

use App\Notification;
use App\NotificationWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends BaseController
{
    /**
     * Page to view a list of notifications
     *
     * @return \Illuminate\View\View
     */
    public function view()
    {
        $user = Auth::user();
        $user->notifications_viewed_at = Carbon::now();
        $user->save();

        $data['notifications'] = NotificationWeb
            ::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $data['header_notification_count'] = 0;

        return view('pages.notifications', $data);
    }

    /**
     * Load more notifications (paginated)
     *
     * @param string $type
     * @return mixed
     */
    public function ajaxLoad($type)
    {
        $notifications = NotificationWeb
            ::where('user_id', Auth::user()->id);

        if ($type != 'all') {
            $notifications->where('notification_type', $type);
        }

        $notifications = $notifications
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        if ($notifications->count() == 0) {
            //Send no content header if there are no results
            return response('', 204);
        }

        return view('partials.notification-list', ['notifications' => $notifications]);
    }

    /**
     * Delete a notification
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxDelete($id)
    {
        $notification = Notification::where('user_id', Auth::user()->id)->where('id', $id)->first();

        if (!$notification) {
            return response()->json(['status' => false]);
        }

        $notification->delete();

        return response()->json(['status' => true]);
    }

    /**
     * Store when the notifications were last checked
     */
    public function ajaxViewed()
    {
        $user = Auth::user();
        $user->notifications_viewed_at = Carbon::now();
        $user->save();

        return response()->json(['status' => true]);
    }
}