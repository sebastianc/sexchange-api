<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 18/01/16
 * Time: 16:29
 */

namespace App\Http\Controllers\API\V1;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    public function api_response($body, $success = true, $head_data = [], $status = 200){

        $response_head = [
            'status' => ($success ? 'OK' : 'Failed'),
        ];

        //Merge additional head items in
        $result = array_replace($response_head, $head_data);
        $result['count'] = count($body);

        if(array_key_exists('data', $body)){
            $result['count'] = count($body['data']);
        }
        $payload = [];
        $payload['result'] = $result;

        if(!empty( $body )){
            $payload['response_payload'] = $body;
        }
        return Response::json($payload, $status);
    }
}