<?php

namespace App\Http\Controllers\API\V1;

use App\Faq;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class FaqController extends ApiController
{
    public function viewInfo () {
        $faq = Faq::all()->groupBy('subject');
        return parent::api_response($faq, true, ['return' => 'FAQs'], 200);
    }

    public function createFaq (Request $request) {
        $data = $request->all();

        $validate = Validator::make($data, [
            'subject' => 'required',
            'question' => 'required',
            'answer' => 'required'
        ]);

        if ($validate->fails()) {
            return parent::api_response([], true, ['error' => 'Validator Failed'], 200);
        }

        $faq = Faq::create([
            'subject' => $data['subject'],
            'question' => $data['question'],
            'answer' => $data['answer']
        ]);

        if ($faq->save()) {
            return parent::api_response($faq, true, ['success' => 'FAQ Saved Successfully'], 200);
        }

    }
}
