<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 12:19
 */

namespace App\Http\Controllers\API\V1;


use App\Comment;
use App\Shoe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CommentController extends ApiController
{
    public function post($id){
        $shoe = Shoe::whereSold(0)->find($id);
        $text = stripslashes(Input::get('comment_text'));
        if(!$text){
            return parent::api_response([$id], true, ['error' => 'Please enter a comment'], 400);
        }
        if($shoe){
            $comment = new Comment;
            $comment->shoe_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->text = $text;
            if($comment->save()){
                if($comment->user_id !== $shoe->seller){
                    $pushData = [
                        'notification_id' => $comment->id,
                        'notification_type' => 'Comment',
                        'channel' => 'user_'.$shoe->seller,
                        'recipient' => $shoe->seller,
                        'message' => 'Someone commented on your shoe!',
                        'snippet' => substr($text, 0, 100),
                        'extra_id' => $shoe->id,

                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                }
                return parent::api_response($comment, true, ['success' => 'comment sent'], 200);
            }else{
                return parent::api_response([$id], true, ['error' => 'There was an error posting your comment' ], 500);
            }
        }else{
            return parent::api_response([$id], true, ['error' => 'This shoe is no longer available'], 400);
        }
    }
    public function get($id){
        $shoe = Shoe::find($id);
        if($shoe){
            $comments = Comment::where('shoe_id', $id)->with('user')->orderBy('created_at', 'desc')->paginate(10);
            return parent::api_response($comments, true, ['return' => 'shoe comments'], 200);
        }else{
            return parent::api_response([$id], true, ['error' => 'Shoe not found'], 404);
        }
    }



}