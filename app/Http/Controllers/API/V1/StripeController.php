<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 13/05/16
 * Time: 10:23
 */

namespace App\Http\Controllers\API\V1;

use App\Purchase;
use App\StripeTransaction;
use App\StripeUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Stripe\Stripe;

class StripeController extends ApiController
{
    protected $stripe_key;

    function __construct() {
//        $this->stripe_key =  Stripe::setApiKey(ENV('STRIPE_SK'));
    }
    function connectLink(){
        $url = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=".ENV('STRIPE_CLIENT_ID')."&scope=read_write";
        return parent::api_response(['connect url' => $url], true, ['return' => 'stripe connect url'], 200);
    }

     function redirect(){
        if(Input::get('error')){
            return redirect()->away('soleexchange://stripe_auth?connected=0&error='.Input::get('error').'&error_description='.Input::get('error_description'));
        }
        if(Input::get('code')){
            return redirect()->away('soleexchange://stripe_auth?scope=read_write&connected=1&code='.Input::get('code'));
        }
    }

     function connectAccount(){
        try{
            $code = Input::get('code');
            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'client_id' => ENV('STRIPE_CLIENT_ID'),
                'code' => $code,
                'client_secret' => ENV('STRIPE_SK')
            );

            $req = curl_init('https://connect.stripe.com/oauth/token');
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true );
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);


            if(isset($resp['error'])){
                return parent::api_response($resp, false, ['error' => 'stripe account connect failed'], 500);
            }else{
                $user = Auth::user();
                $user->stripe_connected = 1;
                $user->save();
                $stripe = new StripeUser;
                $stripe->user_id = Auth::user()->id;
                $stripe->access_token = $resp['access_token'];
                $stripe->refresh_token = $resp['refresh_token'];
                $stripe->token_type = $resp['token_type'];
                $stripe->stripe_publishable_key = $resp['stripe_publishable_key'];
                $stripe->stripe_user_id = $resp['stripe_user_id'];
                if($stripe->save()){
                    return parent::api_response([], true, ['success' => 'stripe account connected'], 200);
                }
            }
        }catch (\Exception $e){
            return parent::api_response($e->getMessage(), false, ['error' => 'stripe account connect failed'], 500);

        }
    }

    function test(){
        $purchase = 17;
        $stripe_user = 2;
        return $this->charge($purchase, $stripe_user);
    }

     function charge($purchase_id, $stripe_user_id, $token){
        try{
            \Stripe\Stripe::setApiKey(env('STRIPE_SK'));
            $stripe_user_obj = StripeUser::find($stripe_user_id);
            $purchase_obj = Purchase::find($purchase_id);
            $charge = \Stripe\Charge::create(array(
                'amount' => ($purchase_obj->price)*100,
                'currency' => 'gbp',
                'source' => $token
            ), array('stripe_account' => $stripe_user_obj->stripe_user_id));

            $transaction = new StripeTransaction;
            $transaction->purchase_id = $purchase_id;
            $transaction->data = json_encode($charge);

            $purchase_obj->payment_sent = 1;
            $purchase_obj->save();
            if($transaction->save()){
                return $charge;
            }
        }catch (\Exception $e){
            return ['error' => $e->getMessage()];
        }
    }

//    function token(){
//        //Test token generator
//        \Stripe\Stripe::setApiKey(env('STRIPE_SK'));
//
//        $token = \Stripe\Token::create(array(
//            "card" => array(
//                "number" => "4242424242424242",
//                "exp_month" => 1,
//                "exp_year" => 2017,
//                "cvc" => "314"
//            )
//        ));
//        return $token;
//
//    }
}