<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 24/08/16
 * Time: 13:36
 */

namespace App\Http\Controllers\API\V1;


use App\TailoringSelection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class TailoringController extends ApiController
{
    public function tailoring(){
        $selections = Input::get('brands');
        $exists = TailoringSelection::where('user_id', Auth::user()->id)->first();

        if($exists){
            $exists->brands = $selections;
            $exists->save();
            return parent::api_response( $exists, true, ['success' => 'tailoring saved'], 200);

        }else{
            $tailoring = new TailoringSelection;
            $tailoring->user_id = Auth::user()->id;
            $tailoring->brands = $selections;
            $tailoring->save();
            return parent::api_response( $tailoring, true, ['success' => 'tailoring saved'], 200);
        }
    }

}