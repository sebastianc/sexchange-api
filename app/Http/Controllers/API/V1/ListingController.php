<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:16
 */

namespace App\Http\Controllers\API\V1;


use App\Color;
use App\ColorsShoe;
use App\Shoe;
use App\ShoeImage;
use App\ShoeView;
use App\StripeUser;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ListingController extends ApiController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';


    public function viewAll () {
        $shoes = Shoe::with('brand_info', 'color', 'sizes', 'comments', 'likes', 'images', 'seller', 'likedBy')->orderBy('created_at', 'desc')->where('seller', Auth::user()->id)->paginate(10);
        return parent::api_response($shoes, true, ['success' => 'Users Listings'], 200);
    }

    protected function upload(){
        $info = Input::only('name', 'description', 'cost', 'condition', 'delivery', 'size', 'visibility', 'brand', 'min_offer', 'wanted');

        if (!Auth::user()->stripe_connected) {
            return parent::api_response([], false, ['error' => 'Please enter bank details to upload a shoe.'], 400);
        }

        $wanted_id = Input::get('wanted_id');
        $shoe = new Shoe;
        $shoe->seller = Auth::user()->id;
        $shoe->slug = str_replace(' ', '-', $info['name']);
        foreach($info as $field => $val){
            $shoe->$field = $val;
        }
        try{
            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            if($images['image_1']){
                $shoe->save();
                if(isset($wanted_id)){
                    $wanted = Shoe::find($wanted_id);
                    if($wanted){
                        $pushData = [
                            'notification_id' => $shoe->id,
                            'notification_type' => 'Wanted',
                            'channel' => 'user_'.$wanted->seller,
                            'recipient' => $wanted->seller,
                            'message' => 'Someone is selling a shoe you want!',
                            'snippet' => $shoe->name,
                            'extra_id' => $shoe->id,
                        ];
                        $push = new NotificationController;
                        $push->sendNotification($pushData);
                    }
                }
            }else{
                return parent::api_response([], false, ['error' => 'Please upload at least one image'], 400);
            }
            foreach($images as $index => $image){
                        if($image) {
                            $file = $image;
                            $newName = 'shoe_upload_'.$shoe->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                            $folder = 'user_'.Auth::user()->id;
                            $file->move($this->uploadsFolder.$folder, $newName);
                            Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                            $image = new ShoeImage;
                            $image->shoe_id = $shoe->id;
                            $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                            $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                            $image->base_url = env('BASE_URL');
                            $image->original_filename = $file->getClientOriginalName();
                            try{
                                $image->save();
                            }catch (QueryException $e){
                                return parent::api_response($e->errorInfo, false, ['error' => 'image upload failed'], 500);
                            }
                        }
                    }

//            dd(Color::find(Input::get('color')));
            $colors = Input::get('colors');
            if($colors){
                $colors = json_decode($colors);
                foreach($colors as $color){
                    $shoe->colors()->attach($color);
                }
            }

            return parent::api_response($shoe, true, ['success' => 'shoe uploaded'], 200);
        }catch (QueryException $e){
                return parent::api_response($e->errorInfo, false, ['error' => 'shoe upload failed'], 500);
            }
        }

    protected function removeImages(){
        $imageIDs = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
        $shoeID = Input::get('shoe_id');
        $shoe = Shoe::find($shoeID)->load('images');

        foreach($imageIDs as $i => $imageID) {
            if (!is_null(ShoeImage::find($imageID))) {
                ShoeImage::find($imageID)->delete();
//                $img->destroy();
            }
        }

        return parent::api_response($shoe, true, ['success' => 'Successfully Deleted Shoe Image'], 200);

    }

    protected function edit(){

        $info = Input::only('id','name', 'description', 'cost', 'condition', 'delivery', 'size', 'visibility', 'brand', 'min_offer');
        $shoe = Shoe::find($info['id']);
        foreach($info as $field => $val){
            $shoe->$field = $val;
        }
        try{
            $shoe->save();
            //Wipe deleted images
            $imageIDs = Input::get('deleted_images');
            if($imageIDs){
                $imageIDs = json_decode($imageIDs);
                foreach($imageIDs as $i => $imageID) {
                    if (!is_null(ShoeImage::find($imageID))) {
                        $image = ShoeImage::find($imageID);
                        File::delete($image->url);
                        $image->delete();
                    }
                }
            }

            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            

            //TODO: Set up correct file sizes and URL's
            if($images){
                foreach($images as $index => $image){
                    if($image){
                        $file = $image;
                        $newName = 'shoe_upload_'.$shoe->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                        $folder = 'user_'.Auth::user()->id;
                        $file->move($this->uploadsFolder.$folder, $newName);
                        Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                        $image = new ShoeImage;
                        $image->shoe_id = $shoe->id;
                        $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                        $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                        $image->base_url = env('BASE_URL');
                        $image->original_filename = $file->getClientOriginalName();
                        try{
                            $image->save();
                        }catch (QueryException $e){
                            return parent::api_response($e->errorInfo, false, ['error' => 'image upload failed'], 500);
                        }
                    }
                }
            }

            $colors = Input::get('colors');
            if($colors){
                ColorsShoe::where('shoe_id', $info['id'] )->delete();
                $colors = json_decode($colors);
                foreach($colors as $color){
                    $shoe->colors()->attach($color);
                }
            }

            return parent::api_response($shoe, true, ['success' => 'shoe uploaded'], 200);
        }catch (QueryException $e){
            return parent::api_response($e->errorInfo, false, ['error' => 'shoe upload failed'], 500);
        }
    }

    public function addViews(){
        $data = Input::only('shoes');
        $data = json_decode($data['shoes']);

        foreach ($data as $shoeId) {
            $shoe = Shoe::find($shoeId);
            if($shoe){
                $view = New ShoeView;
                $view->user_id = Auth::user()->id;
                $view->shoe_id = $shoeId;
                if(!$view->save()){
                    return parent::api_response([], false, ['error' => 'adding views failed'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'shoe not found'], 404);
            }
        }
        return parent::api_response([], true, ['success' => 'views saved'], 200);
    }

    /**
     * Mark a shoe as sold
     * @param $id
     * @return mixed
     */
    protected function markSold($id){
        $shoe = Shoe::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($shoe){
            $shoe->marked_as_sold = 1;
            $shoe->sold = 1;
            if($shoe->save()){
                return parent::api_response($shoe, true, ['success' => 'shoe marked sold'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'mark sold failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Shoe not found'], 404);
        }
    }
    protected function markAvailable($id){
        $shoe = Shoe::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($shoe){
            $shoe->marked_as_sold = 0;
            $shoe->sold = 0;
            if($shoe->save()){
                return parent::api_response($shoe, true, ['success' => 'shoe marked available'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'mark available failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Shoe not found'], 404);
        }
    }


    protected function delete($id){
        $shoe = Shoe::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($shoe){
            if($shoe->delete()){
                return parent::api_response($shoe, true, ['success' => 'shoe deleted'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'delete failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Shoe not found'], 404);
        }
    }

//    SEEDER
    function seedImages(){
        $shoes = Shoe::get();
        foreach($shoes as $shoe){
            $image = new ShoeImage;
            $image->shoe_id = $shoe->id;
            $image->url ='uploads/users/test/'.mt_rand(1,5).'.jpg';
            $image->public_url ='images/original/users/test/'.mt_rand(1,5).'.jpg';
            $image->base_url = 'http://se.dreamrserve.uk';
            $image->original_filename = 'test_image.jpg';
            $image->save();
        }

        return 'SEEDED';
    }

}