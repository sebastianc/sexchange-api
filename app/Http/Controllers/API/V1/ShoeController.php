<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/02/16
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;


use App\Follow;
use App\Notification;
use App\ReportedShoe;
use App\Shoe;
use App\ShoeLike;
use App\TailoringSelection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ShoeController extends ApiController
{
    function viewAll(){

        $shoes = Shoe
            ::where('seller', '!=' , Auth::user()->id)
            ->where('wanted', 0)
            ->with('views', 'brand_info', 'colors', 'sizes', 'images', 'seller', 'likedBy')
            ->where('sold', 0)
            ->whereNull('deleted_at')
            ->whereMarkedAsSold(0)
            ->orderBy('created_at', 'desc')
            ->paginate(30);


        return parent::api_response($shoes, true, ['return' => 'all shoes'], 200);
    }

    /**
     * Main feed endpoint for seeing all shoes by followed users
     * @return mixed
     */
    function feed(){
        $following = Follow::where('user_id', Auth::user()->id)->get();
        $ids = $following->pluck('follows');

        //Sole account
        $ids->push(env('GEORGE_ID', 1));

        //If user has set tailoring options then alter feed to show suggested/tailored posts
        if($tailoring = TailoringSelection::whereUserId(Auth::user()->id)->first()){

            $shoes = Shoe::where('seller', '!=' , Auth::user()->id)
                ->where('wanted', 0)
                ->whereIn('seller', $ids)
                ->with('brand_info', 'likes', 'images', 'seller', 'likedBy', 'comments')
                ->where('sold', 0)
                ->where('reserved', 0)
                ->whereNull('deleted_at')
                ->whereMarkedAsSold(0)
                ->orderBy('created_at', 'DESC')
                ->paginate(8);

            $exclude = $shoes->pluck('id');
            $brands = json_decode($tailoring->brands);
            $tailored = Shoe::where('seller', '!=' , Auth::user()->id)
                ->where('wanted', 0)
                ->whereIn('brand', $brands)
                ->whereNotIn('id', $exclude)
                ->with('brand_info', 'likes', 'images', 'seller', 'likedBy', 'comments')
                ->where('sold', 0)
                ->where('reserved', 0)
                ->whereNull('deleted_at')
                ->whereMarkedAsSold(0)
                ->orderBy('created_at', 'DESC')
                ->paginate(2);


            if ($shoes->count() != 0) {
                $chunks = $shoes->chunk(($shoes->count() / 2));

                //Chunks up original collection and adds tailored shoes at 5 and 10 (Could possibly be improved)
                if ($tailored_collection = $chunks->first()) {
                    if ($tailored->first()) {
                        $tailored_collection->add($tailored->first());
                    }
                    if (count($chunks) > 1) {
                        foreach ($chunks->last() as $obj) {
                            $tailored_collection->add($obj);
                        }
                        if ($tailored->count() > 1) {
                            $tailored_collection->add($tailored->last());
                        }
                    }
                } else {
                    $tailored_collection = null;
                }
            } else {
                $tailored_collection = $tailored;
            }


        }else{
            $shoes = Shoe
                ::where('seller', '!=' , Auth::user()->id)
                ->where('wanted', 0)
                ->whereIn('seller', $ids)
                ->with('brand_info', 'images', 'seller', 'likedBy', 'comments')
                ->where('sold', 0)
                ->where('reserved', 0)
                ->whereNull('deleted_at')
                ->whereMarkedAsSold(0)
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
        }

        $wanted = Shoe::where('seller', '!=' , Auth::user()->id)->where('wanted', 1)->orderByRaw('RAND()')->first();
        $shoes = $shoes->toArray();


        //Update pagination values to account for extra 2 objects per page
        if(isset($tailored_collection)){
            $shoes['data'] = $tailored_collection->toArray();
            $shoes['per_page'] = (int) $shoes['per_page'] + 2;
            $shoes['to'] = (int) $shoes['to'] + 2;
        }

        $shoes['wanted'] = $wanted;
        return parent::api_response($shoes, true, ['return' => 'all shoes'], 200);
    }


    function get($id){
        $shoe = Shoe::with('brand_info', 'colors', 'sizes', 'comments', 'images', 'seller', 'likedBy')->where('id', $id)->paginate(1);
        return parent::api_response($shoe, true, ['return' => 'selected shoe'], 200);
    }
    function getLikes($id){
        $likes = ShoeLike::where("shoe_id", $id)->with('user')->get();
        $liked_by = ShoeLike::where('user_id', Auth::user()->id)->where('shoe_id', $id)->first();
        if($liked_by){
            $liked_by = true;
        }else{
            $liked_by = false;
        }
        return parent::api_response($likes, true, ['return' => 'likes for selected shoe', 'liked_by' => $liked_by], 200);
    }
        public function search(){
        $filters = Input::only('colours', 'brands', 'sizes', 'wanted', 'price', 'condition', 'term');
        $sort = Input::get('sort');

        $shoes = Shoe::where('seller', '!=' , Auth::user()->id)->where('wanted', 0)->with('brand_info', 'colors', 'sizes', 'comments', 'images', 'seller', 'likedBy');
            if (isset($filters['brands'])) {
                $shoes->whereHas('brand_info', function ($query) use ($filters) {
                    if (isset($filters['brands'])) {
                        $query->whereIn('id', json_decode($filters['brands']));
                    }
                });
            }
            if (isset($filters['colours'])) {
                $shoes->whereHas('colors', function ($query) use ($filters) {
                    if (isset($filters['colours'])) {
                        $query->whereIn('color_id', json_decode($filters['colours']));
                    }
                });
            }
            if (isset($filters['sizes'])) {
                $shoes->whereHas('sizes', function ($query) use ($filters) {
                    if (isset($filters['sizes'])) {
                        $query->whereIn('id', json_decode($filters['sizes']));
                    }
                });
            }
            if(isset($filters['condition'])){
                $shoes->where('conditions', '>=', $filters['condition']);
            }
            if(isset($filters['price'])){
                $prices = json_decode($filters['price']);
                if(count($prices) > 1){
                    $shoes->whereBetween('cost', $prices);
                }else{
                    $shoes->where('cost', '>=', $prices[0]);
                }
            }

            $shoes->where('sold', 0)
                ->whereNull('deleted_at')
                ->where('reserved', 0)
                ->whereMarkedAsSold(0);

        if(isset($filters['wanted'])){
            if($filters['wanted'] === 'true'){
                $shoes->where('wanted', 1);
            }else{
                $shoes->where('wanted', 0);
            }
        }
        if(isset($filters['term'])){
            $shoes->search($filters['term']);
        }

        if (empty($sort)) {
            $sort = 'recent';
        }

        switch ($sort){
            case 'price_low_high':
                if(!$filters['term']){
                    $shoes->orderBy('cost', 'asc');
                }else{
                    $query = $shoes->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'ASC']);
                    $shoes = $shoes->setQuery($query);
                }

                break;
            case 'price_high_low':
                if(!$filters['term']){
                    $shoes->orderBy('cost', 'desc');
                }else{
                    $query = $shoes->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'DESC']);
                    $shoes = $shoes->setQuery($query);
                }
                break;
            case 'views':
                $date = Carbon::now()->subMonth();
                $shoes = $shoes
                    ->leftJoin('shoe_views', 'shoes.id', '=', 'shoe_views.shoe_id')
                    ->select(DB::raw('shoes.*, count(case when shoe_views.created_at > "'.$date.'" then 1 else 0 end) as recent_views'))
                    ->groupBy('shoes.id')
                    ->orderBy('recent_views' , 'desc');
                if($filters['term']){
                    $query = $shoes->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'recent_views', 'direction' => 'DESC']);
                    $shoes = $shoes->setQuery($query);
                }
                break;
            case 'recent':
                if(!$filters['term']){
                    $shoes->orderBy('created_at', 'desc');
                }else{
                    $query = $shoes->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'created_at', 'direction' => 'DESC']);
                    $shoes = $shoes->setQuery($query);
                }
                break;
        }

        return parent::api_response($shoes->paginate(30), true, ['return' => 'search for '.$filters['term']], 200);
    }

    public function like($id){
        $shoe = Shoe::with('likedBy')->whereSold(0)->find($id);
        if($shoe){
            $exists = ShoeLike::where('shoe_id', $id)->where('user_id', Auth::user()->id)->get();
            if(!$exists->count()){
                $like = new ShoeLike;
                $like->shoe_id = $id;
                $like->user_id = Auth::user()->id;
                if($like->save()){
                    if($like->user_id != $shoe->seller){
                        $pushData = [
                            'notification_id' => $like->id,
                            'notification_type' => 'ShoeLike',
                            'channel' => 'user_'.$shoe->seller,
                            'recipient' => $shoe->seller,
                            'message' => 'Someone liked your shoe!',
                            'snippet' => $shoe->name,
                            'extra_id' => $shoe->id,


                        ];
                        $push = new NotificationController;
                        $push->sendNotification($pushData);
                    }
                    return parent::api_response($shoe, true, ['success' => 'liked shoe '.$id], 200);
                }
            }else{
                return parent::api_response($shoe, true, ['error' => 'You\'ve already liked that shoe'], 400);
            }

        }else{
            return parent::api_response([$id], true, ['error' => 'Shoe not found'], 404);
        }
    }

    public function unlike($id)
    {
        $shoe = Shoe::whereSold(0)->find($id);
        if ($shoe) {
            $exists = ShoeLike::where('shoe_id', $id)->where('user_id', Auth::user()->id)->first();
            if ($exists->count()) {
                $notification = Notification::where('sender_id', Auth::user()->id)->where('notification_type', 'ShoeLike')->where('notification_id', $exists->id)->first();
                if ($exists->delete() && $notification->delete()) {
                    return parent::api_response($shoe, true, ['success' => 'unliked shoe '.$id], 200);
                } else {
                    return parent::api_response($shoe, true, ['error' => 'Error deleting shoe'], 500);
                }

            } else {
                return parent::api_response([$id], true, ['error' => 'You have not liked that shoe'], 400);
            }
        }else {
            return parent::api_response([$id], true, ['error' => 'Shoe not found'], 404);
        }
    }

    function reportShoe($id){
        $shoe = Shoe::whereSold(0)->find($id);
        $exists = ReportedShoe::where('shoe_id', $id)->where('user_id', Auth::user()->id)->first();
        if($shoe && !$exists){
            $report = New ReportedShoe;
            $report->user_id = Auth::user()->id;
            $report->shoe_id = $id;
            if($report->save()){
                $count = ReportedShoe::where('shoe_id', $id)->get();
                if($count->count() > 50){
                    $shoe->delete();
                }
                return parent::api_response($report, true, ['success' => 'reported shoe '.$id], 200);
            } else {
                return parent::api_response([], true, ['error' => 'Error reporting shoe'], 500);
            }
        }else {
            return parent::api_response([$id], true, ['error' => 'Can\'t report that shoe'], 404);
        }
    }




}