<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 17:03
 */

namespace App\Http\Controllers\API\V1;

use App\Follow;
use App\Purchase;
use App\Review;
use App\Shoe;
use App\ShoeLike;
use App\User;
use App\UserSearchable;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProfileController extends ApiController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';

    function viewShoes(){
        $shoes = Shoe::where('seller', Auth::user()->id)->whereNull('deleted_at')->where('sold', 0)->where('wanted', 0)->with('brand_info', 'colors', 'sizes', 'comments', 'likes', 'images', 'seller')->orderBy('created_at', 'desc')->paginate(10);
        
        return parent::api_response($shoes, true, ['return' => 'shoes by user'], 200);
    }

    function viewWantedShoes(){
        $shoes = Shoe::where('seller', Auth::user()->id)->whereNull('deleted_at')->where('sold', 0)->where('wanted', 1)->with('brand_info', 'colors', 'sizes', 'comments', 'likes', 'images', 'seller')->orderBy('created_at', 'desc')->paginate(10);

        return parent::api_response($shoes, true, ['return' => 'shoes by user'], 200);
    }

    function viewSoldShoes(){
        $shoes = Shoe::with('reviewBySeller', 'reviewByBuyer')->whereNull('deleted_at')->where('seller', Auth::user()->id)->where('sold', 1)->where('wanted', 0)->with('brand_info', 'colors', 'sizes', 'comments', 'likes', 'images', 'seller', 'reviewByBuyer', 'reviewBySeller', 'purchase')->orderBy('created_at', 'desc')->paginate(10);
        return parent::api_response($shoes, true, ['return' => 'shoes by user'], 200);
    }

    function viewPurchased(){
//        $purchases = Purchase::with('shoe')->where('buyer_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
        $purchases = Shoe::with('purchase', 'reviewBySeller', 'reviewByBuyer')
            ->where('payment_sent', 1)
            ->leftJoin('purchases', 'shoes.id','=', 'purchases.shoe_id')
            ->select('shoes.*')
            ->whereHas('purchase', function($q){
            $q->where('buyer_id', Auth::user()->id);
        })->orderBy(DB::raw('purchases.created_at'), 'desc')->paginate(10);

        return parent::api_response($purchases, true, ['return' => 'shoes purchased by user'], 200);

    }

    function buildProfile(){
        //TODO: Remove extra queries and leave user only
        $user = UserSearchable::with('follows', 'followers')->find(Auth::user()->id);
        $follow = Follow::where('user_id', $user->id)->get();
        $followers = Follow::where('follows', $user->id)->get();
        $reviews_seller = Review::where('seller_id', $user->id)->where('is_seller', 1)->get();
        $reviews_buyer = Review::where('seller_id', $user->id)->where('is_seller', 0)->get();
        

        if (isset($reviews_seller)) {
            $user['reviews_as_seller'] = $reviews_seller;
        }
        if (isset($reviews_buyer)) {
            $user['reviews_as_buyer'] = $reviews_buyer;
        }
        if (isset($follow)) {
            $user['following'] = $follow;
        }
        if (isset($followers)) {
            $user['followers'] = $followers;
        }
        $user['phone_number'] = $user->phone();
        $user['user_email'] = $user->getEmail();

        return $user;
    }
    public function viewInfo(){
        return parent::api_response($this->buildProfile(), true, ['return' => 'user info'], 200);
    }

    function reviewsAsBuyer(){
        $reviews = Review::with('user')
            ->where('user_id', Auth::user()->id)
            ->where('is_seller', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as buyer'], 200);
    }

    function reviewsAsSeller(){
        $reviews = Review::with('user')
            ->where('seller_id', Auth::user()->id)
            ->where('is_seller', 0)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as buyer'], 200);
    }

    function followers(){
        $followers = Follow::where('follows', Auth::user()->id)->with('users')->paginate(10);
        return parent::api_response($followers, true, ['return' => 'followers'], 200);

    }

    function following(){
        $followers = Follow::where('user_id', Auth::user()->id)->with('follows')->paginate(10);
        return parent::api_response($followers, true, ['return' => 'following'], 200);

    }

    function likes(){
        $likes = ShoeLike::where('user_id', Auth::user()->id)->with('shoe')->paginate(10);
        return parent::api_response($likes, true, ['return' => 'following'], 200);

    }




    function editProfile() {
        $data = Input::only('bio', 'refunds_given', 'city', 'county', 'paypal_email', 'password', 'image', 'first_name', 'last_name', 'username', 'phone');

        $user = User::where('id', Auth::user()->id)->first();

        if ($user) {

            $validator = Validator::make($data, [
                'bio' => 'string',
                'refunds_given' => 'boolean',
                'city' => 'string',
                'county' => 'string',
                'paypal_email' => 'email|min:4',
                'password' => 'min:8',
                'first_name' => 'string',
                'last_name' => 'string',
                'image' => 'image',
                'username' => 'string|unique:users',
                'phone' => 'string'
            ]);

            if ($validator->fails()) {
                return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            } else {
                // Only update non-null keys
                foreach ($data as $k => $v) {
                    if (!is_null($v)) {
                        if ($k == 'image') {
                            // upload profile image and delete the old one IF exists
                            $file = $v;
                            $newName = 'user_profile_'.Auth::user()->id.'_'.md5(time()).'.'.$file->getClientOriginalExtension();
                            $folder = 'user_'.Auth::user()->id;
                            $file->move($this->uploadsFolder.$folder, $newName);
                            Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                            $user[$k] = env('BASE_URL').'/'.$this->uploadsFolder.$folder.'/'.$newName;
                        } else {
                            if($k == 'phone'){
                                $user->phone_verified = false;
                            }
                            $user[$k] = $v;
                        }
                    }
                }

                if ($user->save()) {
                    $updated = $this->buildProfile();
                    return parent::api_response($updated, true, ['return' => 'updated profile successfully'], 200);
                } else {
                    return parent::api_response(null, true, ['return' => 'problem updating profile'], 500);
                }
            }
        }
        return parent::api_response(null, true, ['return' => 'problem updating profile'], 500);
    }

}

