<?php

namespace App\Http\Controllers\API\V1;

use App\Color;
use Illuminate\Http\Request;

use App\Http\Requests;

class ColourController extends ApiController
{
    public function get() {
        $colours = Color::all();
        return parent::api_response($colours, true, ['success' => 'comment sent'], 200);
    }
}
