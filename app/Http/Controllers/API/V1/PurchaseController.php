<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:14
 */

namespace App\Http\Controllers\API\V1;


use App\Dispute;
use App\Events\PaymentEvent;
use App\Jobs\SendAdminDisputeEmail;
use App\Models\ManagedStripe;
use App\Purchase;
use App\Shoe;
use App\ShoeImage;
use App\ShoeOffer;
use App\StripeAccount;
use App\StripeUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends ApiController
{
    /**
     * Pay for a shoe after pressing buy.
     * @return mixed
     */
    protected function buy(){
        try{
            $purchase_id = Input::get('purchase_id');
            $stripe_token = Input::get('stripe_token');
            $purchase = Purchase::find($purchase_id);
            //If purchase exists
            if($purchase){
                $shoe = Shoe::find($purchase->shoe_id);
                $seller = User::findOrFail($shoe->seller);
                $buyer = Auth::user();

                //If shoe hasn't already been paid for
                if(!$purchase->payment_sent) {

                    $stripe = new ManagedStripe();
                    $stripeTransaction = $stripe->payToUser($seller, $purchase->price, $stripe_token, $purchase->id);

                    //If stripe returns error exit payment function
                    if (!$stripeTransaction) {
                        return parent::api_response([] , false, ['error' => 'payment failed'], 500);
                    } else {
                        $shoe->sold = 1;
                        $purchase->payment_sent = 1;
                        $purchase->status = 'paid';

                        $purchase->address_line1 = $stripeTransaction['source']['address_line1'];
                        $purchase->address_line2 = $stripeTransaction['source']['address_line2'];
                        $purchase->address_city = $stripeTransaction['source']['address_city'];
                        $purchase->address_zip = $stripeTransaction['source']['address_state'];
                        $purchase->address_country = $stripeTransaction['source']['address_country'];
                        $purchase->address_state = $stripeTransaction['source']['address_state'];

                        //shipping address
                        $purchase->ship_address_line1 = Input::get('ship_address_one');
                        $purchase->ship_address_line2 = Input::get('ship_address_two');
                        $purchase->ship_address_city = Input::get('ship_city');
                        $purchase->ship_address_postcode= Input::get('ship_postcode');
                        $purchase->ship_address_county = Input::get('ship_county');


                        //If payment has succeeded and purchase object is successfully updated then send push and return response
                        if($purchase->save() && $shoe->save()){
                            $img = ShoeImage::where('shoe_id', $shoe->id)->first();
                            $img_url = $img['base_url'].'/'.$img['public_url'];
                            $img_url  = str_replace('/original/', '/medium/', $img_url );
                            $pushData = [
                                'notification_id' => $purchase_id,
                                'notification_type' => 'Purchase',
                                'channel' => 'user_'.$seller->id,
                                'recipient' => $seller->id,
                                'message' => 'Your shoe has been sold!',
                                'snippet' => $shoe->name,
                                'extra_id' => $shoe->id,
                                'img' => $img_url,
                            ];
                            $push = new NotificationController;
                            $push->sendNotification($pushData);
                            $return = ['success' => 'transaction complete'];
                            return parent::api_response($return , true, ['success' => 'purchased shoe '. $shoe->id], 200);
                        }
                    }

                }else {
                    return parent::api_response([], false, ['error' => 'Shoe has already been sold.'], 400);
                }
            }else{
                return parent::api_response([], false, ['error' => 'Transaction has expired.'], 400);
            }
        } catch (\Exception $e){
            return parent::api_response(['error' => $e->getMessage() ], false, ['error' => 'Could not purchase shoe.'], 500);
        }


    }

    protected function markDispatched($id){
        $shoe = Purchase::where('shoe_id', $id)->with('shoe')->first();
        if(count($shoe)){
            $shoe->status = 'dispatched';
            if($shoe->save()){
                $img = ShoeImage::where('shoe_id', $id)->first();
                $img_url = $img['base_url'].'/'.$img['public_url'];
                $img_url  = str_replace('/original/', '/medium/', $img_url );
                $pushData = [
                    'notification_id' => $id,
                    'notification_type' => 'Dispatched',
                    'channel' => 'user_'.$shoe->buyer_id,
                    'recipient' => $shoe->buyer_id,
                    'message' => 'Marked as dispatched',
                    'snippet' => $shoe->shoe->name,
                    'extra_id' => $shoe->shoe->id,
                    'img' => $img_url,
                ];
                $push = new NotificationController;
                $push->sendNotification($pushData);
                return parent::api_response($shoe, true, ['success' => 'shoe marked as dispatched'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'error marking shoe dispatched'], 500);
            }
        }else {
            return parent::api_response([], false, ['error' => 'Can\'t find that shoe.'], 404);
        }
    }

    public function createPurchase() {
        $shoeID = Input::get('shoeID');
        $shoe = Shoe::whereNull('deleted_at')->whereMarkedAsSold(0)->findOrFail($shoeID);

        if (Purchase::where('shoe_id', $shoeID)->count() > 0) {
            if($purchase = Purchase::where('shoe_id', $shoeID)->where('buyer_id', Auth::user()->id)->first()){
                return parent::api_response($purchase, true, ['success' => 'purchased shoe '.$shoeID], 200);
            }else{
                return parent::api_response([], false, ['error' => 'shoe already purchased']);
            }
        }

        $offer = ShoeOffer::where('shoe_id', $shoeID)->where('user_id',Auth::user()->id)->where('accepted', 1)->first();
        $price = 0;
        if($offer){
            $price = $offer->amount;
        }
        $purchase = new Purchase;
        $purchase->shoe_id = $shoeID;
        $purchase->buyer_id = Auth::user()->id;
        $purchase->status = 'pending';
        $purchase->payment_sent = 0;
        $purchase->price = ($price?: $shoe->cost);
        $shoe->reserved = true;

        if($purchase->save() && $shoe->save()){
            return parent::api_response($purchase, true, ['success' => 'purchased shoe '.$shoeID], 200);
        }else{
            return parent::api_response([], false, ['error' => 'error purchasing shoe'], 500);
        }
    }

    function getReceipt($id){
        $purchase = Purchase::with('shoe' )->find($id);
        if($purchase && $purchase->buyer_id == Auth::user()->id){
            return parent::api_response($purchase, true, ['success' => 'shoe marked as dispatched'], 200);
        }else{
            return parent::api_response([], false, ['error' => 'You did not buy this shoe'], 400);
        }
    }

    public function openDispute(Request $request, $purchaseId)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|between:1,10000'
        ]);

        if ($validator->fails()) {
            return $this->api_response([], false, ['error' => 'message required'], 500);
        }

        $purchase = Purchase
            ::where('refunded', 0)
            ->where('shoe_id', $purchaseId)
            ->first();

        if (!$purchase) {
            return $this->api_response([], false, ['error' => 'purchase not found'], 400);
        }

        if ($purchase->dispute) {
            return $this->api_response([], false, ['error' => 'purchase already disputed'], 400);
        }

        $dispute = new Dispute();
        $dispute->message = $request->input('message');
        $dispute->purchase_id = $purchase->id;
        $dispute->disputer_id = Auth::user()->id;
        $dispute->save();

        $this->dispatch(new SendAdminDisputeEmail($dispute));

        return parent::api_response([], true, ['success' => 'dispute created'], 200);
    }

    public function error () {
        return 'error';
    }

    public function clearAbandonedPurchases(){
        $purchases = Purchase::where('payment_sent', 0)->where('created_at', '<', Carbon::now()->subMinutes(15))->get();
        foreach($purchases as $purchase){
            $shoe = Shoe::find($purchase->shoe_id);
            $shoe->reserved = 0;
            $shoe->save();
            $purchase->delete();
        }
    }

    function redirect(){
        if(Input::get('error')){
            return redirect()->away('soleexchange://stripe_auth?connected=0&error='.Input::get('error').'&error_description='.Input::get('error_description'));
        }
        if(Input::get('code')){
            return redirect()->away('soleexchange://stripe_auth?scope=read_write&connected=1&code='.Input::get('code'));
        }
    }
}