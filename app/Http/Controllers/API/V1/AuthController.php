<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 18/01/16
 * Time: 16:51
 */

namespace App\Http\Controllers\API\V1;
use App\Brand;
use App\Color;
use App\Follow;
use App\ShoeSize;
use App\User;
use App\UserSearchable;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookRequest;
use Guzzle\Http\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Sofa\Eloquence\Tests\SearchableBuilderUserStub;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends ApiController

{
    use ThrottlesLogins, AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;


    /**
     * Login user
     * @param null $credentials
     * @return mixed
     */
    public function login( Request $request, $credentials = NULL)
    {
        // grab credentials from the request
        if(!$credentials){
            $credentials = Input::only('email', 'password');
        }
        try {
            //If too many login attempts block IP
            if ($this->hasTooManyLoginAttempts($request)) {
                return $this->api_response([], false, ['error' => 'Too many failed login attempts'], 401);
            }
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $user = User::where('email', $credentials['email'])->where('parse_id', '!=', 'null')->where('initial_pass_reset', 0)->first();
                if($user){
                    return $this->api_response(['parse_user_login' => true ], false, ['error' => 'Please reset you\'re password.'], 403);
                }
                //Failed so increment attempts
                $this->incrementLoginAttempts($request);
                return $this->api_response(['attempts remaining' => $this->retriesLeft($request)], false, ['error' => 'Incorrect email or password'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->api_response([], false, ['error' => 'token error'], 500);
        }
        // all good so return the token and reset login attempts
        $this->clearLoginAttempts($request);
        $user = UserSearchable::with('follows', 'followers')->find(Auth::user()->id);
        $user['phone_number'] = $user->phone();
        return $this->api_response(['token' =>$token, 'user' => $user] , true, ['Success' => 'logged in'], 200);

    }

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = Input::only('email', 'password', 'username');


        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'username' => 'required|min:4|max:36|unique:users'
        ]);
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
        } else {
            $new_user = new User;
            $new_user->email = $data['email'];
            $new_user->password = bcrypt($data['password']);
            $new_user->username = $data['username'];

            if($new_user->save()) {
                //TODO: CHANGE TO GEORGE'S USER ID
                $follow = new Follow;
                $follow->user_id = $new_user->id;
                $follow->follows = 1;
                $follow->save();
                $request = new Request();
                $request->merge($data);
                return $this->login($request);
            }
        }
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout(){
        if(!Auth::logout()){
            return $this->api_response([], true, ['Success' => 'logged out'], 200);
        }
    }

    /**
     * Reset password
     * @return mixed
     */
    public function reset(){
        $email = stripslashes(Input::get('email'));
        $user = User::where('email', $email)->first();

        if($user){
            $raw_pass = str_random(10);
            $new_pass = bcrypt($raw_pass);
            $user->password = $new_pass;
            $data['user'] = $user->toArray();
            $data['password'] = $raw_pass;
            if($user->parse_id && !$user->initial_pass_reset){
                $user->initial_pass_reset = 1;
            }
            if($user->save()){
                Mail::send('emails.API.V1.reset', ['data' => $data], function ($m) use ($email, $data) {
                    $m->from('hey@se.com', 'Sole Exchange');

                    $m->to($email, 'user')->subject('Pass Reset');
                });
                return parent::api_response([], true, ['Success' => 'new pass sent'], 200);
            }

        }else{
            return parent::api_response([], false, ['Error' => 'user not found'], 404);
        }


    }

    public function edit(Request $request){
        $data = Input::only('email', 'password');
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'email' => 'email|unique:users|max:255',
        ]);
        if ($validator->fails()) {
            return parent::api_response($validator->errors(), false, ['Error' => 'email invalid/used'], 401);
        } else {
            if($data['email']){
                $user->email = stripslashes($data['email']);
            }
            if($data['password']){
                $user->password = bcrypt(stripslashes($data['password']));
            }

            if($user->save()){
                return parent::api_response([], true, ['Success' => 'new details saved'], 200);
            }
        }
    }

    public function appdata () {
        $brands = Brand::orderby('name')->get();
        $shoe_sizes = ShoeSize::all();
        $colours = Color::orderby('name')->get();

        $data = [
            'brands' => $brands,
            'shoe_sizes' => $shoe_sizes,
            'colours' => $colours
        ];

        return parent::api_response($data, true, ['Success' => 'Appdata'], 200);

    }

    public function update(Request $request)
    {
        $app = $request->input('app');
        $appVersion = config('soleexchange.version.'. $app);

        if ($appVersion == null) {
            return parent::api_response([], false, ['error' => 'invalid app parameter'], 200);
        }

        $shouldUpdate = version_compare($appVersion, $request->input('version'), '>');

        $data = [
            'should_update' => $shouldUpdate
        ];

        return parent::api_response($data, true, ['Success' => 'update'], 200);
    }


    /**
     * Obtain the user information from Facebook and login/register user.
     *
     * @return Response
     */
    public function fbLogin()
    {
        $fb = new Facebook([
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'default_graph_version' => 'v2.2',
        ]);


        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,picture', Input::get('access_token'));
        } catch(FacebookResponseException $e) {
            return parent::api_response([], false, ['error' => $e->getMessage()], 500);
        } catch(FacebookSDKException $e) {
            return parent::api_response([], false, ['error' => $e->getMessage()], 500);
        }


        $fb_user = $response->getGraphUser();



        if($exists = UserSearchable::where('facebook_id', $fb_user->getId())->first()){
            $token = JWTAuth::fromUser($exists);
            return $this->api_response(['token' =>$token, 'user' => $exists] , true, ['Success' => 'logged in'], 200);
        }else{
            try{
                if($non_fb = UserSearchable::where('email', $fb_user->getEmail() )->first()){
                    $non_fb->facebook_id = $fb_user->getId();
                    $non_fb->save();

                    $token = JWTAuth::fromUser($non_fb);

                    return $this->api_response(['token' =>$token, 'user' => $non_fb] , true, ['Success' => 'logged in'], 200);
                }else{
                    $name = explode(' ',$fb_user->getName());
                    $last = count($name)-1;
                    $user = new User;
                    $user->facebook_id = $fb_user->getId();
                    $user->first_name = $name[0];
                    $user->last_name = $name[$last];
                    $user->email = $fb_user->getEmail();
                    $user->password = bcrypt(str_random(12));
                    $user->image = 'https://graph.facebook.com/v2.6/'.$fb_user->getId().'/picture?width=1920';
                    $user->save();

                    $token = JWTAuth::fromUser($user);

                    //Get full user object from DB
                    $user = UserSearchable::find($user->id);

                    return $this->api_response(['token' =>$token, 'user' => $user] , true, ['Success' => 'logged in'], 200);
                }
            }catch (\Exception $e){
                return parent::api_response([], false, ['error' => $e->getMessage()], 500);
            }

        }
    }

}