<?php

namespace App\Http\Controllers\API\V1;

use App\Events\PushNotificationEvent;
use App\Http\Requests;
use App\Notification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class NotificationController extends ApiController
{
    /*
     *
     * Get all notifications related to the signed in user.
     */
    function viewAll(){
        $notifications = Notification::where('user_id', Auth::user()->id)->with('sender')->orderBy('created_at', 'desc')->paginate(10);

        foreach ($notifications as $notification) {
           $notification->type = Notification::getType($notification->notification_type, $notification->notification_id);
        }

        return parent::api_response($notifications, true, ['return' => 'all notifications'], 200);
    }

    function deleteAll(){
        $notifications = Notification::where('user_id', Auth::user()->id);
        if($notifications->delete()){
            return parent::api_response([], true, ['return' => 'deleted all notifications'], 200);
        }else{
            return parent::api_response([], false, ['error' => 'error deleting all notifications'], 500);
        }
    }

    function deleteById($id){
        $notification = Notification::find($id);
        if($notification){
            if($notification->delete()){
                return parent::api_response([], true, ['return' => 'deleted notification'], 200);
            }else{
                return parent::api_response([], true, ['error' => 'error deleting notification'], 500);
            }
        }else{
            return parent::api_response([], true, ['error' => 'Notification not found'], 404);
        }

    }

    function filter(){

        $filter = Input::get('filter');

        if($filter == 'Offer'){
            $filter = ['Offer', 'OfferAccepted', 'OfferDeclined'];
        }else{
            $filter = [$filter];
        }

        $notifications = Notification::where('user_id', Auth::user()->id)->whereIn('notification_type', $filter)->with('sender')->orderBy('created_at', 'desc')->paginate(10);
        foreach ($notifications as $notification) {
//            if ($filter == $notification->notification_type) {
                $notification->type = Notification::getType($notification->notification_type, $notification->notification_id);
//            }
        }

        return parent::api_response($notifications, true, ['return' => 'all notifications'], 200);
    }

    function sendNotification($data) {
        $user = Auth::user();

        $validator = Validator::make($data, array(
            'notification_id' => 'required',
            'notification_type' => 'required|string',
            'channel' => 'required|string',
            'message' => 'required|string',
            'recipient' => 'required|integer',
        ));

        if ($validator->fails()) {
            return parent::api_response([], true, ['return' => 'Error Creating your notification'], 500);
        }

        $notification = Notification::create([
            'user_id' => $data['recipient'],
            'notification_id' => $data['notification_id'],
            'notification_type' => $data['notification_type'],
            'sender_id' => $user->id
        ]);

        $push = [
            'user_id' => $data['recipient'],
            'notification_id' => $data['notification_id'],
            'notification_type' => $data['notification_type'],
            'sender_id' => $user->id,
            'channel' => $data['channel'],
            'message' => $data['message'],
            'user_name' => $user->username,
            'image' => isset($data['img'])? $data['img']: str_replace('original', 'small', $user->image),
            'snippet' => isset($data['snippet'])? $data['snippet'] : 'N/A',
            'time' => Carbon::now()->toDateTimeString(),
            'extra_id' => $data['extra_id']
            ];

        if($notification->save()) {
            // send notification..
            return Event::Fire(new PushNotificationEvent($push));
        };

    }
}
