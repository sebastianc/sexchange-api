<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/03/16
 * Time: 12:33
 */

namespace App\Http\Controllers\API\V1;


use App\VerificationCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SMSController extends ApiController
{
    function sms(){
        $account_sid = 'ACa47fcdd50c5466fa58225951b6873927';
        $auth_token = '7f3617fffc2da2959b63f3cf227fb3fb';
        $code = mt_rand(10000, 99999);
        $user_code = new VerificationCode;
        $user_code->code = $code;
        $user_code->user_id = Auth::user()->id;
        if($user_code->save()){
            try{
                $client = new \Services_Twilio($account_sid, $auth_token);
                $client->account->messages->create(array(
                    'To' => Auth::user()->phone,
                    'From' => "+441668932025",
                    'Body' => 'Welcome to Sole Exchange! Verification code: '.$code,
                ));

                return parent::api_response($user_code, true, ['success' => 'code sent'], 200);
            }catch (\Services_Twilio_RestException $e){
                return parent::api_response(['error sending sms' => $e->getMessage()], false, ['error' => 'Could not send code, please check mobile number is correct.'], 400);
            }

        }else{
            return parent::api_response([], true, ['error' => 'code not sent'], 500);
        }
    }

    function verify(){
        $code = Input::get('code');
        $user =  Auth::user();
        if($code){
            $sent = VerificationCode::where('user_id', $user->id)->orderBy('created_at')->first();
            if($sent->code = $code){
                $user->phone_verified = 1;
                if($user->save()){
                    return parent::api_response([], true, ['success' => 'phone verified'], 200);
                }else{
                    return parent::api_response([], true, ['error' => 'phone not verified'], 500);
                }
            }else{
                return parent::api_response([], true, ['error' => 'Incorrect code'], 400);
            }
        }
    }

}