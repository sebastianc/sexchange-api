<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 12:19
 */

namespace App\Http\Controllers\API\V1;


use App\Support;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Validator;

class SupportController extends ApiController
{

    public function get () {
        $supports = Support::all();
        return parent::api_response($supports, true, ['success' => 'Support has been submitted successfully'], 200);
    }
    public function submit (Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'query' => 'required',
            'shoe_name' => 'required',
            'brand' => 'required'
        ]);
#
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()], 401);
        }

        $support = new Support;
        $support->name = $request['name'];
        $support->brand = $request['brand'];
        $support->shoe_name = $request['shoe_name'];
        $support->query = $request['query'];

        return parent::api_response($support, true, ['success' => 'Support has been submitted successfully'], 200);
    }
}
