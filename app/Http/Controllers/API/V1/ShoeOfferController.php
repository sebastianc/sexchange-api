<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 02/03/16
 * Time: 14:10
 */

namespace App\Http\Controllers\API\V1;


use App\Shoe;
use App\ShoeOffer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ShoeOfferController extends ApiController
{

    protected function getAllOffers() {
//        $offers = ShoeOffer::where('user_id', Auth::user()->id)->with('shoe')->paginate(10);
        $offers = Shoe::has('offers')->whereSold(0)->whereNull('deleted_at')->whereMarkedAsSold(0)->where('seller', Auth::user()->id)->with('offers')->paginate(10);
        return parent::api_response($offers, true, ['success' => 'shoe offers'], 200);
    }

    protected function getOffersMade() {
        $offers = Shoe::whereSold(0)->whereNull('deleted_at')->whereMarkedAsSold(0)->whereHas('offers', function ($query) {
            $query->where('user_id', Auth::user()->id);
        })->with('offersByCurrent')->paginate(10);
        return parent::api_response($offers, true, ['success' => 'shoe offers'], 200);
    }
    protected function getById($id) {
        $offers = ShoeOffer::whereHas('shoe', function($shoe){
            $shoe->whereSold(0)->whereNull('deleted_at')->whereMarkedAsSold(0);
        })->where('shoe_id', $id)->with('shoe', 'user')->paginate(10);
        return parent::api_response($offers, true, ['success' => 'shoe offers'], 200);
    }


    /**
     * Make an offer for a listed shoe
     * @param $id
     * @return mixed
     */
    protected function makeOffer($id){
        $shoe = Shoe::find($id);
        if($shoe->count()){
            $amount = Input::get('amount');
            if($amount >= $shoe->cost) {
                return parent::api_response([], false, ['error' => 'Offer is higher than price.'], 400);
            }

            if($amount >= $shoe->min_offer){
                $offer = new ShoeOffer;
                $offer->amount = $amount;
                $offer->user_id = Auth::user()->id;
                $offer->shoe_id = $id;
                if($offer->save()){
                    $pushData = [
                        'notification_id' => $offer->id,
                        'notification_type' => 'Offer',
                        'channel' => 'user_'.$shoe->seller,
                        'recipient' => $shoe->seller,
                        'message' => 'You have a new offer!',
                        'extra_id' => $shoe->id,
                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                    return parent::api_response($shoe, true, ['success' => 'shoe uploaded'], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'failed sending offer'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'Offer is too low.'], 400);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Shoe not found'], 404);
        }
    }

    protected function acceptOffer($id){
        $offer = ShoeOffer::find($id);
        if($offer){
            $shoe = Shoe::find($offer->shoe_id);
            if($shoe->seller == Auth::user()->id){
                $others = (new ShoeOffer())->getTable();
                DB::table($others)->where('shoe_id', '=', $offer->shoe_id)->update(array('declined' => 1, 'accepted' => 0));
                $offer->accepted = true;
                $offer->declined = false;
                if($offer->save()){
                    $pushData = [
                        'notification_id' => $id,
                        'notification_type' => 'OfferAccepted',
                        'channel' => 'user_'.$offer->user_id,
                        'recipient' => $offer->user_id,
                        'message' => 'Your offer was accepted!',
                        'extra_id' => $shoe->id,
                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                    return parent::api_response($offer, true, ['success' => 'offer accepted'], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'failed accepting offer'], 500);
                }
            }
        }else{
            return parent::api_response([], false, ['error' => 'Offer not found'], 404);
        }
    }

    protected function declineOffer($id){
        $offer = ShoeOffer::find($id);
        if($offer){
            $shoe = Shoe::find($offer->shoe_id);
            $offer->declined = true;
            $offer->accepted = false;
            if($offer->save()){
                $pushData = [
                    'notification_id' => $id,
                    'notification_type' => 'OfferDeclined',
                    'channel' => 'user_'.$offer->user_id,
                    'recipient' => $offer->user_id,
                    'message' => 'Your offer was declined.',
                    'extra_id' => $shoe->id,
                ];
                $push = new NotificationController;
                $push->sendNotification($pushData);
                return parent::api_response($offer, true, ['success' => 'offer declined'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'failed declining offer'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Offer not found'], 404);
        }
    }
}