<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 10:13
 */

namespace App\Http\Controllers\API\V1;


use App\BlockedUser;
use App\Follow;
use App\Review;
use App\Shoe;
use App\ShoeLike;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserSearchable;
use Illuminate\Support\Facades\Input;

class UserController extends ApiController
{
    /**
     * Reusable function for removing blocked users from user based requests
     * @param $users
     * @return mixed
     */

    //TODO: Change to more efficient function
    function blocked($users){
        $blocked = BlockedUser::where('user_id', Auth::user()->id)->get()->pluck('blocked');
        $blockedBy = BlockedUser::where('blocked', Auth::user()->id)->get()->pluck('user_id');
        return $users->whereNotIn('id', $blockedBy)->whereNotIn('id', $blocked);
    }

    function follow($id){
        $user = User::where('id', $id);
        //$user = $this->blocked($user)->get();
        if($user->count()){
            $exists = Follow::where('user_id', Auth::user()->id)->where('follows', $id)->get();
            if(!$exists->count()){
                $follow = new Follow;
                $follow->user_id = Auth::user()->id;
                $follow->follows = $id;
                if($follow->save()){
                    $pushData = [
                        'notification_id' => $id,
                        'notification_type' => 'Follow',
                        'channel' => 'user_'.$id,
                        'recipient' => $id,
                        'message' => 'You have a new follower!',
                        'extra_id' => $id,
                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                    return parent::api_response($follow, true, ['success' => 'followed user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'error saving'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'Already liked this user'], 400);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function unfollow($id){
        $user = User::where('id', $id);
        if($user->count()){
            $exists = Follow::where('user_id', Auth::user()->id)->where('follows', $id)->first();
            if($exists){
                if($exists->delete()){
                    return parent::api_response($exists, true, ['success' => 'unfollowed user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'error removing'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'You do not follow that user'], 400);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function block($id){
        $user = User::find($id);
        if($user){
            $exists = BlockedUser::where('user_id', Auth::user()->id)->where('blocked', $id)->get();
            if(!$exists->count()){
                $block = new BlockedUser;
                $block->user_id = Auth::user()->id;
                $block->blocked = $id;
                if($block->save()){
                    $blocks = BlockedUser::where('blocked', $id)->get();
                    if($blocks->count() > 15){
                        $user = UserSearchable::find($id);
                        if($user){
                            $user->delete();
                        }
                    }
                    return parent::api_response($block, true, ['success' => 'blocked user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'block failed'], 500);
                }
            }else{
                return parent::api_response([], true, ['success' => 'Already blocked that user'], 200);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    /**
     * Review user
     * @param $id
     * @return mixed
     */
    function review(Request $request, $id)
    {
        $user = UserSearchable::find($id);
        if ($user) {
            $text = stripslashes(Input::get('text'));
            $rating = (int)Input::get('rating');
            $shoeId = $request->input('shoe');

            $validator = Validator::make($request->all(), [
                'text' => 'required|min:4',
                'rating' => 'required|integer|max:5',
                'shoe' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            } else {
                $shoe = Shoe::findOrFail($shoeId);
                $isSeller = Auth::user()->id == $shoe->seller;
                $review = new Review;
                $review->user_id = Auth::user()->id;
                $review->seller_id = $shoe->seller;
                $review->rating = $rating;
                $review->text = $text;
                $review->shoe_id = $shoe->id;
                $review->is_seller = $isSeller;

                if ($review->save()) {
                    $pushData = [
                        'notification_id' => $review->id,
                        'notification_type' => 'Review',
                        'channel' => 'user_' . ($isSeller ? $review->user_id : $review->seller_id),
                        'recipient' => ($isSeller ? $review->user_id : $review->seller_id),
                        'message' => 'Someone reviewed your transaction!',
                        'snippet' => substr($review->text, 0, 100),
                        'extra_id' => $review->id,

                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);

                    $user->calculateRating();
                    $user->save();
                    return parent::api_response($review, true, ['success' => 'reviewed user ' . $id], 200);
                } else {
                    return parent::api_response([], false, ['error' => 'Please enter review and rating'], 400);
                }

            }
        }
    }

    public function search($term){

        $users = UserSearchable::where('id', '!=', Auth::user()->id)->with('following')->search($term)->paginate(30);
        return parent::api_response($users, true, ['return' => 'search for '.$term], 200);

    }
    function getUserRating($id){
        $average_reviews = Review::where('seller_id', $id)->avg('rating');
        return parent::api_response(['average_rating' => number_format($average_reviews, 1)], true, ['return' => 'Average rating'], 200);
    }

    public function getUsers() {
        $users = UserSearchable::where('id', '!=', Auth::user()->id)->with('following')->orderBy('created_at', 'desc')->paginate(30);
        return parent::api_response($users, true, ['return' => 'all users']);
    }

    function getById($id){
        $user = UserSearchable::with('follows', 'followers')->find($id);
        if($user){
            $average_reviews = Review::where('seller_id', $user->id)->avg('rating');
            $user['average_rating'] = number_format($average_reviews, 1);
            return parent::api_response($user, true, ['return' => 'user '], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function getShoes($id){
        $shoes = Shoe::where('seller', $id)->where('sold', 0)->orderBy('created_at', 'desc')->paginate(10);
        return parent::api_response($shoes, true, ['return' => 'shoes by user'], 200);
    }

    function getLikes($id){
        $shoes = ShoeLike::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(10);
        return parent::api_response($shoes, true, ['return' => 'likes by user'], 200);
    }
    function getFollowers($id){
        $shoes = Follow::where('follows', $id)->with('users')->paginate(10);
        return parent::api_response($shoes, true, ['return' => 'user followers'], 200);
    }
    function getFollowing($id){
        $shoes = Follow::where('user_id', $id)->with('follows')->paginate(10);
        return parent::api_response($shoes, true, ['return' => 'user following'], 200);
    }

    function reviewsAsBuyer($id){
        $reviews = Review::with('user')->where('user_id', $id)->where('is_seller', 0)->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as buyer'], 200);
    }

    function reviewsAsSeller($id){
        $reviews = Review::with('user')->where('seller_id', $id)->where('is_seller', 1)->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as buyer'], 200);
    }



}