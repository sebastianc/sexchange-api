<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11/08/16
 * Time: 12:14
 */

namespace App\Http\Controllers\API\V1;


use App\Models\ManagedStripe;
use App\StripeBankTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Mockery\CountValidator\Exception;
use Stripe\Balance;
use Stripe\Stripe;

class PaymentController extends ApiController
{
    public function saveBankAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address_line_one' => 'required',
            'address_city' => 'required',
            'address_county' => 'required',
            'address_country' => 'required',
            'address_postal_code' => 'required',
            'date_of_birth' => 'required|date',
            'stripe_bank_token' => 'required|sometimes',
            'account_number' => 'required_with:sort_code,account_holder_name',
            'sort_code' => 'required_with:account_number,account_holder_name',
            'account_holder_name' => 'required_with:sort_code,account_number',
        ]);

        if ($validator->fails()) {
            return $this->api_response(['error' => $validator->errors()->first()], false, [], 400);
        }

        $user = Auth::user();
        $stripe = new ManagedStripe();

        //Create the stripe account if required

        if (!$user->stripeAccount) {

            if(!$request->input('account_number') || !$request->input('sort_code') || !$request->input('account_holder_name')){
                return $this->api_response([], false, ['return' => 'Please provide bank account details'], 400);
            }

            $created = $stripe->createAccount($user, $request->ip(), $request->input('first_name'), $request->input('last_name'));

            if (!$created) {
                return $this->api_response([], false, ['return' => 'unable to create stripe account'], 400);
            }

            $user = $user->fresh();
        }

        //Update the personal details

        $updated = $stripe->updatePersonalDetails($user,
            Carbon::parse($request->input('date_of_birth')),
            $request->input('address_line_one'),
            $request->input('address_line_two'),
            $request->input('address_city'),
            $request->input('address_county'),
            $request->input('address_country'),
            $request->input('address_postal_code'),
            $request->input('first_name'),
            $request->input('last_name')
        );

        if (!$updated) {
            return $this->api_response([], false, ['return' => 'unable to update address details'], 200);
        }

        //Update bank details

        if ($request->has('account_number')) {
            $updated = $stripe->updateBankAccount(
                $user,
                $request->input('account_number'),
                $request->input('sort_code'),
                $request->input('account_holder_name')
            );
            if (!$updated) {
                return $this->api_response([], false, ['return' => 'unable to update bank details'], 200);
            }

            $user->stripe_connected = true;
            $user->save();
        }

        return $this->api_response(['source' => $stripe->getAccount($user)], true, ['return' => 'bank details updated'], 200);
    }

    public function getBankAccount()
    {
        $stripe = new ManagedStripe();
        $account = $stripe->getAccount(Auth::user());

        return $this->api_response(['source' => $account], true, ['return' => 'bank details'], 200);
    }

    public function listTransactions()
    {
        $user = Auth::user();

        $transactions = StripeBankTransaction
            ::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        //TODO: Tidy up
        if($user->stripeAccount){
            Stripe::setApiKey($user->stripeAccount->secret_key);
            $balance = Balance::retrieve();
            $balance = $balance->available[0]->amount;
        }else{
            return $this->api_response([], false, ['error' => 'No stripe account found for user'], 400);
        }

        return $this->api_response(['balance' => $balance, 'transactions' => $transactions], true, ['return' => 'transactions'], 200);
    }

    public function transferToBank()
    {
        $stripe = new ManagedStripe();
        $user = Auth::user();

        if (!$stripe->transferToBank($user, 'Sole Exchange payment')) {
            return $this->api_response([], false, ['return' => 'money not transferred'], 400);
        }

        $transactions = StripeBankTransaction
            ::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->api_response(['balance' => $user->balance, 'transactions' => $transactions], true, ['return' => 'money transferred'], 200);
    }

    public function topup(Request $request)
    {
        try {
//            \Stripe\Stripe::setApiKey(env('STRIPE_SK'));
            Stripe::setApiKey(Auth::user()->stripeAccount->secret_key);


            $token = \Stripe\Token::create(array(
                "card" => array(
                    "number" => "4000000000000077",
                    "exp_month" => 8,
                    "exp_year" => 2017,
                    "cvc" => "314"
                )
            ));

            $response = \Stripe\Charge::create(array(
                "amount" => $request->input('amount') * 100,
                "currency" => "gbp",
                "source" => $token, // obtained with Stripe.js
                "description" => "Topup"
            ));

            dd($response);

        } catch (Exception $e) {
            dd($e);
        }
    }
}