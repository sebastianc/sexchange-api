<?php


/**
 * WEB ROUTES
 */
Route::group(['middleware' => ['web','ip-lock']], function($request) {

    Route::post('/register', 'Web\AuthController@register');
    Route::get('/register', 'Web\AuthController@registerPage');
    Route::post('/login', 'Web\AuthController@login');
    Route::get('/login', 'Web\AuthController@loginPage');
    Route::get('/logout', 'Auth\AuthController@logout');
    Route::get('login/facebook', 'Web\AuthController@redirectToProvider');
    Route::get('login/facebook/callback', 'Web\AuthController@handleProviderCallback');

    Route::get('/', 'Web\SearchController@explore');
    Route::get('/wanted', 'Web\SearchController@wanted');
    Route::get('/search', 'Web\SearchController@searchRedirect');
    Route::post('/search/filter', 'Web\SearchController@ajax_search');
    Route::get('/search/users/{term}', 'Web\SearchController@searchUsers');
    Route::get('/search/{term}', 'Web\SearchController@search');

    Route::group(['prefix' => 'shoes'], function ($request) {
        Route::post('/load-comments', 'Web\ShoeController@ajaxComments');
        Route::get('/{slug}/edit', 'Web\ShoeController@editShoe');
        Route::post('/{slug}/edit', 'Web\ShoeController@postEditShoe');
        Route::get('/{slug}', 'Web\ShoeController@viewShoe');
    });

    Route::group(['prefix' => 'profile'], function ($request) {
        Route::get('/{slug}', 'Web\ProfileController@index');
        Route::get('/{slug}/listings', 'Web\ProfileController@listings');
        Route::get('/{slug}/likes', 'Web\ProfileController@likes');
        Route::get('/{slug}/reviews', 'Web\ProfileController@reviews');
        Route::get('/{slug}/reviews/sold', 'Web\ProfileController@reviewsSold');
        Route::get('/{slug}/reviews/purchased', 'Web\ProfileController@reviewsPurchased');
        Route::get('/{slug}/followers', 'Web\ProfileController@followers');
        Route::get('/{slug}/following', 'Web\ProfileController@following');
        Route::get('/{slug}/report', 'Web\ProfileController@report');
        Route::post('/follow/{id}', 'Web\ProfileController@ajaxFollow');
    });

    Route::get('faq', 'Web\AboutController@viewFaq');
    Route::post('faq/contact', 'Web\AboutController@postContact');

    //Must be logged in to access
    Route::group(['middleware' => 'auth'], function($request) {
        Route::post('/upload-image', 'Web\AuthController@upload_profile_image');
        Route::get('/notifications', 'Web\NotificationController@view');
        Route::get('/notifications/load-more/{type}', 'Web\NotificationController@ajaxLoad');
        Route::post('/notifications/delete/{id}', 'Web\NotificationController@ajaxDelete');
        Route::post('/notifications/viewed', 'Web\NotificationController@ajaxViewed');

        Route::group(['prefix' => 'payment'], function ($request) {
            Route::get('/edit-bank', 'Web\PaymentController@bankDetails');
            Route::post('/edit-bank', 'Web\PaymentController@postBankDetails');
            Route::get('/shoe/{id}', 'Web\PaymentController@payShoe');
            Route::get('/offer/{id}', 'Web\PaymentController@payOffer');
            Route::post('pay', 'Web\PaymentController@ajaxPay');
            Route::get('/complete/{id}', 'Web\PaymentController@purchaseComplete');
        });

        Route::group(['prefix' => 'account'], function ($request) {
            Route::get('/', 'Web\AccountController@index');
            Route::get('/listings', 'Web\AccountController@listings');
            Route::get('/likes', 'Web\AccountController@likes');
            Route::get('/offers/received', 'Web\AccountController@offersReceived');
            Route::get('/offers/sent', 'Web\AccountController@offersSent');
            Route::post('/offers/{id}/accept', 'Web\AccountController@ajaxAcceptOffer');
            Route::post('/offers/{id}/decline', 'Web\AccountController@ajaxDeclineOffer');
            Route::get('/wanted', 'Web\AccountController@wanted');
            Route::get('/transactions/purchases', 'Web\AccountController@listPurchases');
            Route::get('/transactions/sold', 'Web\AccountController@listSold');
            Route::post('/transactions/review', 'Web\AccountController@postReview');
            Route::post('/transactions/dispute', 'Web\AccountController@postDispute');
            Route::post('/transactions/{id}/dispatch', 'Web\AccountController@ajaxDispatchSold');
            Route::get('/edit', 'Web\AccountController@edit');
            Route::post('/edit', 'Web\AccountController@postEdit');
            Route::post('/edit/image', 'Web\AccountController@postEditPicture');

            Route::get('/complete', 'Web\AccountController@complete');
            Route::post('/complete', 'Web\AccountController@addUsername');
        });

        Route::group(['prefix' => 'upload'], function ($request) {
            Route::get('/shoe', 'Web\ListingController@index');
            Route::post('/shoe', 'Web\ListingController@addShoe');

        });

        Route::group(['prefix' => 'shoes'], function ($request) {
            Route::post('/like/{id}', 'Web\APIController@likeShoe');
            Route::post('/unlike/{id}', 'Web\APIController@unlikeShoe');
            Route::post('/comment', 'Web\ShoeController@ajaxComment');
            Route::post('/delete/{id}', 'Web\ShoeController@ajaxDelete');
            Route::post('/mark-sold/{id}', 'Web\ShoeController@ajaxMarkSold');
            Route::post('/offer/{id}', 'Web\ShoeController@ajaxOffer');
            Route::get('/{slug}/edit', 'Web\ShoeController@editShoe');
        });

    });
});


/**
 * API ENDPOINTS (V1)
 */

Route::group(['prefix' => 'api/v1','middleware' => 'api'], function($request) {
    //Account management
    Route::group(['prefix' => 'auth'], function ($request) {
        Route::post('/login', 'API\V1\AuthController@login');
        Route::post('/login/fb', 'API\V1\AuthController@fbLogin');
        Route::post('/register', 'API\V1\AuthController@register');
        Route::get('/logout', 'API\V1\AuthController@logout');
        Route::post('/reset', 'API\V1\AuthController@reset');
        Route::get('/appdata', 'API\V1\AuthController@appdata');
    });
    
    Route::get('update', 'API\V1\AuthController@update');

    //AUTHENTICATED ROUTES
    Route::group(['middleware' => 'jwt'], function ($request) {

        //Stripe payment
        Route::group(['prefix' => 'payment'], function ($request) {
            Route::get('/source', 'API\V1\PaymentController@getBankAccount');
            Route::post('/source', 'API\V1\PaymentController@saveBankAccount');
            Route::get('/transactions', 'API\V1\PaymentController@listTransactions');
            Route::post('/transfer', 'API\V1\PaymentController@transferToBank');
            if (env('APP_DEBUG')) Route::get('/topup', 'API\V1\PaymentController@topup');
        });

        //Shoes
        Route::group(['prefix' => 'shoes'], function ($request) {
            Route::get('/', 'API\V1\ShoeController@viewAll');
            Route::get('/followed', 'API\V1\ShoeController@feed');
            Route::post('/tailor', 'API\V1\TailoringController@tailoring');
            Route::get('/search', 'API\V1\ShoeController@search');
            Route::post('/like/{shoe_id}', 'API\V1\ShoeController@like');
            Route::post('/unlike/{shoe_id}', 'API\V1\ShoeController@unlike');
            Route::get('/{shoe_id}', 'API\V1\ShoeController@get');
        });
        //users
        Route::group(['prefix' => 'users'], function ($request) {
            Route::get('/', 'API\V1\UserController@getUsers');
            Route::get('/search/{search_term}', 'API\V1\UserController@search');
            Route::get('/search/', 'API\V1\UserController@getUsers');
            Route::get('/rating/{user_id}', 'API\V1\UserController@getUserRating');
            Route::get('/shoes/{user_id}', 'API\V1\UserController@getShoes');
            Route::get('/likes/{user_id}', 'API\V1\UserController@getLikes');
            Route::get('/followers/{user_id}', 'API\V1\UserController@getFollowers');
            Route::get('/following/{user_id}', 'API\V1\UserController@getFollowing');
            Route::get('/reviews-buyer/{user_id}', 'API\V1\UserController@reviewsAsBuyer');
            Route::get('/reviews-seller/{user_id}', 'API\V1\UserController@reviewsAsSeller');
            Route::get('/{id}/', 'API\V1\UserController@getById');
        });

        //Following users
        Route::group(['prefix' => 'follow'], function ($request) {
            Route::post('/{user_id}', 'API\V1\UserController@follow');
            Route::post('remove/{user_id}', 'API\V1\UserController@unfollow');
        });
        //Blocking users
        Route::group(['prefix' => 'block'], function ($request) {
            Route::post('/{user_id}', 'API\V1\UserController@block');
        });
        //Comments
        Route::group(['prefix' => 'comments'], function ($request) {
            Route::get('/get/{shoe_term}', 'API\V1\CommentController@get');
            Route::post('/{shoe_id}', 'API\V1\CommentController@post');
        });
        //Profile
        Route::group(['prefix' => 'profile'], function ($request) {
            Route::get('/', 'API\V1\ProfileController@viewInfo');
            Route::post('', 'API\V1\ProfileController@editProfile');
            Route::get('/listings', 'API\V1\ProfileController@viewShoes');
            Route::get('/wanted', 'API\V1\ProfileController@viewWantedShoes');
            Route::get('/sold', 'API\V1\ProfileController@viewSoldShoes');
            Route::get('/purchased', 'API\V1\ProfileController@viewPurchased');
            Route::get('/reviews-buyer', 'API\V1\ProfileController@reviewsAsBuyer');
            Route::get('/reviews-seller', 'API\V1\ProfileController@reviewsAsSeller');
            Route::get('/followers', 'API\V1\ProfileController@followers');
            Route::get('/following', 'API\V1\ProfileController@following');
            Route::get('/likes', 'API\V1\ProfileController@likes');
        });
        //review
        Route::group(['prefix' => 'review'], function ($request) {
            Route::get('/', 'API\V1\ProfileController@viewInfo');
            Route::post('/{user_id}', 'API\V1\UserController@review');
        });
        //Listings
        Route::group(['prefix' => 'listings'], function ($request) {
            Route::get('/', 'API\V1\ListingController@viewAll');
            Route::post('mark-sold/{shoe_id}', 'API\V1\ListingController@markSold');
            Route::post('mark-available/{shoe_id}', 'API\V1\ListingController@markAvailable');
            Route::post('delete/{shoe_id}', 'API\V1\ListingController@delete');
            Route::post('mark-dispatched/{shoe_id}', 'API\V1\PurchaseController@markDispatched');
            Route::post('reserve', 'API\V1\PurchaseController@createPurchase');
            Route::post('buy', 'API\V1\PurchaseController@buy');
            Route::get('/buy/success', 'API\V1\PurchaseController@success');
            Route::get('/buy/error', 'API\V1\PurchaseController@error');
            Route::get('receipt/{purchase_id}', 'API\V1\PurchaseController@getReceipt');
            Route::post('dispute/{shoe_id}', 'API\V1\PurchaseController@openDispute');
            Route::post('new', 'API\V1\ListingController@upload');
            Route::get('/colours', 'API\V1\ColourController@get');
            Route::post('edit', 'API\V1\ListingController@edit');
            Route::post('image/remove', 'API\V1\ListingController@removeImages');
            Route::post('/views', 'API\V1\ListingController@addViews');
            Route::post('/report/{shoe_id}', 'API\V1\ShoeController@reportShoe');
            Route::get('/likes/{shoe_id}', 'API\V1\ShoeController@getLikes');
        });
        //Offers
        Route::group(['prefix' => 'offer'], function ($request) {
            Route::get('/', 'API\V1\ShoeOfferController@getAllOffers');
            Route::get('/placed', 'API\V1\ShoeOfferController@getOffersMade');
            Route::get('/get/{shoe_id}', 'API\V1\ShoeOfferController@getById');
            Route::post('/make/{shoe_id}', 'API\V1\ShoeOfferController@makeOffer');
            Route::post('/accept/{offer_id}', 'API\V1\ShoeOfferController@acceptOffer');
            Route::post('/decline/{offer_id}', 'API\V1\ShoeOfferController@declineOffer');
        });
        // Notifications
        Route::group(['prefix' => 'notifications'], function ($request) {
            Route::get('/', 'API\V1\NotificationController@viewAll');
            Route::get('/filter', 'API\V1\NotificationController@filter');
            Route::post('/delete/all', 'API\V1\NotificationController@deleteAll');
            Route::post('/delete/{id}', 'API\V1\NotificationController@deleteById');
//            Route::post('/send', 'API\V1\NotificationController@sendNotification');
        });
        // Support
        Route::group(['prefix' => 'support'], function () {
            Route::post('/submit', 'API\V1\SupportController@submit');
        });
        // FAQ
        Route::group(['prefix' => 'faq'], function () {
            Route::get('/', 'API\V1\FaqController@viewInfo');
            Route::post('/submit', 'API\V1\FaqController@createFaq');
        });

        Route::group(['prefix' => 'stripe'], function () {
            Route::get('/auth', 'API\V1\StripeController@connectLink');
            Route::post('/connect', 'API\V1\StripeController@connectAccount');
            Route::get('/charge', 'API\V1\StripeController@test');
            Route::get('/test', 'API\V1\PurchaseController@clearAbandonedPurchases');
        });

        Route::group(['prefix' => 'sms'], function () {
            Route::post('/send', 'API\V1\SMSController@sms');
            Route::post('/verify', 'API\V1\SMSController@verify');
        });
    });

});

//Redirect for app deep links
Route::get('stripe/connected', 'API\V1\PurchaseController@redirect');


//Route::get('stripe/token', 'API\V1\StripeController@token');

