<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 07/09/16
 * Time: 09:40
 */

namespace App\Http\Middleware;


use Illuminate\Session\Store;
use Closure;

class ClearSessionViews
{
    private $session;

    public function __construct(Store $session)
    {
        // Let Laravel inject the session Store instance,
        // and assign it to our $session variable.
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        $posts = $this->getViewedPosts();

        if ( ! is_null($posts))
        {
            $posts = $this->cleanExpiredViews($posts);

            $this->storePosts($posts);
        }


        return $next($request);
    }

    private function getViewedPosts()
    {
        // Get all the viewed posts from the session. If no

        return $this->session->get('viewed_shoes', null);
    }

    private function cleanExpiredViews($posts)
    {
        $time = time();

        // Let the views expire after one hour.
        $throttleTime = 3600;

        // Filter through the post array. The argument passed to the
        // function will be the value from the array, which is the
        // timestamp in our case.
        return array_filter($posts, function ($timestamp) use ($time, $throttleTime)
        {
            // If the view timestamp + the throttle time is
            // still after the current timestamp the view
            // has not expired yet, so we want to keep it.
            return ($timestamp + $throttleTime) > $time;
        });
    }

    private function storePosts($posts)
    {
        $this->session->put('viewed_shoes', $posts);
    }
}