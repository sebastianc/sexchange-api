<?php

namespace App\Http\Middleware;

use App\Notification;
use App\NotificationWeb;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class FetchNotifications
{
    /**
     * Load the last three user notifications and share with the view.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $notifications = NotificationWeb
                ::where('user_id', Auth::user()->id)
                ->orderBy('created_at', 'desc')
                ->limit(3)
                ->get();
            if (Auth::user()->notifications_viewed_at == null) {
                $count = NotificationWeb
                    ::where('user_id', Auth::user()->id)
                    ->count();
            } else {
                $count = NotificationWeb
                    ::where('user_id', Auth::user()->id)
                    ->where('created_at', '>', Auth::user()->notifications_viewed_at)
                    ->count();
            }
        } else {
            $notifications = collect();
            $count = 0;
        }

        View::share('header_notifications', $notifications);
        View::share('header_notification_count', $count);
        
        return $next($request);
    }
}
