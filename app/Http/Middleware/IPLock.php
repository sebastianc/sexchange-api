<?php
/**
 * Created by PhpStorm.
 * User: eg
 * Date: 17/08/16
 * Time: 10:08
 */

namespace App\Http\Middleware;

use App\Http\Requests\Request;
use Closure;

class IPLock
{
    /**
     * Middleware to lock access to routes to listed IP's during development
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $allowed = [
            '86.8.115.102',
            '192.168.10.1',
            '192.168.10.10',
            '194.12.6.99', //George
            '31.48.81.180', //George Home,
            '109.146.205.54', //New IP
            '185.115.130.193' //New IP

        ];

        if (!in_array($request->server('REMOTE_ADDR'), $allowed) && env('IPLOCK') == true ) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }

}

