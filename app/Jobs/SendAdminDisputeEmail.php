<?php

namespace App\Jobs;

use App\Dispute;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminDisputeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $dispute;

    /**
     * Create a new job instance.
     *
     * @param Dispute $dispute
     */
    public function __construct(Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * Send a dispute email to the site admin.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('emails.dispute', ['dispute' => $this->dispute], function ($m) {
            $m->to(config('mail.admin.address'), config('mail.admin.name'));
            $m->subject('Dispute raised on shoe');
        });
    }
}
