<?php

namespace App\Jobs;

use App\Dispute;
use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContactFormEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $name;
    private $email;
    private $number;
    private $message;

    /**
     * Create a new job instance.
     *
     * @param $name
     * @param $email
     * @param $number
     * @param $message
     */
    public function __construct($name, $email, $number, $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->number = $number;
        $this->message = $message;
    }

    /**
     * Send a dispute email to the site admin.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $data['name'] = $this->name;
        $data['email'] = $this->email;
        $data['number'] = $this->number;
        $data['message'] = $this->message;
        $data['timestamp'] = Carbon::now();

        $mailer->send('emails.contact', $data, function ($m) {
            $m->to(config('mail.admin.address'), config('mail.admin.name'));
            $m->subject('Contact form request');
        });
    }
}
