<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:13
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $with = ['dispute'];
    protected $hidden = ['refunded'];

    function shoe(){
        return $this->belongsTo('App\Shoe')->with('brand_info', 'colors', 'sizes', 'comments', 'commentCount', 'likes', 'images', 'seller','reviewBySeller','reviewByBuyer');
    }

    function user(){
        return $this->belongsTo('App\User', 'buyer_id');
    }

    /**
     * Morph notification type, to a notification.
     */
    public function notification()
    {
        return $this->morphMany('Notification', 'notification');
    }

    /**
     * A dispute about the purchase.
     */
    public function dispute()
    {
        return $this->hasOne('App\Dispute', 'purchase_id', 'id');
    }

    /**
     * The stripe transaction associated with a purchase
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stripeTransaction()
    {
        return $this->hasOne('App\StripeTransaction');
    }

    /**
     * Human readable status for purchases.
     *
     * @return string
     */
    public function getFormattedStatusAttribute()
    {
        switch ($this->status) {
            case 'pending': return 'Payment pending';
            case 'paid': return 'Not dispatched';
            case 'dispatched': return 'Dispatched';
        }
    }

    public function getFormattedShippingAddressAttribute()
    {
        $parts = [
            $this->ship_address_line1,
            $this->ship_address_line2,
            $this->ship_address_city,
            $this->ship_address_county,
            $this->ship_address_postcode
        ];
        $address = '';

        foreach ($parts as $part) {
            if ($part !== null && $part !== '') {
                $address .= $part . ', ';
            }
        }

        return trim($address, ', ');
    }
}