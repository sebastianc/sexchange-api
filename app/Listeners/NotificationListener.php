<?php

namespace App\Listeners;

use App\Events\PushNotificationEvent;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;

class NotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PushNotificationEvent  $event
     * @return Response
     */
    public function handle(PushNotificationEvent $event)
    {
        $client = new Client();
        $notification = $event->notification;

        $uri = 'https://go.urbanairship.com/api/push';
        $headers = [
            'Authorization' => 'Basic '.base64_encode(env('AIRSHIP_KEY').':'.env('AIRSHIP_MASTER_SECRET')),
            'Accept' => 'application/vnd.urbanairship+json; version=3;',
            'Content-Type' => 'application/json'
        ];
        $postBody = [
            'audience' => [
                'tag' => $notification['channel'],
            ],
            'notification' => [
                'alert' => $notification['message'],
                'ios' => [
                    'alert' => $notification['message'],
                    'badge' => '+1',
                    'extra' => [
                        'type' => $notification['notification_type'],
                        'object_id' => $notification['message'],
                        'user_id' => $notification['sender_id']
                    ]
                ],
                'android' => [
                    'alert' => $notification['message'],
                    'extra' => [
                        'type' => $notification['notification_type'],
                        'object_id' => $notification['message'],
                        'user_id' => $notification['sender_id']
                    ]
                ],
            ],
            'device_types' => ['ios', 'android']
        ];

        //If push aimed at a user
        if(str_contains($notification['channel'], 'user_')){
            $postBody = [
                'audience' => [
                    'named_user' => $notification['channel'],
                ],
                'notification' => [
                    'alert' => $notification['message'],
                    'ios' => [
                        'alert' => $notification['message'],
                        'badge' => '+1',
                        'extra' => [
                            'type' => $notification['notification_type'],
                            'object_id' => $notification['notification_id'],
                            'user_id' => $notification['sender_id'],
                            'sent_at' => $notification['time'],
                            'user_name' => $notification['user_name'],
                            'img' => $notification['image'],
                            'snippet' => $notification['snippet'],
                            'extra_id' => $notification['extra_id'],

                        ]
                     ],
                    'android' => [
                        'alert' => $notification['message'],
                        'extra' => [
                            'type' => $notification['notification_type'],
                            'object_id' => $notification['notification_id'],
                            'user_id' => $notification['sender_id'],
                            'sent_at' => $notification['time'],
                            'user_name' => $notification['user_name'],
                            'img' => $notification['image'],
                            'snippet' => $notification['snippet'],
                            'extra_id' => $notification['extra_id'],

                        ]
                    ],
                ],
                'device_types' => ['ios', 'android'],

            ];
        }
        $request = $client->createRequest('POST', $uri, [
            'headers' => $headers,
            'body' => json_encode($postBody)
        ]);
        //Attempt to send push
        try{
            $response = $client->send($request);
            $body = json_decode($response->getBody()->getContents());
            return  $body;
        } catch(ClientException $e) {
            $response = $e->getResponse()->json(); //Get error response body
            return  $response;
        }

    }
}
