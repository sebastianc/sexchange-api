<?php

namespace App\Listeners;

use App\Events\PaymentEvent;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;

class PaymentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $shoe;
    public $user;
    public $seller;
    public $token;
    public $userVerification;
    public $offer;


    public $apiUrl = 'https://svcs.sandbox.paypal.com/AdaptivePayments/';
    public $paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=';

    public function __construct()
    {
        $this->headers = array(
            "X-PAYPAL-SECURITY-USERID: "."eg-facilitator_api1.dreamr.uk",
            "X-PAYPAL-SECURITY-PASSWORD: "."2YBTAGFVTF39TVBE",
            "X-PAYPAL-SECURITY-SIGNATURE: "."AFcWxV21C7fd0v3bYYYRCpSSRl31AXnjW-GjgPjeq9bhCP7qwc2lMbxV",
            "X-PAYPAL-REQUEST-DATA-FORMAT: JSON",
            "X-PAYPAL-RESPONSE-DATA-FORMAT: JSON",
            "X-PAYPAL-APPLICATION-ID: "."APP-80W284485P519543T",
        );

        $this->envelope = array(
            "errorLanguage" => "en_US",
            "detailLevel" => "ReturnAll"
        );
    }

    function getVerification() {
        $createAccountDetails = array(
            "emailAddress" => "christopher.somers1995@gmail.com",
            "firstName" => "Christopher",
            "lastName" => "McLaughlin",
            "matchCriteria" => "NAME",
            "requestEnvelope" => $this->envelope
        );

        $response = $this->_paypalSend($createAccountDetails, "GetVerifiedStatus");
    }

    function getPaymentOptions($paykey) {
        $packet = array(
            "requestEnvelope" => $this->envelope,
            "payKey" => $paykey
        );

        return $this->_paypalSend($packet, "GetPaymentOptions");

    }

    public function handle(PaymentEvent $event)
    {
        $this->user = $event->user;
        $this->seller = $event->seller;
        $this->shoe = $event->shoe;
        $this->token = $event->token;
        $this->offer = $event->offer;

        return $this->splitPay();
    }

    function _paypalSend($data, $call) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl.$call);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

        return json_decode(curl_exec($ch), TRUE);
    }


    function splitPay(){
        if($this->offer){
            $commish = ($this->offer->amount/100)*10;
            $amount = $this->offer->amount - $commish;
        }else{
            $commish = ($this->shoe['cost']/100)*10;
            $amount = $this->shoe['cost'] - $commish;

        }
        $createPacket = array(
            "actionType" => "CREATE",
            "currencyCode" => "GBP",
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => $amount,
                        "email" => 'seller@se.uk'
                    ),
                    array(
                        "amount" => $commish,
                        "email" => "eg-facilitator@dreamr.uk"
                    )
                )
            ),
            //"returnUrl" => "soleexchange://?success=1&shoeID=" . $this->shoe->id . '&' . 'token=' . $this->token,
            "returnUrl" => "http://local.se.uk/api/v1/paypal/success?token=" . $this->token,
            "cancelUrl" => "http://local.se.uk/api/v1/paypal/fail?token=" . $this->token,
            "requestEnvelope" => array(
                "errorLanguage" => "en_US",
                "detailLevel" => "ReturnAll"
            )

    );
        $response = $this->_paypalSend($createPacket, "Pay");


            $detailsPacket = array(
            "requestEnvelope" => array(
                "errorLanguage" => "en_US",
                "detailLevel" => "ReturnAll"
            ),
            "payKey" => $response['payKey'],
            "senderOptions" => array(
                "requireShippingAddressSelection" => true
            ),
            "receiverOptions" => array(
                array(
                    "receiver" => array("email" => 'seller@se.uk'),
                    "invoiceData" => array(
                        "item" => array(
                            array(
                                "name" => $this->shoe['title'],
                                "price" => $amount,
                                "identifier" => 'shoe'
                            )
                        )
                    )
                ),
                array(
                    "receiver" => array("email" => "eg-facilitator@dreamr.uk"),
                    "invoiceData" => array(
                        "item" => array(
                            array(
                                "name" => "Fees",
                                "price" => $commish,
                                "identifier" => "com"
                            )
                        )
                    )
                ),
            )
        );
        $paykey = $response['payKey'];

        if (!$paykey == null) {
            $response = $this->_paypalSend($detailsPacket, "SetPaymentOptions");
            $dets =  $this->getPaymentOptions($paykey);
            return $this->paypalUrl.$paykey;

        }   else {
            return $this->paypalUrl = 'PAYKEY NOT FOUND';
        }


    }
}
