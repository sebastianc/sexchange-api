<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $fillable = ['type', 'sender_id', 'user_id', 'notification_id', 'notification_type'];

    use SoftDeletes;

    public function type() {
        return $this->morphTo();
    }

    public function user () {
        return $this->belongsTo('App\User');
    }

    public function sender () {
        return $this->hasMany('App\User', 'id', 'sender_id');
    }

    /*
     * Hack:
     *
     * In Laravel, you can't eagar-load polymorphic relationships
     * This method will crap the attached class and bring back the
     * data with the notification.
     *
     * @see: https://github.com/laravel/framework/issues/1939
     *
     * To Add different types that are 'polymorphically' related to
     * notifications, a copy of the if statement needs to be added
     * below
     */
    public static function getType($notification_type, $notification_id) {
        if ( $notification_type == 'Comment') {
            return Comment::with('shoe')->find($notification_id);
        }
        if ( $notification_type == 'Follow') {
            return User::find($notification_id);
        }
        if ( $notification_type == 'ShoeLike') {
            return ShoeLike::with('shoe')->find($notification_id);
        }
        if ( $notification_type == 'Offer' ||  $notification_type == 'OfferAccepted' || $notification_type == 'OfferDeclined') {
            return ShoeOffer::with('shoe')->find($notification_id);
        }
        if ( $notification_type == 'Purchase' || $notification_type == 'Dispatched') {
            return Purchase::with('shoe')->find($notification_id);
        }
        if ( $notification_type == 'Wanted') {
            return Shoe::find($notification_id);
        }
        if ( $notification_type == 'Review') {
            return Review::with('shoe')->find($notification_id);
        }
    }
}
