<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stripe\Account;
use Stripe\Customer;

class StripeAccount extends Model
{
    protected $hidden = ['secret_key', 'publishable_key'];
    private $account = null;

    /**
     * Test if a stripe account is still linked.
     *
     * @return bool
     */
    public function getIsLinkedAttribute()
    {
        try {
            Stripe::setApiKey(env('STRIPE_SK'));
            Account::retrieve($this->account_id);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    private function loadRemote()
    {
        if ($this->account === null) {
            $this->account = Account::retrieve($this->account_id);
        }
    }

    /**
     * Get the stripe account associated with the user.
     *
     * @return mixed
     */
    public function getAccountsAttribute()
    {
        $this->loadRemote();
        return $this->account;
    }
}
