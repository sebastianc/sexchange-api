<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stripe\Account;

class StripeUser extends Model
{
    /**
     * Test if a stripe account is still linked.
     *
     * @return bool
     */
    public function isLinked()
    {
        try {
            \Stripe\Stripe::setApiKey(env('STRIPE_SK'));
            $account = Account::retrieve($this->stripe_user_id);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
