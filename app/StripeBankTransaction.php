<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeBankTransaction extends Model
{
    protected $appends = ['amount_formatted', 'date', 'time'];

    /**
     * Format the amount as money
     *
     * @return string
     */
    public function getAmountFormattedAttribute()
    {
        return '£' . number_format($this->amount, 2);
    }

    public function getDateAttribute()
    {
        return $this->created_at->format('d/m/y');
    }

    public function getTimeAttribute()
    {
        return $this->created_at->format('H.i');
    }
}
