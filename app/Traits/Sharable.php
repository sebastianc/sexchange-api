<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 28/07/2016
 * Time: 11:51
 */

namespace App\Traits;

use DOMDocument;
use Guzzle\Http\Curl\CurlHandle;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

/**
 * Trait Sharable
 *
 * TODO: Make sure this is configured correctly and is working.
 *
 * Make sure the associated table has the following columns:
 *  - int facebook_shares
 *  - int twitter_shares
 *  - int gplus_shares
 * Make sure to implement a share_url attribute.
 *
 */
trait Sharable
{
    public function __construct()
    {
        if (isset($this->appends)) {
            $this->appends[] = 'shares';
            $this->appends[] = 'share_url';
        } else {
            $this->appends = ['shares', 'share_url'];
        }

        if (isset($this->hidden)) {
            $this->hidden[] = 'facebook_shares';
            $this->hidden[] = 'twitter_shares';
            $this->hidden[] = 'gplus_shares';
        } else {
            $this->hidden = ['facebook_shares', 'twitter_shares', 'gplus_shares'];
        }
    }

    //TEMP
    public function getShareUrlAttribute()
    {
        return ['https://google.com', 'http://facebook.com'][mt_rand(0,1)];
    }

    public function getSharesAttribute()
    {
        return [
            'facebook' => $this->facebook_shares,
            'twitter' => $this->twitter_shares,
            'gplus' => $this->gplus_shares,
        ];
    }

    public function updateShares()
    {
        try {
            $client = new Client();
            $shareUrl = urlencode(env('BASE_URL').'/shoes/'.$this->slug);

            $this->updateFacebookShares($client, $shareUrl);
            $this->updateGplusShares($client, $shareUrl);
            $this->updateTwitterShares($client, $shareUrl);
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }

    private function updateFacebookShares(Client $client, $shareUrl)
    {
        $request = $client->createRequest(
            'GET',
            'http://graph.facebook.com/?id=' . $shareUrl
        );
        $response = $client->send($request);

        if ($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody()->getContents(), 1);
            if (isset($body['shares'])) {
                $this->facebook_shares = $body['shares'];
            }
        }
    }

    //New twitter function based on twitter search API
     private function updateTwitterShares(Client $client, $shareUrl)
    {
        $auth = new Oauth1([
            'consumer_key'    => env('TWITTER_CK'),
            'consumer_secret' => env('TWITTER_CS'),
            'token'           => env('TWITTER_TK'),
            'token_secret'    => env('TWITTER_TS')
        ]);

        $client->getEmitter()->attach($auth);

        try{
            $response = $client->get(
                'https://api.twitter.com/1.1/search/tweets.json?count=100&q=' . $shareUrl,
                ['auth' => 'oauth']
            );
            if ($response->getStatusCode() == 200) {
                $body = json_decode($response->getBody()->getContents(), 1);
                if (isset($body['statuses'])) {
                    $this->twitter_shares = count($body['statuses']);
                }
            }

            return $response;
        }catch (\ClientException $e){
            var_dump($e);
        }
    }

    private function updateGplusShares(Client $client, $shareUrl)
    {
        $request = $client->createRequest(
            'GET',
            'https://plusone.google.com/_/+1/fastbutton?url=' . $shareUrl
        );
        $response = $client->send($request);

        if ($response->getStatusCode() == 200) {
            libxml_use_internal_errors(true);
            $doc = new DOMDocument();
            $doc->loadHTML($response->getBody()->getContents());
            $counter = $doc->getElementById('aggregateCount')->nodeValue;

            $lastChar = substr($counter, -1);

            if ($lastChar == 'k') {
                $counter = floor(floatval($counter) * 1000);
            }
            else if ($lastChar == 'M') {
                $counter = floor(floatval($counter) * 1000000);
            }
            else {
                $counter = intval($counter);
            }

            $this->gplus_shares = $counter;
        }
    }
}