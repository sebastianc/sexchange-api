<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
//    public $average_rating;
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];

    function toArray()
    {

        $array = parent::toArray();
        if (isset($array['image'])) {
            $url = $array['image'];
            $dimensions = [
                'thumb' => str_replace('/original/', '/medium/', $url),
                'large' => str_replace('/original/', '/ios/', $url),
                'full' => $url,
            ];
            $array['image'] = $dimensions;
        }


        return $array;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'phone', 'paypal_email', 'created_at', 'updated_at', 'phone_verified', 'initial_pass_reset', 'parse_id'
    ];

    public function phone()
    {
        return $this->phone;
    }

    public function thumb()
    {
        $url = $this->image;
        if(!str_contains($url, 'graph.facebook')){
            return str_replace('original', 'small', $url);
        }else{
            return str_replace('width=1920', 'type=small', $url);
        }
    }

    public function stripeAccount()
    {
        return $this->hasOne('App\StripeAccount');
    }
}