<?php

return [

    /*
     * The fee charged on each purchase as a percentage. Includes the stripe transaction charge.
     * EG shoe sells for £20 when the fee is set to 10%. The seller will get £18 and admin will get £2.
     */

    'fee' => 3,

    /*
     * The version of the app, used for force updating.
     * Leave off any trailing zeros. EG 2.0.0 becomes 2, 2.1.0 becomes 2.1
     */

    'version' => [
        'android' => '2',
        'ios' => '2'
    ]
];