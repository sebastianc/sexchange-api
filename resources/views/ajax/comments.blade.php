@foreach( $comments as $comment)
    <div><b>{{ $comment->user->username }}</b> {{ $comment->text }}</div>
@endforeach

@if($comments->currentPage() === $comments->lastPage())
    <script>
        $('.more_comments').hide();
    </script>
@endif