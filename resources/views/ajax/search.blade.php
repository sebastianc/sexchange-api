@if($shoes->count())
    @foreach($shoes as $shoe)
        @include('partials.explore-shoe')
    @endforeach
@else
    @include('partials.no-results-shoes')
@endif


<nav id="ajax_pagination" data-url="{{URL::to($shoes->url(1))}}">
    @include('partials.paginator', ['paginator' => $shoes])
</nav>