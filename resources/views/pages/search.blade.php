@extends('templates.main')

@section('title') Explore - @stop

@section('body_class') class="explore_page" @stop

@section('content')
    <ul id="breadcrumbs" class="breadcrumbs col-xs-12 hidden-xs no-padding">
        <li class="item-home"><a class="bread-link bread-home" href="/" title="Homepage">Home</a></li>
        <li class="item-current item-7537"><span class="bread-current bread-7537"> Search results for {{session('term')}}</span></li>
    </ul>
    <div id="tabs">
        <div class="col-xs-12 no-padding">
            <ul class="nav nav-tabs pull-left">
                <li class="pull-right"><a href="{{$user_url}}" class="pull-left">Users</a></li>
                <li class="active pull-right"><a href="/" class="pull-left">Shoes</a></li>
            </ul>
            <div class="col-sm-6 pull-right ">
                <div class="pull-right text-right relative">
                    <select dir="rtl" class="filter-sort filter-btn style-select">
                        <option >Sort Results</option>
                        <option value="price_high_low" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_high_low'){ echo 'active';} ?>>Price (High to Low)</option>
                        <option value="price_low_high" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_low_high'){ echo 'active';} ?> >Price (Low to High)</option>
                        <option value="views" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'views'){ echo 'active';} ?>>Most Viewed</option>
                        <option value="recent" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'recent'){ echo 'active';} ?> >Most Recent</option>
                    </select>
                </div>
                <?php
                if(isset($_GET['load'])){
                    $load = $_GET['load'];
                }else{
                    $load = 0;
                }
                ?>
                <ul class="pull-right pagination_ul hidden-xs">
                    <li>Show: </li>
                    <li><a class="load_more <?php if($load == '50'){ echo 'active';} ?>" data-amount="50" >50</a>/</li>
                    <li><a class="load_more <?php if($load == '100'){ echo 'active';} ?>"  data-amount="100">100</a>/</li>
                    <li><a class="load_more <?php if($load == '200'){ echo 'active';} ?>"  data-amount="200">200</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 no-padding">
            @include('partials.filter-options', ['show_type_filter' => true])
            <div class="tab-content col-sm-9 col-md-10 col-xs-12 no-padding-xs">
                <div id="buy" class="tab-pane fade in active grid">
                    <div class="">
                        <ul id="search-results-gallery" class="col-sm-12 no-padding">
                            @if($shoes->count())
                                @foreach($shoes as $shoe)
                                    @include('partials.explore-shoe')
                                @endforeach
                            @else
                                @include('partials.no-results-shoes')
                            @endif
                            <nav>
                                @include('partials.paginator', ['paginator' => $shoes])
                            </nav>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

