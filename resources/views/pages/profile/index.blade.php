@extends('templates.main')

@section('title', ucfirst($type) . ' - ' . $user->username . ' - ')

@section('content')
    @include('pages.profile.header', ['extended_profile' => true])
    <div class="col-sm-6 col-md-5">
        <a href="#" class="user-options-dropdown pull-right" data-toggle="popover" data-trigger="click" data-content="
            <ul class='more-popover'>
                <li><a href='javascript:share(&quot;https://www.facebook.com/sharer/sharer.php?u={{ url('profile/' . $user->username) }}&quot;)'>Share on Facebook</a></li>
                <li><a href='javascript:share(&quot;https://twitter.com/home?status=Check%20out%20these%20shoes%20on%20{{ url('profile/' . $user->username) }}&quot;)'>Share on Twitter</a></li>
                <li><a href='javascript:share(&quot;https://plus.google.com/share?url={{ url('profile/' . $user->username) }}&quot;)'>Share on Google+</a></li>
                <li class='border-bottom col-sm-12 no-padding'><a href='javascript:share(&quot;http://pinterest.com/pin/create/button/?url={{ url('profile/' . $user->username) }}&quot;)'>Share on Pinterest</a></li>
                <li id='user-report-link' @if (!Auth::check() || !$can_report || $current_user)class='hidden'@endif><a href='{{ url('profile/' . $user->username . '/report') }}' class='red'>Report user</a></li>
                <li id='user-report-done' @if (!Auth::check() || $can_report || $current_user)class='hidden'@endif><a>User reported</a></li>
            </ul>
        " data-placement="bottom">More <img data-toggle="popover" src="{{ url('assets/images/arrow_down.png') }}" alt=""></a>
    </div>
    <div class="border-bottom margin-bottom-1 margin-top-1 col-sm-12 col-xs-12 hidden-xs"></div>

    <div class="col-sm-12 margin-bottom-1">
        <ul class="nav nav-tabs profile-nav-tabs users-profile-nav-tabs pull-left margin-bottom-1">
            <li @if($type == 'listings')class="active"@endif><a href="{{ url('profile/' . $user->username . '/listings') }}" class="pull-left">Listings</a></li>
            <li @if($type == 'likes')class="active"@endif><a href="{{ url('profile/' . $user->username . '/likes') }}" class="pull-left">Likes</a></li>
        </ul>
    </div>

    @forelse ($shoes as $shoe)
        <article class="profile-product col-sm-3">
            <a class="product-box col-sm-12 no-padding" href="{{ url('shoes/' . $shoe->slug) }}">
                @if ($shoe->sold && $shoe->purchase != null && $shoe->purchase->payment_sent == 0)
                    <span class="sold-badge btn sole-btn bordered-red-btn white-bg">Reserved</span>
                @elseif ($shoe->sold)
                    <span class="sold-badge btn sole-btn bordered-red-btn white-bg">Sold</span>
                @endif
                <img alt="new-releases-product" class="img-responsive center-block product-img" src="{{ $shoe->images->first()->large() }}">
            </a>
            <!-- Post Content -->
            <div class="prod-info">
                <div class="pricebar cf">
                    <h2>
                        <a href="{{ url('shoes/' . $shoe->slug) }}">
                            <div class="prod_title_holder"> <span class="prod_title ">{{ $shoe->name }}</span> <span class="price pull-right red">£{{ round($shoe->cost) }}</span> </div>
                            <div class="prod_size grey text-left">{{ $shoe->sizes->description }}</div>
                        </a>
                    </h2>
                </div>
            </div>
        </article>
    @empty
        @if($type == 'listings')
            @include('partials.no-results', ['text' => 'Looks like this user doesn\'t have any listings yet'])
        @else
            @include('partials.no-results', ['text' => 'Looks like this user doesn\'t have any likes yet'])
        @endif
    @endforelse

@endsection