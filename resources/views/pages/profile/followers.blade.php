@extends('templates.main')

@section('title', $title . ' - ' . $user->username . ' - ')

@section('content')
    @include('pages.profile.header')
    <ul class="nav nav-tabs profile-nav-tabs reviews-nav-tabs col-sm-6 col-md-5 pull-right margin-top-1">
        <li class="{{ $type == 'following' ? 'active ' : '' }}pull-right"><a href="{{ url('profile/' . $user->username . '/following') }}" class="pull-left">Following</a></li>
        <li class="{{ $type == 'followers' ? 'active ' : '' }}pull-right"><a href="{{ url('profile/' . $user->username . '/followers') }}" class="pull-left">Followers</a></li>
    </ul>
    <div class="border-bottom margin-bottom-1 margin-top-1 col-sm-12 col-xs-12 hidden-xs"></div>
    <div class="col-sm-12 col-xs-12">
        <div class="col-xs-12 border-bottom visible-xs"></div>
        @forelse ($follows as $follower)
            <div class="search-user-box fl-re-box">
                <div class="col-xs-2 col-sm-4 no-padding">
                    <div style="background-image: url('{{ $follower->{$property}->image }}')" class="profile-image profile-image-medium"></div>
                </div>
                <div class="pull-left col-xs-7 col-sm-8">
                    <a href="{{ url('/profile/' . $follower->{$property}->username) }}" class="dark-grey">{{ $follower->{$property}->username }}</a>
                    <div class="col-sm-12 no-padding margin-bottom-1">
                        @if ($follower->{$property}->location)
                            <div class="pull-left margin-right-1 grey"><img src="{{ url('assets/images/location_icon.png') }}" alt="location icon" class="pull-left location-icon"/>{{ $follower->{$property}->location }}</div>
                        @endif
                        <div class="pull-left hidden-xs">
                            <input value="{{ $follower->{$property}->average_rating }}" type="hidden" class="rating" data-size="s" disabled>
                        </div>
                    </div>
                </div>
                @include('partials.follow', ['user' => $follower->{$property}])
            </div>
        @empty
            @if ($type == 'followers')
                @include('partials.no-results', ['text' => Auth::user()->id == $user->id ? 'Looks like no one is following you yet' : 'Looks like no one is following ' . $user->username . ' yet'])
            @else
                @include('partials.no-results', ['text' => Auth::user()->id == $user->id ? 'Looks like you\'re not following anyone yet' : 'Looks like ' . $user->username . ' isn\'t following anyone yet'])
            @endif
        @endforelse
        <nav>
            @include('partials.paginator', ['paginator' => $follows])
        </nav>
    </div>
@endsection