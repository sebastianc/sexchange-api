<div class="col-sm-6 col-md-7 hidden-xs">
    <div class="col-xs-12 col-sm-3 col-md-2 no-padding">
        <div style="background-image: url('{{ $user->image }}')" class="profile-image profile-image-large"></div>
    </div>
    <div class="pull-left col-xs-12 col-sm-9">
        <h2 class="no-margin-top light-heading">{{ $user->username }}</h2>
        <div class="col-sm-12 no-padding margin-bottom-1">
            <div class="pull-left margin-right-1"><a href="{{ url('profile/'. $user->username .'/followers') }}"><span class="black">{{ $user->follower_count }}</span> <span class="grey">Follower{{$user->follower_count != 1 ? 's' : '' }}</span></a></div>
            <div class="pull-left"><a href="{{ url('profile/'. $user->username .'/following') }}"><span class="black">{{ $user->follow_count }}</span> <span class="grey">Following</span></a></div>
        </div>
        <div class="col-sm-12 no-padding margin-bottom-1">
            <div class="pull-left margin-right-1 grey"><img src="{{ url('assets/images/location_icon.png') }}" alt="location icon" class="pull-left location-icon"/>{{ $user->location }}</div>
            <div class="pull-left">
                <a href="{{ url('/profile/' . $user->username . '/reviews') }}"><input value="{{ $user->average_rating }}" type="hidden" class="rating" data-size="s" disabled></a>
            </div>
            @if (isset($extended_profile))
                <div class="pull-left view_reviews"><a href="{{ url('/profile/' . $user->username . '/reviews') }}" class=" grey">View all Reviews <img src="{{ url('assets/images/arrow_right.png') }}" alt=""></a></div>
            @endif
        </div>
        @if (isset($extended_profile))
            <div class="col-sm-6 col-md-4 no-padding margin-bottom-1">@include('partials.follow', ['user' => $user])</div>
            <p class="col-sm-12 no-padding">{{ $user->bio }}</p>
        @endif
    </div>
</div>