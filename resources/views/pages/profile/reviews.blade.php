@extends('templates.main')

@section('title', 'Reviews - ' . $user->username . ' - ')

@section('content')
    <div id="tabs">
        @include('pages.profile.header')
        <ul class="nav nav-tabs profile-nav-tabs reviews-nav-tabs col-sm-6 col-md-5 pull-right margin-top-1">
            <li class="{{ $type == 'sold' ? 'active ' : '' }}pull-right"><a href="{{ url('profile/' . $user->username . '/reviews/sold') }}" class="pull-left">Sold Shoes</a></li>
            <li class="{{ $type == 'purchased' ? 'active ' : '' }}pull-right"><a href="{{ url('profile/' . $user->username . '/reviews/purchased') }}" class="pull-left">Purchased Shoes</a></li>
        </ul>
        <div class="border-bottom-dark margin-top-1 col-sm-12 col-xs-12"></div>
        <div class="tab-pane">
            <div class="col-sm-12 col-xs-12">
                @forelse ($reviewGroups as $reviewGroup)
                    <div class="row review-group">
                        @foreach ($reviewGroup as $review)
                            <div class="col-sm-6 col-xs-12 review-box">
                                <div class="no-padding col-sm-12">
                                    <div class="col-xs-12 col-sm-2 col-md-1 no-padding">
                                        <div style="background-image: url('{{ $review->{$property}->image }}')" class="profile-image profile-image-small"></div>
                                    </div>
                                    <div class="pull-left col-xs-12 col-sm-9">
                                        <div>
                                            <a class="black" href="{{ url('profile/' . $review->{$property}->username) }}">{{ $review->{$property}->username }}</a>
                                            <span class="pull-right"> {{ $review->time_ago }}</span>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <div class="pull-left">
                                                <input value="{{ $review->rating }}" type="hidden" class="rating" data-size="s" disabled>
                                            </div>
                                        </div>
                                        <p>{{ $review->text }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @empty
                    @if($type == 'sold' && Auth::check() && $user->id == Auth::user()->id)
                        @include('partials.no-results', ['text' => 'Looks like you have no ratings as a seller'])
                    @elseif($type == 'sold')
                        @include('partials.no-results', ['text' => 'Looks like '.$user->username.' has no ratings as a seller', 'sub_text' => 'Why don\'t you <a href="/profile/'.$user->username.'/listings">check out their shoes</a>? '])
                    @elseif($type == 'purchased' && Auth::check() && $user->id == Auth::user()->id)
                        @include('partials.no-results', ['text' => 'Looks like you have no ratings as a buyer'])
                    @else
                        @include('partials.no-results', ['text' => 'Looks like '.$user->username.' has no ratings as a buyer', 'sub_text' => 'Why don\'t you <a href="/profile/'.$user->username.'/wanted">check out their wanted shoes</a> right now? '])
                    @endif
                @endforelse
                <nav>
                    @include('partials.paginator', ['paginator' => $reviews])
                </nav>
            </div>
        </div>
    </div>
@endsection