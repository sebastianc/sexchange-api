

@extends('templates.main')

@section('title') Explore - @stop

@section('body_class') class="explore_page" @stop

@section('content')
    <div id="tabs">
        <ul class="nav nav-tabs pull-left">
            <li class="active pull-right"><a data-toggle="tab" href="/wanted" class="pull-left">Wanted Shoes</a></li>
            <li class="pull-right"><a data-toggle="tab" href="/" class="pull-left">Buy Shoes</a></li>
        </ul>
        <div id="wanted" class="tab-pane fade grid">
            <div class="col-sm-6 pull-right no-padding">
                <div class="pull-right text-right relative">
                    <select class="filter-sort filter-btn turnintodropdown style-select">
                        <option value="" >Sort Results</option>
                        <option value="release_date" >Date Order</option>
                        <option value="date" >Newest</option>
                        <option value="price" >Price</option>
                        <option value="popular" >Popular</option>
                        <option value="week">Releasing This Week</option>
                    </select>
                </div>
                <?php
                if(isset($_GET['load'])){
                    $load = $_GET['load'];
                }else{
                    $load = 0;
                }
                ?>
                <ul class="pull-right pagination_ul hidden-xs">
                    <li>Show: </li>
                    <li><a class="load_more <?php if($load == '50'){ echo 'active';} ?>" href="?load=50">50</a>/</li>
                    <li><a class="load_more <?php if($load == '100'){ echo 'active';} ?>" href="?load=100">100</a>/</li>
                    <li><a class="load_more <?php if($load == '200'){ echo 'active';} ?>" href="?load=200">200</a></li>
                </ul>
            </div>
            @include('partials.filter-options')
            <div class="col-sm-9 col-md-10 col-xs-12">
                <ul id="search-results-gallery" class="col-sm-12 no-padding">
                    @foreach($wanted as $shoe)
                        @include('partials.explore-shoe')
                    @endforeach
                </ul>
                <nav>
                    <ul class="pagination">
                        <li class="page-item hidden-xs">Page 1 of 5</li>
                        <li class="page-item"> <a class="page-link" href="#" aria-label="Previous">Previous</a> </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item">...</li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"> <a class="page-link" href="#" aria-label="Next">Next</a> </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@stop


