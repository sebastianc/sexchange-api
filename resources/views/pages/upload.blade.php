@extends('templates.main')

@section('title') Upload - @stop

@section('body_class') class="upload_page" @stop

@section('content')
    <div id="tabs" >
        <div class="tab-content col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-12 margin-bottom-1 no-padding-xs">
                <ul class="nav nav-tabs profile-nav-tabs users-profile-nav-tabs upload-shoe-nav-tabs pull-left margin-bottom-1">
                    <li class="active"><a data-toggle="tab" href="#sell" class="pull-left">Sell a Shoe</a></li>
                    <li class=""><a data-toggle="tab" href="#wanted" class="pull-left">Post a Wanted Shoe</a></li>
                </ul>
            </div>
            <div id="sell" class="tab-pane fade in active">
                @include('partials.upload-form', ['wanted' => false ])
            </div>
            <div id="wanted" class="tab-pane fade">
                @include('partials.upload-form', ['wanted' => true ])

                {{--<div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 bordered-box">--}}
                    {{--<div class="col-sm-6 col-xs-12 no-padding ">--}}
                        {{--<form action="">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-12 col-xs-12 no-padding grey">Brand</label>--}}
                                {{--<select class="form-control cl-form-control select-control">--}}
                                    {{--<option class="grey">Choose a brand</option>--}}
                                    {{--<option>Adidas</option>--}}
                                    {{--<option>Nike</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-12 col-xs-12 no-padding grey">Shoe Name</label>--}}
                                {{--<input type="text" class="form-control cl-form-control" value=""placeholder="Type here"/>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-12 col-xs-12 no-padding grey">Cost (Including P&amp;P)</label>--}}
                                {{--<div class="input-symbol"> <span>&pound;</span>--}}
                                    {{--<input type="text" class="form-control cl-form-control" value="" placeholder="Type here"/>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-12 col-xs-12 no-padding grey">Size</label>--}}
                                {{--<select class="form-control cl-form-control select-control">--}}
                                    {{--<option class="grey">Add Size</option>--}}
                                    {{--<option>4 UK</option>--}}
                                    {{--<option>4.5 UK</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-6 no-padding profile-img-holder">--}}
                        {{--<label for="" class="col-sm-12 grey col-xs-12 no-padding-xs">Add Image</label>--}}
                        {{--<div class="col-sm-12 col-xs-12 no-padding-xs">--}}
                            {{--<form id="" action="/file-upload" class="dropzone shoe_img">--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-12 col-xs-12 no-padding-xs">--}}
                            {{--<div class="shoe_img_holder"></div>--}}
                            {{--<div class="shoe_img_holder"></div>--}}
                            {{--<div class="shoe_img_holder"></div>--}}
                            {{--<div class="shoe_img_holder"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-12 col-xs-12 no-padding ">--}}
                        {{--<form action="">--}}
                            {{--<div class="col-sm-12 col-xs-12 no-padding form-group">--}}
                                {{--<label for="" class="grey">Description</label>--}}
                                {{--<textarea type="text"class="form-control cl-form-control" value="" rows="5" placeholder="Type here"></textarea>--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-sm-6 col-xs-12  no-padding">--}}
                                {{--<label for="" class="grey">Colour</label>--}}
                                {{--<input id="colour-link" type="text" class="form-control cl-form-control pop-up-link" placeholder="Add Colour +" readonly/>--}}
                            {{--</div>--}}
                            {{--<div class="pull-right col-sm-6 col-xs-12 no-padding-xs">--}}
                                {{--<label for="" class="grey">Condition</label>--}}
                                {{--<input value="4" type="number" class="rating form-control" data-size="sm">--}}
                            {{--</div>--}}
                            {{--<div class="form-group"> <a class="white red-btn col-xs-12 col-sm-12 btn sole-btn margin-top-1">Save Changes</a> </div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@stop

@section('popups')
    <div id="colour" class="pop-up hidden white-bg pop-up-short">
        <a class="close pull-right"></a>
        <div class="col-xs-12 white-bg">
            <h3 class="col-xs-12 text-center text-capitalize post-title dark-grey margin-bottom-1 ">Add Colour</h3>
            <div  class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 margin_bottom_1">
                <form id="colour_select">
                    @foreach($filters['colours'] as $colour)
                        <label class="checkbox-inline btn sole-btn bordered-grey-btn">
                            <input type="checkbox" name="{{$colour->name}}" value="{{$colour->id}}">
                            {{$colour->name}}
                        </label>
                    @endforeach
                </form>
            </div>
            <div class="height10"></div>
            <div class=" col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin-top-1">
                <div class="form_wrap">
                    <a class="btn red-btn col-xs-12" id="add_colours" href="#" onclick="" >Done</a>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer_scripts')
    <script>
        $(document).ready(function(){
            $('#add_colours').click(function(e){
                e.preventDefault();
                var selected = $('#colour_select').serializeArray();
                $('.tab-pane.active #selected_colours').val(JSON.stringify(selected));

                $.each( selected, function( key, value ) {
                    var old = $('.tab-pane.active #colour-link').val()
                    if(key){
                        $('.tab-pane.active #colour-link').val(old + ', '+value.name)
                    }else{
                        $('.tab-pane.active #colour-link').val(value.name)
                    }
                });

                $('#colour .close').click();
            });

            $(".shoe_image_input").on('change', function () {

                if (typeof (FileReader) != "undefined") {

                    var image = $(this).data('image');
                    var image_holder = $(this).parents("#image_"+image+"_preview");
//                    image_holder.empty();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(image_holder).css({'background-image' : 'url('+e.target.result+')', 'background-size' : 'cover'});
                        $(image_holder).children('.upload_text').addClass('hidden');

                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[0]);
                    $('#save-pic').removeAttr('disabled');
                } else {
                    alert("This browser does not support FileReader.");
                }
            });





        });
    </script>
@stop
