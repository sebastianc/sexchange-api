@extends('templates.main')

@section('title', 'Bank Account - ')

@section('content')
    <div id="tabs">
        <div class="tab-content col-sm-12 col-xs-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 bordered-box">
                <h3>{{ $first_setup ? 'Set up a Seller Account' : 'Edit Seller Account' }}</h3>
                @if ($first_setup)
                    <p class="bank-setup-text">
                        Before you upload a shoe we need you to provide the following details.
                    </p>
                    <p class="bank-setup-text">
                        The Sole Marketplace uses a securely managed payment system allowing buyers to connect with sellers and send payments without worry. Buyers payments made inside the website are protected by the Sole Exchange Guarantee system which you can read more about in the Terms & Conditions.
                    </p>
                @endif
                <form action="{{ url('payment/edit-bank') }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-xs-12 no-padding border-bottom margin-top-1">
                        <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">First Name</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['first_name'] }}" name="first_name" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Last Name</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['last_name'] }}" name="last_name" placeholder="Type here"/>
                        </div>
                    </div>
                    <div class="col-sm-12 border-bottom no-padding margin-top-1">
                        <div class="form-group col-xs-6 no-padding-left no-padding-right-xsgit ">
                            <label for="" class=" grey">Date of Birth</label>
                            <input class="form-control cl-form-control" value="{{ $details['date_of_birth'] }}" id="date-of-birth" name="date_of_birth" placeholder="dd/mm/yyyy" type="text" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 no-padding margin-top-1">
                        <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Address Line</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['address_line_one'] }}" name="address_line_one" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Address Line 2</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['address_line_two'] }}" name="address_line_two" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">City</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['address_city'] }}" name="address_city" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">County</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['address_county'] }}" name="address_county" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Country</label>
                            <input type="text" class="form-control cl-form-control" value="United Kingdom" disabled="disabled" placeholder="Type here"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12 no-padding">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Postcode</label>
                            <input type="text" class="form-control cl-form-control" value="{{ $details['address_postal_code'] }}"  name="address_postal_code" placeholder="Type here"/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 no-padding">
                        <h3>Bank Details</h3>
                        <div class="form-group col-xs-6 no-padding-left no-padding-right-xs">
                            <label for="" class="col-sm-12 col-xs-12 no-padding grey">Account Holder Name</label>
                            <input type="text" class="form-control cl-form-control" value="{{ old('account_holder_name') }}" name="account_holder_name" placeholder="Type here"/>
                        </div>
                        <div class="col-sm-12 col-xs-12 no-padding">
                            <div class="form-group col-xs-6 no-padding-left no-padding-right-xs">
                                <label for="" class="col-sm-12 col-xs-12 no-padding grey">Account Number</label>
                                <input type="text" class="form-control cl-form-control" value="{{ old('account_number') }}" name="account_number" maxlength="8" placeholder="Type here"/>
                            </div>
                            <div class="form-group col-sm-6 col-xs-12 no-padding">
                                <label for="sort-code" class="col-sm-12 col-xs-12 no-padding grey">Sort Code</label>
                                <input type="text" id="sort-code" class="form-control cl-form-control" value="{{ old('sort_code') }}" name="sort_code" placeholder="Type here"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="white red-btn col-xs-12 col-sm-12 btn sole-btn margin-top-1 pay-btn">Done <img src="{{ url('assets/images/padlock.png') }}" alt="" class="btn-image"></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $('#sort-code').mask('00-00-00');
        $('#date-of-birth').datepicker({
            minDate: '-100y',
            maxDate: '-15y',
            format: 'dd/mm/yyyy',
            orientation: 'bottom'
        });
    </script>
@endsection