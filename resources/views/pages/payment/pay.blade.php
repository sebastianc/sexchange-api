@extends('templates.main')

@section('title', 'Pay - ')

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 bordered-box">
        <form method="post" id="pay-form" action="{{ url('payment/pay') }}">
            <input type="hidden" name="stripe_token" value="" id="stripe-input">
            <input type="hidden" name="shoe_id" value="{{ $shoe->id }}">
            <div class="col-xs-12 no-padding">
                <h3>Order Summary</h3>
                <div class="col-sm-12 no-padding col-xs-12 border-bottom">
                    <div class="col-sm-2 col-xs-2 no-padding margin-bottom-1"> <img src="{{ $shoe->images->first()->small() }}" class="img-responsive pull-left"/> </div>
                    <div class="col-sm-4 col-xs-6">
                        <p class="grey hidden-xs">Shoe</p>
                        <p>{{ $shoe->name }}</p>
                    </div>
                </div>
                <div class="col-xs-12 no-padding border-bottom">
                    <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Address Line 1</label>
                        <input type="text" class="form-control cl-form-control" name="address_line_one">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12 no-padding">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Address Line 2</label>
                        <input type="text" class="form-control cl-form-control" name="address_line_two">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">County</label>
                        <input type="text" class="form-control cl-form-control" name="address_county">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12 no-padding">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Postcode</label>
                        <input type="text" class="form-control cl-form-control" name="address_postcode">
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding border-bottom">
                    <h3>Payment</h3>
                    <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Card Number</label>
                        <input type="text" class="form-control cl-form-control" id="card-number" placeholder="0000 0000 0000 0000">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12 no-padding">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Expiry Date</label>
                        <input type="text" class="form-control cl-form-control" id="card-expiry" placeholder="MM/YY">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12 no-padding-left no-padding-right-xs">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">CVC</label>
                        <input type="password" class="form-control cl-form-control" id="card-cvc" maxlength="3" placeholder="000">
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding ">
                    <h3 class="col-xs-12 no-padding">
                        Total <span class="pull-right">£{{ number_format($offer ? $offer->amount : $shoe->cost, 2) }}</span>
                    </h3>
                    <div class="form-group">
                        <button type="submit" class="white green-btn col-xs-12 col-sm-12 btn sole-btn margin-top-1 pay-btn">Pay <img src="{{ url('assets/images/padlock.png') }}" alt="" class="btn-image"></button>
                    </div>
                    <small class="col-xs-12 no-padding margin-bottom-1 margin-top-1 grey">By clicking PLACE ORDER you confirm that you have read and understood our <a href="{{ url('terms') }}"><u>Terms and Conditions</u></a>, <a href="{{ url('returns') }}"><u>Returns Policy</u></a> and <a href="{{ url('privacy') }}"><u>Privacy Policy</u></a>.</small>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">

        Stripe.setPublishableKey('{{ env('STRIPE_PK') }}');

        var splitCardExpiry = function (expiry) {
            try {
                var parts = expiry.split('/');
                return {
                    'month': parts[0],
                    'year': parts[1]
                }
            } catch (error) {
                return false
            }
        };
        
        $('#pay-form').submit(function(e) {
            e.preventDefault();
            $('.ajax_loader').fadeIn();

            //Create a stripe token

            var stripeInput = $('#stripe-input');

            var cardExpiry = splitCardExpiry($('#card-expiry').val());
            if (!cardExpiry) {
                $('.ajax_loader').fadeOut();
                return createStatusMessage('Please enter a valid card expiry date', false);
            }

            Stripe.card.createToken({
                number: $('#card-number').val(),
                exp_month: cardExpiry.month,
                exp_year: cardExpiry.year,
                cvc: $('#card-cvc').val()
            }, function(status, response) {

                if (status !== 200) {
                    $('.ajax_loader').fadeOut();
                    return createStatusMessage('Please enter valid card details.', false)
                }

                stripeInput.val(response.id);

                //Post to server

                $.post(BASE_URL + '/payment/pay', $('#pay-form').serialize())
                    .done(function(response) {
                        window.location = BASE_URL + '/payment/complete/' + response.purchaseId;
                    })
                    .fail(function(response) {
                        $('.ajax_loader').fadeOut();
                        createStatusMessage(response.responseJSON.error);
                    });
            });
        });

        $('#card-number').mask('0000 0000 0000 0000');
        $('#card-expiry').mask('00/00');
    </script>
@endsection