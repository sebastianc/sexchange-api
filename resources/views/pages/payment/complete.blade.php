@extends('templates.main')

@section('title', 'Transaction Complete - ')

@section('content')
    <div class="transaction-complete-box">
        <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 white-bg bordered-box">
            <div class=" col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-bottom-1 margin-top-1 no-padding text-center">
                <img src="{{ url('assets/images/check.png') }}" alt="">
            </div>
            <div class=" col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-1">
                <h4 class="grey text-center">Transaction Complete!</h4>
                <p class="grey text-center">Your order has been sent, so keep an eye on your emails.</p>
            </div>
            <div class=" col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 margin-top-1 margin-bottom-1">
                <a class="white red-btn col-xs-12 col-sm-12 btn sole-btn margin-top-1" href="{{ url('account/transactions/purchases') }}">View Order</a>
            </div>
        </div>
    </div>
@endsection
