@extends('templates.main')

@section('title', 'Notifications - ')

@section('content')

    <div class="col-xs-12 no-padding visible-mobile margin-bottom-1 margin-top-1">
        <div class="pull-right text-right relative">
            <select dir="rtl" id="notifications-filter" class="filter-sort filter-btn turnintodropdown style-select">
                <option value="">More</option>
                <option value="all">Show all</option>
                <option value="Comment" data-human-type="comment">Comments</option>
                <option value="Follow" data-human-type="follower">Follows</option>
                <option value="ShoeLike" data-human-type="shoe like">Likes</option>
                <option value="Offer" data-human-type="offer">Offers</option>
                <option value="Purchase" data-human-type="purchase">Purchases</option>
                <option value="Dispatched" data-human-type="dispatch">Dispatches</option>
                <option value="Wanted" data-human-type="wanted shoe">Wanted</option>
            </select>
        </div>
    </div>
    <div class="white-bg col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 notifications no-padding">
        <div class="notification-list">
            @include('partials.notification-list')
        </div>
        <div class="notifications-loading-container">
            <div class="notifications-loading"></div>
        </div>
        <div class="notifications-empty">
            @include('partials.no-results', ['text' => 'Looks like you don\'t have any notifications', 'sub_text' => ''])
        </div>
    </div>

@stop


@section('footer_scripts')

    <script>
        $(document).ready(function(){

            var type = 'all';
            var humanType = 'all';
            var isLoading = false;
            var currentPage = 1;
            var everythingLoaded = false;

            var bindDeletes = function() {
                $('.delete-notification').unbind();
                $('.delete-notification').click(function() {
                    var notificationId = $(this).data('notification-id');
                    $.post(BASE_URL + '/notifications/delete/' + notificationId);
                    $(this).parent().fadeOut(200);
                    loadMore($('.notifications'));
                });
                toggleEmpty();
            };

            var toggleEmpty = function() {
                var emptyElement = $('.notifications-empty');
                if($('.notification-list').children().length == 0) {
                    var text = (type == 'all') ?
                        'Looks like you don\'t have any notifications' :
                        'Looks like you don\'t have any ' + humanType + ' notifications';
                    emptyElement.show();
                    emptyElement.find('h4').text(text);
                } else {
                    emptyElement.hide();
                }
            };

            var loadMore = function(element) {
                var offset = $(element).scrollTop() + $(element).innerHeight();
                var height = $(element)[0].scrollHeight;
                var loadingElement = $('.notifications-loading-container');
                var emptyElement = $('.notifications-empty');
                emptyElement.hide();

                if (height-offset < 200 && isLoading !== true && everythingLoaded == false) {
                    isLoading = true;
                    loadingElement.show();
                    currentPage += 1;
                    $.get(BASE_URL + '/notifications/load-more/'+ type +'?page=' + currentPage, function(data, statusText, xhr) {

                        if (xhr.status == 204) {
                            everythingLoaded = true;
                        } else {
                            $('.notification-list').append($(data));
                        }

                        isLoading = false;
                        loadingElement.hide();

                        toggleEmpty();
                        bindDeletes();
                    });
                }
            };

            $('.notifications').scroll(function(){
                loadMore(this);
            });

            $('#notifications-filter').change(function(){
                currentPage = 0;
                everythingLoaded = false;
                type = $(this).val();
                humanType = $(this).find(':selected').data('human-type');
                $('.notification-list').empty();
                loadMore($('.notifications'));
            });

            bindDeletes();
            toggleEmpty();
        });
    </script>

@stop