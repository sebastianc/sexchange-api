@extends('templates.main')

@section('title') Upload - @stop

@section('body_class') class="edit_shoe_page" @stop

@section('content')
    <div id="tabs" >
        <div class="tab-content col-sm-12 col-xs-12 no-padding">
            <div id="sell" class="tab-pane fade in active">
                @include('partials.upload-form', ['wanted' => $shoe->wanted ])
            </div>
        </div>
    </div>

@stop

@section('popups')
    <div id="colour" class="pop-up hidden white-bg pop-up-short">
        <a class="close pull-right"></a>
        <div class="col-xs-12 white-bg">
            <h3 class="col-xs-12 text-center text-capitalize post-title dark-grey margin-bottom-1 ">Add Colour</h3>
            <div  class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 margin_bottom_1">
                <form id="colour_select">
                    @foreach($filters['colours'] as $colour)
                        <label class="checkbox-inline btn sole-btn bordered-grey-btn @if(isset($selected_colors) && in_array($colour->id, $selected_colors) ) checked @endif">
                            <input type="checkbox" name="{{$colour->name}}" value="{{$colour->id}}" @if(isset($selected_colors) && in_array($colour->id, $selected_colors) ) checked @endif>
                            {{$colour->name}}
                        </label>
                    @endforeach
                </form>
            </div>
            <div class="height10"></div>
            <div class=" col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin-top-1">
                <div class="form_wrap">
                    <a class="btn red-btn col-xs-12" id="add_colours" href="#" onclick="" >Done</a>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer_scripts')
    <script>
        $(document).ready(function(){
            $('#add_colours').click(function(e){
                e.preventDefault();
                var selected = $('#colour_select').serializeArray();
                $('.tab-pane.active #selected_colours').val(JSON.stringify(selected));

                $.each( selected, function( key, value ) {
                    var old = $('.tab-pane.active #colour-link').val()
                    if(key){
                        $('.tab-pane.active #colour-link').val(old + ', '+value.name)
                    }else{
                        $('.tab-pane.active #colour-link').val(value.name)
                    }
                });

                $('#colour .close').click();
            });

            $(".shoe_image_input").on('change', function () {

                if (typeof (FileReader) != "undefined") {

                    var image = $(this).data('image');
                    var image_holder = $(this).parents("#image_"+image+"_preview");
//                    image_holder.empty();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(image_holder).css({'background-image' : 'url('+e.target.result+')', 'background-size' : 'cover'});
                        $(image_holder).children('.upload_text').addClass('hidden');

                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[0]);
                    $('#save-pic').removeAttr('disabled');
                } else {
                    alert("This browser does not support FileReader.");
                }
            });





        });
    </script>
@stop
