@extends('templates.main')

@section('title') Log In - @stop

@section('body_class') class="login_page" @stop

@section('content')
    @include('partials.login-form')
@stop


