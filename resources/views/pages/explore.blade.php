@extends('templates.main')

@section('title') Explore - @stop

@section('body_class') class="explore_page" @stop

@section('content')
    <div id="tabs">
        <div class="col-xs-12">
            <ul class="nav nav-tabs pull-left">
                <li class="@if($wanted) active @endif pull-right"><a href="/wanted" class="pull-left">Wanted Shoes</a></li>
                <li class="@if(!$wanted) active @endif pull-right"><a href="/" class="pull-left">Buy Shoes</a></li>
            </ul>

            <input type="hidden" value="{{$wanted}}" id="wanted_shoes" >
        <div class="col-sm-6 pull-right ">
            <div class="pull-right text-right relative sort-results">
                <select dir="rtl" class="filter-sort filter-btn style-select">
                    <option >Sort Results</option>
                    <option value="price_high_low" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_high_low'){ echo 'active';} ?>>Price (High to Low)</option>
                    <option value="price_low_high" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_low_high'){ echo 'active';} ?> >Price (Low to High)</option>
                    <option value="views" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'views'){ echo 'active';} ?>>Most Viewed</option>
                    <option value="recent" <?php if(isset($_GET['sortBy']) && $_GET['sortBy'] == 'recent'){ echo 'active';} ?> >Most Recent</option>
                </select>
            </div>
            <?php
            if(isset($_GET['per_page'])){
                $load = $_GET['per_page'];
            }else{
                $load = 20;
            }
            ?>
            <ul class="pull-right pagination_ul hidden-xs">
                <li>Show: </li>
                <li><a class="load_more <?php if($load == '50'){ echo 'selected';} ?>" data-amount="50" >50</a>/</li>
                <li><a class="load_more <?php if($load == '100'){ echo 'selected';} ?>"  data-amount="100">100</a>/</li>
                <li><a class="load_more <?php if($load == '200'){ echo 'selected';} ?>"  data-amount="200">200</a></li>
            </ul>
        </div>
    </div>
        <div class="col-xs-12">
            @include('partials.filter-options')
            <div class="tab-content col-sm-9 col-md-10 col-xs-12 no-padding-xs">
                <div id="buy" class="tab-pane fade in active grid">
                    <div class="">
                        <ul id="search-results-gallery" class="col-sm-12 no-padding">
                            @foreach($shoes as $shoe)
                                @include('partials.explore-shoe')
                            @endforeach
                            <nav>
                                @include('partials.paginator', ['paginator' => $shoes])
                            </nav>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop


