<li>
    <article class="col-xs-6 col-sm-4 col-md-3 products">
        <figure class="featured-thumbnail thumbnail product-box "> <a href="/shoes/{{$shoe->slug}}">
                <div class="mk-image-overlay"></div>
                <img width="300" height="181" src="{{$shoe->images->first()->medium()}}" class="" alt=""> </a></figure>
        <div class="prod-info">
            <div class="pricebar cf">
                <h2><a href="">
                        <div class="prod_title release_dates_title"> {{$shoe->name}}  </div><span class="price pull-right red">£{{$shoe->cost}}</span>
                    </a> </h2>
                <a href=""> <span class="prod_size grey pull-left"> @if(isset($shoe->sizes)) UK {{$shoe->sizes->size}} @endif @if(isset($shoe->sizes)){{$shoe->sizes->gender}} @endif </span></a> </div>
        </div>
    </article>
</li>


