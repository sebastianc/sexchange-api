@extends('templates.main')

@section('title', 'Shoe - ')

@section('meta')
    <meta property="og:image" content="{{$shoe->images->first()->large()}}">
    <meta property="og:title" content="{{$shoe->name}}">
@stop


@section('content')
    <div class="pop-up pop-up-short white-bg offer-sent-box hidden" id="make-shoe-offer-confirmation">
        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1">
            <h2 class="text-center">Offer Sent!</h2>
            <div class="col-sm-12 no-padding margin-bottom-1">
                <div class="grey text-center">Your offer has been sent to the seller of this shoe. To track the progress of your offer, go to My Offers.</div>
            </div>
        </div>
        <div class=" col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-bottom-1 no-padding text-center">
            <img src="{{ url('assets/images/offer-sent.png') }}" alt="">
        </div>
        <div class=" col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-1">
            <div class="grey text-center">You can also check updates on the Exchange App.</div>
        </div>
        <div class=" col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-1">
            <a class="white red-btn col-xs-12 col-sm-12 btn sole-btn margin-top-1" id="make-shoe-offer-confirmation-button">Done</a>
        </div>
    </div>
    <input type="hidden" id="shoe_id" value="{{ $shoe->id }}">
    <section class="col-md-8 no-padding-xs col-xs-12" role="main">
        <article class="product">
            @if($shoe->images->count() > 1)
                <div class="lightSlider_holder">
                    <div id="gallery" class="content">
                        <div id="controls" class="controls"></div>
                        <div class="slideshow-container">
                            <div id="loading" class="loader"></div>
                            <div id="slideshow" class="slideshow"></div>
                        </div>
                        <div id="caption" class="caption-container"></div>
                    </div>
                    <div id="thumbs" class="navigation">
                        <ul class="thumbs">
                            @foreach($shoe->images as $image)
                                <li>
                                    <a class="thumb" href="{{$image->xl()}}" title="">
                                        <img src="{{$image->small()}}" alt="" />
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @if(!$shoe->wanted)
                        <div class="sticker_wrap">
                            <span class="price-tag btn red-btn">£{{ number_format($shoe->cost, 2) }}</span>
                            <span class="size-tag btn red-btn">{{ $shoe->sizes->description }}</span>
                        </div>
                    @else
                        <div class="sticker_wrap">
                            <span class="wanted-tag btn white-btn">Wanted</span>
                            <span class="price-tag btn red-btn">{{ $shoe->sizes->description }}</span>
                        </div>
                    @endif
                </div>
            @else
                <div class="main_image">
                    <div class="slideshow-container">
                        <div class="slideshow">
                            <div class="image-wrapper">
                                <img src="{{$shoe->images->first()->xl()}}" alt="" />
                            </div>
                        </div>
                    </div>
                    @if(!$shoe->wanted)
                        <div class="sticker_wrap">
                            <span class="price-tag btn red-btn">£{{ number_format($shoe->cost, 2) }}</span>
                            <span class="size-tag btn red-btn">{{ $shoe->sizes->description }}</span>
                        </div>
                    @else
                        <div class="sticker_wrap">
                            <span class="wanted-tag btn white-btn">Wanted</span>
                            <span class="price-tag btn red-btn">{{ $shoe->sizes->description }}</span>
                        </div>
                    @endif
                </div>
            @endif
            <h2 class="big_title">{{ $shoe->name }}</h2>
            <div class="col-sm-12 no-padding product-info">
                <div> <span class="col-sm-3 col-md-2 no-padding-left">Uploaded</span> {{ $shoe->created_at->format('d/m/y - H:i') }}</div>
                @if (!$shoe->wanted)<div><span class="col-sm-3 col-md-2 no-padding-left">Total Cost</span> £{{ number_format($shoe->cost, 2) }} (inc. P&amp;P)</div>@endif
                <div>
                    <span class="col-sm-3 col-md-2 no-padding-left">Colour</span>
                    @foreach($shoe->colors as $index => $color)
                        {{$color->name}}@if ($index != $shoe->colors->count()-1),@endif
                    @endforeach
                </div>
                @if (!$shoe->wanted)
                    <div>
                        <span class="col-sm-3 col-md-2 no-padding-left">Condition</span>
                        <input type="text" class="rating rating-loading col-sm-8" value="{{ $shoe->condition }}" data-size="xxs" title="" disabled>
                    </div>
                    <div><span class="col-sm-3 col-md-2 no-padding-left">Delivery</span> {{ $shoe->delivery }}</div>
                @endif
            </div>
            <div class="col-xs-12 no-padding product-description">
                <p>{!! nl2br($shoe->description) !!}</p>
            </div>
            @include('partials.share')
        </article>
    </section>
    <aside class="col-md-4 col-sm-12 col-xs-12" role="complementary">
        <div class="bordered-box col-sm-12">
            <div class="col-xs-12 col-sm-4 col-md-2 no-padding"> <div style="background-image: url('{{$shoe->sellerUser->thumb()}}')" class="profile-image profile-image-medium"></div> </div>
            <div class="pull-left col-xs-12 col-sm-10">
                <h2 class="no-margin-top product-title username">
                    <a href="{{ url('profile/' . $shoe->sellerUser->username) }}">{{ $shoe->sellerUser->username }}</a>
                </h2>
                <div class="col-sm-12 no-padding margin-bottom-1">
                    <div class="pull-left margin-right-1 followers_shoe_page"> {{ $shoe->sellerUser->follower_count }} <span class="grey">Follower{{$shoe->sellerUser->follower_count != 1 ? 's' : '' }}</span></div>
                    <div class="pull-left"> {{ $shoe->sellerUser->follow_count}} <span class="grey">Following</span></div>
                </div>
                <div class="col-sm-12 no-padding">
                    <div class="pull-left margin-right-1 grey followers_shoe_page "><img src="{{ asset('assets/images/location_icon.png') }}" alt="location icon" class="pull-left location-icon"/>{{ $shoe->sellerUser->location }}</div>
                    <div class="pull-left">
                        <input value="{{ $shoe->sellerUser->average_rating }}" type="hidden" class="rating" data-size="s" disabled>
                    </div>
                </div>
                @include('partials.follow', ['user' => $shoe->sellerUser])
                @if (Auth::check() && $can_report && !$current_user)
                    <a href="{{ url('profile/' . $shoe->sellerUser->username . '/report') }}" class="col-sm-12 btn sole-btn bordered-grey-btn pull-left margin-top-1">Report</a>
                @elseif (Auth::check() && !$can_report && !$current_user)
                    <a class="col-sm-12 btn sole-btn bordered-grey-btn pull-left margin-top-1 no-click-btn">User Reported</a>
                @endif
            </div>
            <div class="border-bottom col-sm-12 height2 margin-bottom-1"> </div>
            @if ($shoe->sold && $shoe->purchase && $shoe->purchase->payment_sent == 0)
                <p class="grey">
                    This shoe is reserved. If the user doesn't complete the purchase it will become available again.
                </p>
                <div class="border-bottom col-sm-12 margin-top-1"> </div>
            @elseif ($shoe->sold && $shoe->purchase)
                <p class="grey">
                    This shoe was sold on {{ $shoe->purchase->created_at->format('d/m/Y') }}.
                </p>
                <div class="border-bottom col-sm-12 margin-top-1"> </div>
            @elseif ($offer)
                <p class="grey">
                    Your offer on this shoe was accepted and the shoe is now reduced from £{{ number_format($shoe->cost, 2) }} to £{{ number_format($offer->amount, 2) }}.
                </p>
                <div class="border-bottom col-sm-12 margin-top-1"> </div>
            @endif
            <h2 class="col-sm-12 no-padding no-margin-bottom product-title"><span class="grey">Size</span><span class="pull-right">{{ $shoe->sizes->description }}</span></h2>

            @if(!$shoe->wanted && !$shoe->sold && !$shoe->belongsToCurrentUser())
                <h5 class="col-sm-12 no-padding no-margin-top product-title">
                    <span class="grey">Total:</span><span class="pull-right">£{{ number_format(($offer ? $offer->amount : $shoe->cost), 2) }}</span>
                </h5>
                <a href="{{ url( $offer ? 'payment/offer/' . $offer->id : 'payment/shoe/' . $shoe->id) }}" class="btn green-btn paypal-btn col-sm-12 margin-top-1">Buy Now</a>
                <span class="col-sm-12 text-center margin-bottom-1 margin-top-1">or</span>
                <form class="clear">
                    <div class="form-group">
                        <div class="input-symbol"> <span class="grey">£</span>
                            <input type="number" name="cost" class="form-control cl-form-control" value="" id="make-shoe-offer-value" placeholder="Offer Amount">
                        </div>
                        <a class="btn red-btn col-sm-12 margin-top-1 margin-bottom-1 make-shoe-offer-btn">Make an Offer</a>
                        <p class="grey">Make your offer reasonable for the best chance of approval</p>
                    </div>
                </form>
            @elseif ($shoe->wanted && !$shoe->belongsToCurrentUser())
                <a class="btn red-btn col-sm-12 margin-top-1 margin-bottom-1" href="/upload/shoe?wanted={{$shoe->slug}}">I've got this shoe!</a>
                <p class="grey">Let {{ $shoe->sellerUser->username }} know that you have this shoe. Then sell it to them!</p>
            @endif
        </div>

        <div class="bordered-box col-sm-12 margin-top-1">
            <a class="{{ $shoe->has_liked ? 'red' : 'grey' }} favourite-box small-box">
                <img src="{{ asset('assets/images/favourite' . ($shoe->has_liked ? '_active' : '') . '.png') }}" alt="">
                <span>{{ $shoe->like_count }}</span>
            </a>
            <span class="grey small-box comment-box">
                <img src="{{ asset('assets/images/comment.png') }}" alt=""><span class="comment_count">{{ $shoe->comment_count }}</span>
            </span>
            <div class="border-bottom col-sm-12"> </div>
            <div class="comments-area-holder">
                <div class="col-sm-12 no-padding comments-area">
                    <div class="comments_wrap">
                        @foreach( $shoe->webComments as $comment)
                            <div><b>{{ $comment->user->username }}</b> {{ $comment->text }}</div>
                        @endforeach
                    </div>
                    @if($shoe->webComments->count() < $shoe->comment_count )
                        <a class="more_comments" data-page="2">Load more</a>
                    @endif
                </div>
            </div>
            @if(Auth::check())
                <form class="comments-form col-sm-12 no-padding">
                    <div class="form-group">
                        <label>
                            <img src="{{ asset('assets/images/comment.png') }}" alt="">
                        </label>
                        <textarea id="comment_input" class="form-control cl-form-control expand" value="" rows="1" placeholder="Comment" data-toggle="collapse" data-target="#demo"></textarea>
                    </div>
                        <div id="demo" class="collapse form-group">
                            <a class="btn red-btn col-sm-12 margin-top-1 submit_comment">Comment</a>
                        </div>
                </form>
            @else
                <a id="register-link" href="#" class="pop-up-link login_to_comment" >Login to comment</a>
            @endif
        </div>

    </aside>

@stop


@section('footer_scripts')

    <script>
        $(document).ready(function(){
            var logged_in = '{{Auth::check()}}';
//            $(".comments-area").scrollTop($('.comments-area').height());

            // Offer button
            $('.make-shoe-offer-btn').click(function() {
                if (logged_in === '') {
                    return createStatusMessage('Please log in to make an offer on a shoe.', false);
                }

                var value = $('#make-shoe-offer-value').val();
                var loader = $('.ajax_loader');
                var confirmation = $('#make-shoe-offer-confirmation');

                if (isNaN(value) || value <= 0 || !value) {
                    return createStatusMessage('Please enter a valid offer amount.', false);
                }

                loader.fadeIn(300);
                $.post(BASE_URL + '/shoes/offer/{{ $shoe->id }}', {'amount': value})
                    .done(function() {
                        $('#make-shoe-offer-value').val('');
                        $(".box-overlay").addClass("box-overlay-dark");
                        $(".box-overlay").toggleClass("hidden");
                        confirmation.delay(300).removeClass('hidden');
                    }).fail(function(data) {
                        if (data.responseJSON.message) {
                            createStatusMessage(data.responseJSON.message, false);
                        } else {

                            createStatusMessage('Unable to make an offer on the shoe at the moment.', false);
                        }
                    }).always(function() {
                        loader.fadeOut(300);
                    });
            });

            $('#make-shoe-offer-confirmation-button').click(function() {
                $(".box-overlay").addClass("hidden");
                $('#make-shoe-offer-confirmation').addClass('hidden');
            });

            // favourite button
            $('.favourite-box').click(function () {
                if(logged_in !== '' ){
                    $(this).toggleClass("red");
                    $(this).toggleClass("grey");
                    var buttonCount = $(this).find('span');
                    var shoeId = $('#shoe_id').val();
                    if( $(this).hasClass("red") ){
                        $(this).find("img").attr('src', BASE_URL + '/assets/images/favourite_active.png');
                        $.post(BASE_URL + '/shoes/like/' + shoeId);
                        buttonCount.text(parseInt(buttonCount.text())+1);
                    }
                    else{
                        $(this).find("img").attr('src', BASE_URL + '/assets/images/favourite.png');
                        $.post(BASE_URL + '/shoes/unlike/' + shoeId);
                        buttonCount.text(parseInt(buttonCount.text())-1);
                    }
                }else{
                    $('.error_message').dequeue();
                    $('.error_message').html('Please log in to like a shoe.');
                    $('.error_message').fadeIn().delay(4000).fadeOut(900);
                }
            });

            $('.more_comments').click(function () {
                $('.ajax_loader').fadeIn();
                var page = $('.more_comments').attr('data-page');
                var data = {
                    _token:'{{csrf_token()}}',
                    page: page,
                    shoe: '{{$shoe->id}}'
                };
                $.post(
                        "/shoes/load-comments?page="+page,
                        data,
                        function(response) {
                            if(response){
                                //console.log(response);
                                $('.more_comments').attr('data-page', parseInt(page) + 1);
                                $('.comments_wrap').append(response);
                                $('.ajax_loader').fadeOut();
                            }else{
                                $('.ajax_loader').fadeOut();
                                $('.more_comments').hide();
                            }
                        }
                );
            });

            $('.submit_comment').click(function () {

                var comment = $('#comment_input').val();
                if(comment){
                    $('.ajax_loader').fadeIn();
                    var data = {
                        _token:'{{csrf_token()}}',
                        comment: comment,
                        shoe: '{{$shoe->id}}'
                    };
                    $.post(
                            "/shoes/comment",
                            data,
                            function(response) {
                                if(response){
                                    //console.log(response);
                                    $('.comments_wrap').prepend(response);
                                    $('#comment_input').val('');
                                    $('.comment_count').html(parseInt($('.comment_count').html()) + 1);
                                    $(".comments-area").animate({ scrollTop: 0 }, "slow");
                                    $('.ajax_loader').fadeOut();
                                }else{
                                    $('.ajax_loader').fadeOut();
                                    $('.more_comments').hide();
                                }
                            }
                    );
                }else{
                    $('.error_message').dequeue();
                    $('.error_message').html('Please enter a comment before posting.');
                    $('.error_message').fadeIn().delay(4000).fadeOut(900);
                }
            });
        })
    </script>

@stop