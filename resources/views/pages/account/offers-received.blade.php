@extends('templates.main')


@section('title', 'My Offers - Account - ')

@section('body_class') class="account_page" @stop

@section('content')
    <div class="white-bg col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 offers-info-page">
        @forelse ($shoes as $index => $shoe)
            <article class="col-sm-12 bordered-box margin-bottom-1">
                @if ($index == 0)
                    <ul class="nav nav-tabs transactions-nav-tabs col-sm-6 col-md-5 col-xs-12">
                        <li class="active"><a href="{{ url('account/offers/received') }}" class="pull-left">Received Offers</a></li>
                        <li><a href="{{ url('account/offers/sent') }}" class="pull-left">Sent Offers</a></li>
                    </ul>
                @endif
                <div class="col-sm-6 no-padding-left col-xs-12 no-padding-xs margin-top-1 clear">
                    <div class="border-right col-sm-12 no-padding col-xs-12">
                        <div class="col-sm-2 col-xs-2 no-padding"> <img src="{{ $shoe->images->first()->small() }}" class="img-responsive pull-left"/> </div>
                        <div class="col-sm-4 col-xs-10">
                            <p class="grey hidden-xs">Shoe</p>
                            <p><a href="{{ url('shoes/' . $shoe->slug) }}" class="black">{{ $shoe->name }}</a></p>
                        </div>
                        <div class="col-xs-12 visible-xs"></div>
                        <div class="col-sm-3 no-padding-left col-xs-6">
                            <p class="grey">Price</p>
                            <p class="red">£{{ number_format($shoe->cost, 2) }}</p>
                        </div>
                        <div class="col-sm-3 no-padding-left col-xs-6">
                            <p class="grey">Size</p>
                            <p class="red">{{ $shoe->sizes->description }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6 no-padding-left-xs margin-top-1">
                    <div class="border-right col-sm-12 no-padding">
                        <p class="grey">Offers</p>
                        @if ($shoe->offers->count() == 0)
                            <div>No offers made</div>
                        @elseif ($shoe->offers->count() == 1)
                            <div>1 Offer made</div>
                        @else
                            <div>{{ $shoe->offers->count() }} Offers made</div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-4  col-xs-6 no-padding-left-xs margin-top-1">
                    <div class="col-sm-6 no-padding-left col-xs-12">
                        <p class="grey">Date</p>
                        <p>{{ $shoe->created_at->format('d/m/y') }}</p>
                    </div>
                    <div class="col-sm-6 no-padding col-xs-12 margin-bottom-1">
                        <span class="col-sm-12 margin-top-half margin-bottom hidden-xs"></span>
                        <a data-toggle="collapse" data-target="#viewMore{{$shoe->id}}" class="btn sole-btn bordered-grey-btn col-sm-12 pull-right margin-bottom collapse-btn col-xs-12" aria-expanded="false" data-open-text="Hide Offers">View Offers</a>
                    </div>
                </div>
                <div id="viewMore{{$shoe->id}}" class="collapse col-sm-12 no-padding margin-top-1 col-xs-12 margin-bottom-1" aria-expanded="false">
                    <div class=" col-sm-12 no-padding col-xs-12">
                        @forelse ($shoe->offers as $offer)
                            <div class="offer">
                                <div class="col-sm-6 col-xs-6">
                                    <div class="col-sm-12 no-padding col-xs-12">
                                        <div class="col-sm-3 col-sm-offset-9 no-padding">
                                            <p class="grey">User</p>
                                            <a href="{{ url('profile/' . $offer->user->username) }}" class="black offer-column">
                                                <div style="background-image: url('{{ $offer->user->image }}')" class="profile-image profile-image-extra-small pull-left margin-right-half"></div><strong>{{ $offer->user->username }}</strong>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <div class="col-sm-12 no-padding">
                                        <p class="grey">Offer</p>
                                        <p class="red offer-column">£{{ number_format($offer->amount, 2) }}</p>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="col-sm-6 no-padding-left col-xs-6">
                                        <p class="grey">Date</p>
                                        <p class="offer-column">{{ $offer->created_at->format('d/m/y') }}</p>
                                    </div>
                                    <div class="col-sm-6 no-padding col-xs-6">
                                        <a class="btn sole-btn {{ (!$offer->accepted && !$offer->declined) ? 'offer-accept-button' : 'no-click-btn' }} {{ $offer->accepted ? 'green-btn' : 'grey-btn' }} col-sm-12 pull-right margin-bottom col-xs-12" id="offer-accept-button-{{ $offer->id }}" data-offer-id="{{ $offer->id }}">{{ $offer->accepted ? 'Accepted' : 'Accept' }}</a>
                                        <a class="btn sole-btn {{ (!$offer->accepted && !$offer->declined) ? 'offer-decline-button' : 'no-click-btn' }} {{ $offer->declined ? 'offer-button red-btn' : 'grey-btn' }} col-sm-12 pull-right col-xs-12" id="offer-decline-button-{{ $offer->id }}" data-offer-id="{{ $offer->id }}">{{ $offer->declined ? 'Declined' : 'Decline' }}</a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No offers have been made for this shoe yet.</p>
                        @endforelse
                    </div>
                </div>
            </article>
        @empty
            <article class="col-sm-12 bordered-box margin-bottom-1">
                <ul class="nav nav-tabs transactions-nav-tabs col-sm-6 col-md-5 col-xs-12">
                    <li class="active"><a href="{{ url('account/offers/received') }}" class="pull-left">Received Offers</a></li>
                    <li><a href="{{ url('account/offers/sent') }}" class="pull-left">Sent Offers</a></li>
                </ul>
            </article>
            @include('partials.no-results', ['text' => 'Looks like you have no offers', 'sub_text' => 'Why don\'t you <a href="' . url('upload/shoe') .'">upload a shoe</a> right now? '])
        @endforelse
    </div>

    <nav>
        @include('partials.paginator', ['paginator' => $shoes])
    </nav>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(function() {

            var updateOfferButton = function(offerId, accepted) {

                var acceptButton = $('#offer-accept-button-' + offerId);
                acceptButton.addClass(accepted ? 'green-btn' : 'grey-btn');
                acceptButton.removeClass(accepted ? 'grey-btn' : 'green-btn');
                acceptButton.addClass('no-click-btn');

                var declineButton = $('#offer-decline-button-' + offerId);
                declineButton.addClass(accepted ? 'grey-btn' : 'red-btn');
                declineButton.removeClass(accepted ? 'red-btn' : 'grey-btn');
                declineButton.addClass('no-click-btn');

                if (accepted) {
                    acceptButton.text('Accepted');
                } else {
                    declineButton.text('Declined');
                }
            };

            var actionWasTaken = function(offerId) {
                var acceptButton = $('#offer-accept-button-' + offerId);
                var declineButton = $('#offer-decline-button-' + offerId);

                return acceptButton.hasClass('green-btn') || declineButton.hasClass('red-btn');
            };

            $('.offer-accept-button').click(function(e) {

                var offerId = $(this).data('offer-id');
                if (actionWasTaken(offerId)) return;

                $.post('{{ url('account/offers') }}/' + offerId + '/accept', function( data ) {
                    if (data.status) {
                        for (var i=0; i<data.accepted.length; i++) {
                            updateOfferButton(data.accepted[i], true);
                        }
                        for (var i=0; i<data.declined.length; i++) {
                            updateOfferButton(data.declined[i], false);
                        }
                    } else {
                        createStatusMessage('Could not accept offer. Please reload the page and try again.', false)
                    }
                });
            });

            $('.offer-decline-button').click(function(e) {

                var offerId = $(this).data('offer-id');
                if (actionWasTaken(offerId)) return;

                $.post('{{ url('account/offers') }}/' + offerId + '/decline', function( data ) {
                    if (data.status) {
                        updateOfferButton(offerId, false);
                    } else {
                        createStatusMessage('Could not decline offer. Please reload the page and try again.', false)
                    }
                });
            });
        })
    </script>
@endsection