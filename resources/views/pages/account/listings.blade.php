@extends('templates.main')


@section('title', 'My Listings - Account - ')

@section('body_class') class="account_page" @stop

@section('content')

    @include('pages.account.header')

    <div id="col-xs-12">
        @if(isset($listings))
            @forelse ($listings as $shoe)
                <article class="profile-product col-sm-3">
                    <div class="product-box col-sm-12 no-padding"> <img alt="new-releases-product" class="img-responsive center-block product-img" src="{{ $shoe->images->first()->large() }}">
                        @if ($shoe->sold && $shoe->purchase && $shoe->purchase->payment_sent)
                            <div class="box_overlay_sold center-block">
                                <div class="col-sm-12 text-center btn_holder_middle"><br>
                                    <a href="#" class="btn no-click-btn transparent-btn bordered-white-btn col-sm-9 margin-bottom-2 mark_unsold_btn" data-shoe-id="2">Sold {{ $shoe->purchase->created_at->format('d/m/y') }}</a>
                                </div>
                            </div>
                        @elseif ($shoe->marked_as_sold)
                            <div class="box_overlay_sold center-block">
                                <div class="col-sm-12 text-center btn_holder_middle"><br>
                                    <a href="#" class="btn transparent-btn bordered-white-btn col-sm-5 margin-bottom-2 mark_unsold_btn" data-shoe-id="{{ $shoe->id }}">Sold</a>
                                </div>
                            </div>
                        @endif
                        <div class="box_overlay" @if ($shoe->marked_as_sold || $shoe->sold) style="display:none;" @endif>
                            <a href="/shoes/{{$shoe->slug}}" class="shoeLink"></a>
                            <div class="col-sm-12 text-center overlay-buttons-holder no-padding"> <a class="col-sm-4 text-center no-padding edit_btn" title="Edit Shoe"><img src="{{ url('assets/images/edit_icon.png') }}" alt="" class="location-icon"></a><a class="col-sm-4 text-center no-padding mark_sold_btn" title="Mark as sold"><img src="{{ url('assets/images/wanted_icon.png') }}" alt="" class="location-icon"></a><a class="col-sm-4 text-center no-padding delete_btn" title="Delete shoe"><img src="{{ url('assets/images/bin_icon.png') }}" alt="" class="location-icon"></a> </div>
                            <div class="box_overlay_edit center-block" style="display:none;">
                                <div class="col-sm-12 text-center btn_holder_middle">
                                    <p class="text-center white">Would you like to<br>
                                        Edit this item?</p>
                                    <a href="{{ url('shoes/' . $shoe->slug . '/edit') }}" class="btn red-btn col-sm-5 col-sm-offset-1 margin-bottom-2">Edit</a> <a class="btn transparent-btn col-sm-5 margin-bottom-2 cancel_edit white">Cancel</a> </div>
                            </div>
                            <div class="box_overlay_mark_sold center-block" style="display:none;">
                                <div class="col-sm-12 text-center btn_holder_middle">
                                    <p class="text-center white">Would you like to mark<br>
                                        this shoe as sold?</p>
                                    <a href="" class="btn red-btn col-sm-5 col-sm-offset-1 margin-bottom-2 delete_mark_sold" data-shoe-id="{{ $shoe->id }}">Sold</a> <a class="btn transparent-btn col-sm-5 margin-bottom-2 cancel_mark_sold white">Cancel</a> </div>
                            </div>
                            <div class="box_overlay_delete center-block" style="display:none;">
                                <div class="col-sm-12 text-center btn_holder_middle">
                                    <p class="text-center white">Are you sure you want to <br>
                                        Delete this item?</p>
                                    <a href="" class="btn red-btn col-sm-5 col-sm-offset-1 margin-bottom-2 delete_perm" data-shoe-id="{{ $shoe->id }}">Delete</a> <a class="btn transparent-btn col-sm-5 margin-bottom-2 cancel_del white">Cancel</a> </div>
                            </div>
                        </div>
                    </div>
                    <!-- Post Content -->
                    <div class="prod-info">
                        <div class="pricebar cf">
                            <h2>
                                <a href="{{ url('shoes/' . $shoe->slug) }}">
                                    <div class="prod_title_holder">
                                        <span class="prod_title ">{{ $shoe->name }}</span> <span class="price pull-right red">£{{ round($shoe->cost) }}</span>
                                    </div>
                                    <div class="prod_size grey text-left">{{ $shoe->sizes->description }}</div>
                                </a>
                            </h2>
                        </div>
                    </div>
                </article>
            @empty
                @include('partials.no-results', ['text' => 'Looks like you have no listings', 'sub_text' => 'Why don\'t you <a href="' . url('upload/shoe') .'">upload a shoe</a> right now? '])
            @endforelse
    </div>
    <nav>
        @include('partials.paginator', ['paginator' => $listings])
    </nav>
    @else
        @include('partials.no-results', ['text' => 'Looks like you have no listings', 'sub_text' => 'Why don\'t you <a href="' . url('upload/shoe') .'">upload a shoe</a> right now? '])
    @endif

@stop
