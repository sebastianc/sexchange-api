@extends('templates.main')


@section('title', 'My Offers - Account - ')

@section('body_class') class="account_page" @stop

@section('content')
    <div class="white-bg col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 offers-info-page">
        @forelse ($offers as $index => $offer)
            <article class="col-sm-12 bordered-box margin-bottom-1">
                @if ($index == 0)
                    <ul class="nav nav-tabs transactions-nav-tabs col-sm-6 col-md-5 col-xs-12">
                        <li><a href="{{ url('account/offers/received') }}" class="pull-left">Received Offers</a></li>
                        <li class="active"><a href="{{ url('account/offers/sent') }}" class="pull-left">Sent Offers</a></li>
                    </ul>
                @endif
                <div class="col-sm-5 no-padding-left col-xs-12 no-padding-xs margin-top-1 clear">
                    <div class="border-right col-sm-12 no-padding col-xs-12">
                        <div class="col-sm-3 col-xs-2 no-padding"> <img src="{{ $offer->shoe->images->first()->small() }}" class="img-responsive pull-left"/> </div>
                        <div class="col-sm-5 col-xs-10">
                            <p class="grey hidden-xs">Shoe</p>
                            <p><a href="{{ url('shoes/' . $offer->shoe->slug) }}" class="black">{{ $offer->shoe->name }}</a></p>
                        </div>
                        <div class="col-xs-12 visible-xs"></div>
                        <div class="col-sm-4 no-padding-left col-xs-6">
                            <p class="grey">Offer Amount</p>
                            <p class="red">£{{ number_format($offer->amount, 2) }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 no-padding-left-xs margin-top-1">
                    <div class="border-right col-sm-12 no-padding">
                        <p class="grey">Status</p>
                        @if ($offer->declined)
                            <div>Seller declined</div>
                        @elseif ($offer->accepted)
                            <div>Seller accepted</div>
                        @else
                            <div>Awaiting response</div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-4  col-xs-6 no-padding-left-xs margin-top-1">
                    <div class="col-sm-6 no-padding-left col-xs-12">
                        <p class="grey">Date</p>
                        <p>{{ $offer->created_at->format('d/m/y') }}</p>
                    </div>
                    <div class="col-sm-6 no-padding col-xs-12 margin-bottom-1">
                        <a class="btn sole-btn {{ $offer->accepted ? 'green-btn' : 'grey-btn no-click-btn' }} col-sm-12 pull-right margin-bottom" href="{{ url('shoes/' . $offer->shoe->slug) }}">Accepted</a>
                        <a class="no-click-btn btn sole-btn {{ $offer->declined ? 'red-btn' : 'grey-btn' }} col-sm-12 pull-right margin-bottom">Declined</a>
                    </div>
                </div>
            </article>
        @empty
            <article class="col-sm-12 bordered-box margin-bottom-1">
                <ul class="nav nav-tabs transactions-nav-tabs col-sm-6 col-md-5 col-xs-12">
                    <li><a href="{{ url('account/offers/received') }}" class="pull-left">Received Offers</a></li>
                    <li class="active"><a href="{{ url('account/offers/sent') }}" class="pull-left">Sent Offers</a></li>
                </ul>
            </article>
            @include('partials.no-results', ['text' => 'Looks like you\'ve not made any offers'])
        @endforelse
    </div>

    <nav>
        @include('partials.paginator', ['paginator' => $offers])
    </nav>
@endsection