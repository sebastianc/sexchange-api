@extends('templates.main')

@section('title', 'Sold Shoes - Account - ')

@section('content')
    @include('partials.review-seller')
    @include('partials.dispute')
    <div class="white-bg col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 offers-info-page no-padding-xs">
        @forelse ($shoes as $index => $shoe)
            <article class="col-sm-12 col-xs-12 bordered-box margin-bottom-1">
                @if ($index == 0)
                    <ul class="nav nav-tabs transactions-nav-tabs col-xs-12 margin-bottom-2">
                        <li><a href="{{ url('account/transactions/purchases') }}" class="pull-left">Purchased Shoes</a></li>
                        <li class="active"><a href="{{ url('account/transactions/sold') }}" class="pull-left">Sold Shoes</a></li>
                    </ul>
                @endif
                <div class="col-sm-5 col-xs-12 no-padding-xs offers-info no-padding-left">
                    <div class="border-right col-sm-12 no-padding">
                        <div class="col-sm-3 col-xs-2 no-padding"> <img src="{{ $shoe->images->first()->large() }}" class="img-responsive pull-left"/> </div>
                        <div class="col-sm-9 col-xs-10 no-padding">
                            <div class="col-sm-6 col-xs-12">
                                <p class="grey hidden-xs">Shoe</p>
                                <p><a href="{{ url('shoes/' . $shoe->slug) }}" class="black">{{ $shoe->name }}</a></p>
                            </div>
                            <div class="col-sm-6 col-xs-4 no-padding-left">
                                <div class="transactions-top-row">
                                    <p class="grey hidden-xs">Price</p>
                                    <p class="red">£{{ number_format($shoe->purchase->price, 2) }}</p>
                                </div>
                                <div>
                                    <p class="grey hidden-xs">Size</p>
                                    <p class="red">{{ $shoe->sizes->description }}</p>
                                </div>
                            </div>
                            <div class="col-xs-12 visible-xs">
                                <p>{{ $shoe->purchase->created_at->format('d/m/y') }}</p>
                            </div>
                            <a class="visible-xs pull-right collapse-btn"  aria-expanded="false" data-toggle="collapse" data-target="#info1"><img src="{{ url('assets/images/arrow_down.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
                <div id="info1" class="col-sm-7 col-xs-12 info-collapse">
                    <div class="col-sm-5 col-xs-4">
                        <div class="border-right col-sm-12 no-padding">
                            <div class="transactions-top-row">
                                <p class="grey">Bought By</p>
                                <span>
                                    <a href="{{ url('/profile/' . $shoe->purchase->user->username) }}">
                                        <div style="background-image: url('{{ $shoe->purchase->user->image }}')" class="pull-left img-circle img-circle-small img-responsive profile-img"></div>
                                    </a>
                                    <a href="{{ url('/profile/' . $shoe->purchase->user->username) }}">{{ $shoe->purchase->user->username }}</a>
                                </span>
                                <p class="red">£{{ number_format($shoe->purchase->price, 2) }}</p>
                            </div>
                            <div>
                                <p class="grey">Shipping Address</p>
                                <div>{{ $shoe->purchase->formatted_shipping_address }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-xs-8 no-padding">
                        <div class="col-sm-5 no-padding-left hidden-xs">
                            <p class="grey">Date and Time</p>
                            <p>
                                {{ $shoe->purchase->created_at->format('d/m/y') }}<br>
                                {{ $shoe->purchase->created_at->format('H:i') }}
                            </p>
                        </div>
                        <div class="col-sm-7 no-padding">
                            @if ($shoe->reviewBySeller)
                                <div class="review-link btn sole-btn green-btn col-sm-12 pull-right margin-bottom no-click-btn">Reviewed</div>
                            @else
                                <a id="review-link" class="review-link btn sole-btn grey-btn col-sm-12 pull-right margin-bottom pop-up-link"
                                   data-shoe-id="{{ $shoe->id }}" data-username="{{ $shoe->purchase->user->username }}" data-image="{{ $shoe->purchase->user->image }}">Review</a>
                            @endif
                            <a href=""
                               class="dispatch-shoe-button btn sole-btn {{ $shoe->purchase->status == 'dispatched' ? 'green-btn' : 'grey-btn' }} col-sm-12 margin-bottom pull-right"
                               data-shoe-id="{{ $shoe->id }}">{{ $shoe->purchase->status == 'dispatched' ? 'Dispatched' : 'Mark as Dispatched' }}</a>
                            @if ($shoe->purchase->dispute)
                                <a href="" class="no-click-btn btn sole-btn green-btn col-sm-12 pull-right" data-shoe-id="{{ $shoe->id }}">{{ $shoe->purchase->dispute->disputer }} Disputed</a>
                            @else
                                <a href="" class="dispute-link btn sole-btn grey-btn col-sm-12 pull-right" data-shoe-id="{{ $shoe->id }}">Make A Dispute</a>
                            @endif
                        </div>
                    </div>
                </div>
            </article>
        @empty
            <article class="col-sm-12 bordered-box margin-bottom-1">
                <ul class="nav nav-tabs transactions-nav-tabs col-sm-6 col-md-5 col-xs-12">
                    <li><a href="{{ url('account/transactions/purchases') }}" class="pull-left">Purchased Shoes</a></li>
                    <li class="active"><a href="{{ url('account/transactions/sold') }}" class="pull-left">Sold Shoes</a></li>
                </ul>
            </article>
            @include('partials.no-results', ['text' => 'Looks like you\'ve not sold any shoes yet.', 'sub_text' => 'Why don\'t you  <a href="' . url('upload/shoe') . '">upload a shoe</a>?'])
        @endforelse
        <nav>
            @include('partials.paginator', ['paginator' => $shoes])
        </nav>
    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(function() {
            $('#review-form').submit(function(e) {
                if ($('input[name=rating]').val() == 0) {
                    createStatusMessage('Please select a rating.', false);
                    e.preventDefault();
                }
                else if ($('#review-text').val().length < 10) {
                    createStatusMessage('Please enter a review that is over 10 characters in length.', false);
                    e.preventDefault();
                }

            });

           $('.dispatch-shoe-button').click(function(e) {
               e.preventDefault();

               var update = function(element, selected) {
                   if (!selected) {
                       $(element).removeClass('green-btn');
                       $(element).addClass('grey-btn');
                       $(element).text('Mark as Dispatched');
                   } else {
                       $(element).removeClass('grey-btn');
                       $(element).addClass('green-btn');
                       $(element).text('Dispatched');
                   }
               };
               var shoeId = $(this).data('shoe-id');

               update(this, !$(this).hasClass('green-btn'));

               $.post(BASE_URL + '/account/transactions/' + shoeId + '/dispatch', function(data){
                   update(this, data.shoe_status == 'dispatched');
               });
           });
        });

    </script>
@endsection