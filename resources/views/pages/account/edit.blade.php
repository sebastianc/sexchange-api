@extends('templates.main')

@section('title', 'Edit Account - ')

@section('content')

    <div class="col-sm-12 col-xs-12 col-md-8 col-sm-offset-0 col-md-offset-2 bordered-box edit-profile-page">
        <div class="col-sm-12 col-xs-12 no-padding margin-bottom-1">
            <div class="col-xs-2 col-sm-3 col-md-2 no-padding profile-img-holder">
                <div id="profile_img_upload"  @if ($user->image != '') style="background-image: url('{{ old('image', $user->image) }}')" @endif>
                    <div id="profile_img_icon"></div>
                </div>
                <form id="profile_img_form" action="{{ url('account/edit/image') }}" method="post"><input type="file" name="file" id="profile_img_hidden"></form>
            </div>
            <div class="pull-left col-xs-10 col-sm-9 margin-bottom-1">
                <h2>Edit Profile</h2>
                <div class="col-sm-12 col-xs-12 no-padding margin-bottom-1">
                    <div class="pull-left grey">{{ $user->username }}</div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 border-bottom  margin-bottom-1" ></div>
        <div class="col-sm-12 col-xs-12 no-padding ">
            <h4 class="grey">Personal</h4>
            <form action="{{ url('account/edit') }}" method="post" id="edit-profile-form">
                {{ csrf_field() }}
                <input type="hidden" name="image" id="hidden_image_input" value="{{ old('image', $user->image) }}">
                <div class="form-group">
                    <label for="" class="col-sm-12 col-xs-12 no-padding grey">Your Bio</label>
                    <textarea name="bio" class="edit-profile-bio" class="form-control cl-form-control" rows="5" placeholder="Type here">{{ old('bio', $user->bio) }}</textarea>
                </div>
                <div class="col-sm-6 col-xs-12 no-padding">
                    <div class="form-group">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Town/City</label>
                        <input type="text" class="form-control cl-form-control" value="{{ old('city', $user->city) }}" name="city" placeholder="Type here" />
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">County</label>
                        <input type="text" class="form-control cl-form-control" value="{{ old('county', $user->county) }}" name="county" placeholder="Type here" />
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-12 col-xs-12 no-padding grey">Phone Number</label>
                        <input type="tel" class="form-control cl-form-control" value="{{ old('phone', $user->phone) }}" name="phone" placeholder="Type here" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="white grey-btn col-sm-12 col-xs-12 btn sole-btn input-btn" value="Save Changes">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('footer_scripts')
    <script>
        $(function() {

            var unsavedChanges = false;

            $(window).bind('beforeunload', function(){
                if (unsavedChanges) {
                    return 'You have not saved your profile.';
                }
            });


            $('input[name=city], input[name=phone], input[name=county], textarea[name=bio]').keyup(function(e) {
                unsavedChanges = true;
            });

            $('#edit-profile-form').submit(function() {
                unsavedChanges = false;
            });

            $('#profile_img_upload').click(function(e) {
                $('#profile_img_hidden').click();
            });

            $('#profile_img_hidden').change(function(e) {
                $('#profile_img_form').submit();
            });

            $('#profile_img_form').submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $('#profile_img_icon').addClass('background_img_icon_loading');

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success:function(data){
                        $('#hidden_image_input').val(data.url);
                        $('#profile_img_upload').css('background-image', 'url(' + data.url + ')');
                        $('#profile_img_icon').removeClass('background_img_icon_loading');
                    },
                    error: function(data){
                        alert('Error updating image, please try again.');
                        $('#profile_img_icon').removeClass('background_img_icon_loading');
                    }
                });
            })
        });
    </script>
@endsection