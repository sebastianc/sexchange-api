@extends('templates.main')


@section('title', 'My Likes - Account - ')

@section('body_class') class="account_page" @stop

@section('content')

    @include('pages.account.header')

    <div id="col-xs-12">
        @forelse ($likedShoes as $shoe)
            <article class="profile-product col-sm-3">
                <!-- Post Content -->
                <a class="product-box col-sm-12 no-padding" href="{{ url('shoes/' . $shoe->slug) }}">
                    <img alt="new-releases-product" class="img-responsive center-block product-img" src="{{ $shoe->images->first()->large() }}">
                </a>
                <div class="prod-info">
                    <div class="pricebar cf">
                        <h2>
                            <a href="{{ url('shoes/' . $shoe->slug) }}">
                                <div class="prod_title_holder">
                                    <span class="prod_title ">{{ $shoe->name }}</span> <span class="price pull-right red">£{{ round($shoe->cost) }}</span>
                                </div>
                                <div class="prod_size grey text-left">{{ $shoe->sizes->description }}</div>
                            </a>
                        </h2>
                    </div>
                </div>
            </article>
        @empty
            @include('partials.no-results', ['text' => 'Looks like you\'ve not liked any shoes yet', 'sub_text' => 'Why don\'t you  <a href="/">check out whats on offer</a>? '])

        @endforelse
    </div>
    <nav>
        @include('partials.paginator', ['paginator' => $likedShoes])
    </nav>
@stop
