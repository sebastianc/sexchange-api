<div class="col-xs-12 no-padding margin-bottom-1 margin-top-1 visible-xs border-bottom"> <a href="{{ url('account/edit') }}" class="btn sole-btn bordered-grey-btn pull-left edit-profile">Edit Profile</a>
    <select id="dynamic_select" dir="rtl" class="filter-sort filter-btn turnintodropdown pull-right">
        <option data-toggle="tab" value="listings" >My Listings</option>
        <option data-toggle="tab" value="likes" >My Likes</option>
        <option data-toggle="tab" value="wanted" >My Wanted</option>
    </select>
</div>
<div class="col-sm-6 col-md-7 col-xs-12 no-padding-xs profile-info">
    <div class="col-xs-2 col-sm-3 col-md-2 no-padding"> <div style="background: url('{{ url($user->thumb()) }}')" class="profile-image profile-image-large"></div></div>
    <div class="pull-left col-xs-10 col-sm-9">
        <h2 class="no-margin-top">{{ $user->username }}</h2>
        <div class="col-sm-12 col-xs-12 no-padding margin-bottom-1">
            <div class="pull-left margin-right-1"><a href="{{ url('profile/'. $user->username .'/followers') }}"><span class="black">{{ $user->follower_count }}</span> <span class="grey">Follower{{$user->follower_count != 1 ? 's' : '' }}</span></a></div>
            <div class="pull-left"><a href="{{ url('profile/'. $user->username .'/following') }}"><span class="black">{{ $user->follow_count }}</span> <span class="grey">Following</span></a></div>
        </div>
        <div class="col-sm-12 col-xs-12 no-padding margin-bottom-1">
            <div class="pull-left margin-right-1 grey"><img src="{{ url('assets/images/location_icon.png') }}" alt="location icon" class="pull-left location-icon"/>{{ $user->location }}</div>
            <div class="pull-left">
                <a href="{{ url('/profile/' . $user->username . '/reviews') }}"><input value="{{ $user->average_rating }}" type="hidden" class="rating" data-size="s" disabled></a>
            </div>
        </div>
        <div class="col-sm-12 no-padding margin-bottom-1 hidden-xs"> <a href="{{ url('account/edit') }}" class="btn sole-btn bordered-grey-btn pull-left">Edit Profile</a> </div>
    </div>
    @if ($user->bio)
        <p class="col-md-9 col-md-offset-2 col-sm-9 col-sm-offset-3 col-xs-12 col-xs-offset-0 no-padding-xs">{{ $user->bio }}</p>
    @endif
</div>
<ul class="nav nav-tabs profile-nav-tabs col-sm-6 col-md-5 col-xs-12 pull-right hidden-xs">
    <li class="{{ $type == 'wanted' ? 'active ' : '' }}pull-right"><a href="{{ url('account/wanted') }}" class="pull-left">My Wanted</a></li>
    <li class="{{ $type == 'likes' ? 'active ' : '' }}pull-right"><a href="{{ url('account/likes') }}" class="pull-left">My Likes</a></li>
    <li class="{{ $type == 'listings' ? 'active ' : '' }}pull-right"><a href="{{ url('account/listings') }}" class="pull-left">My Listings</a></li>
</ul>
<div class="col-sm-12 hidden-xs border-bottom margin-bottom-1 margin-top-1"></div>