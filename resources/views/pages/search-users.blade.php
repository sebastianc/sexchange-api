@extends('templates.main')

@section('title') Explore - @stop

@section('body_class') class="explore_page" @stop

@section('content')
    <ul id="breadcrumbs" class="breadcrumbs col-xs-12 hidden-xs">
        <li class="item-home"><a class="bread-link bread-home" href="/" title="Homepage">Home</a></li>
        <li class="item-current item-7537"><span class="bread-current bread-7537"> Search results for {{session('term')}}</span></li>
    </ul>
    <div id="tabs">
        <ul class="nav nav-tabs pull-left">
            <li class="active pull-right"><a href="" class="pull-left">Users</a></li>
            <li class=" pull-right"><a href="{{$shoe_url}}" class="pull-left">Shoes</a></li>
        </ul>
        <div class="tab-content col-sm-12 no-padding">
            <div id="buy" class="tab-pane fade in active grid">
                <div id="search-results-gallery" class="col-sm-12 no-padding">
                    @if($users->count())
                    @foreach($users as $user)
                        @include('partials.search-user')
                    @endforeach
                    @else
                        @include('partials.no-results-users')
                    @endif
                    <nav>
                        @include('partials.paginator', ['paginator' => $users])
                    </nav>
                </div>
            </div>

        </div>
    </div>
@stop

