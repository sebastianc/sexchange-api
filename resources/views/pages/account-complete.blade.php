{{--@section('popover')--}}
    <div class="username_box_overlay">
    </div>
    <div class="username_pop pop-up">
        <div class="col-xs-12 white-bg tlva">
            <div class="col-sm-12 text-center">
                <img src="{{asset('assets/images/SOLE.png')}}"/>
            </div>
            <h3 class="col-xs-12 text-center text-capitalize post-title dark-grey margin-bottom-1 ">Please choose a username</h3>
            @if(session('error'))
                <div class="col-xs-12 margin-bottom-1 red text-center">
                    Oops! {{session('error')}}
                </div>

            @endif

            <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin_bottom_1">
                <div class="form_wrap">
                    <form name="complete" id="complete" action="/account/complete" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group">
                            <input type="text" name="username" id="" class="form-control cl-form-control" value="" size="20" placeholder="Username" />
                        </div>
                        <input type="submit" name="submit" id=""  class="btn sole-btn red-btn col-xs-12" value="Save" />

                    </form>
                </div>
            </div>
        </div>
    </div>
{{--@stop--}}
{{--TODO: ADD Accont page in background--}}
@include('pages.account.listings', ['type' => 'listings'])

