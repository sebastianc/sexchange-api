<div class="col-xs-12 white-bg">
    <div class="col-sm-12 text-center">
        <img src="{{asset('assets/images/SOLE.png')}}"/>
    </div>
    <h3 class="col-xs-12 text-center text-capitalize post-title dark-grey margin-bottom-1 ">Log In</h3>
    @if(session('error'))
        <div class="col-xs-12 margin-bottom-1 red text-center">
            Oops! {{session('error')}}
            @if(session('attempts_remaining') == '1')
                <br/>
                You have {{session('attempts_remaining')}} login attempt remaining
            @endif
        </div>

    @endif

    <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin_bottom_1">
        <div class="form_wrap">
            <form name="login" id="login" action="/login" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="form-group">
                    <input type="text" name="email" id="" class="form-control cl-form-control" value="" size="20" placeholder="Email Address" />
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="" class="form-control cl-form-control" value="" size="20" placeholder="Password" />
                </div>
                <input type="submit" name="submit" id=""  class="btn sole-btn red-btn col-xs-12" value="Login" />

            </form>
        </div>
    </div>
    <div class=" col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3">
        <span class="col-xs-12 text-center margin-bottom-1 margin-top-1">or</span>
        <div class="form_wrap">
            <a class="btn sole-btn blue-btn col-xs-12 fb-register input-btn margin-bottom-1" href="/login/facebook" onclick="" ><img src="{{asset('assets/images/facebook.png')}}">Login with Facebook</a><div class="height10"></div>
            <a class="col-xs-12 text-center no-padding" href="">Need help signing in? <b>Forgot Password</b></a>
        </div>
    </div>
</div>