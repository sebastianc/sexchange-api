
<div id="filter_option" class="col-sm-3 col-md-2 col-xs-12 margin-top-1 no-padding">
    <input type="hidden" id="per_page" value="20">
        <div class="col-xs-12 no-padding">
            @if (isset($show_type_filter))
                <div class="filter-option col-sm-12 col-xs-6">
                    <h4 id="wantedh4" class="toggleul ">Search</h4>
                    <ul id="wantedul">
                        <li>
                            <input type="checkbox" class="filter" id="typebuy" value="1" @if(isset($_GET['buyType']) && $_GET['buyType']=='true') checked @endif>
                            <label for="typebuy">Buy Shoes</label>
                        </li>
                        <li>
                            <input type="checkbox" class="filter" id="typewanted" value="1" @if(isset($_GET['wantedType']) && $_GET['wantedType']=='true') checked @endif>
                            <label for="typewanted">Wanted Shoes</label>
                        </li>
                    </ul>
                </div>
            @endif
            <div class="filter-option col-sm-12 col-xs-6">
                <h4 id="sizeh4" class="toggleul ">Size</h4>
                <ul id="sizeul">
                    @foreach($filters['sizes'] as $size)
                        <li>
                            <input type="checkbox" class="filter" id="{{$size->gender.$size->size}}" value="{{$size->id}}" @if(isset($_GET['size']) && in_array($size->id, explode(',', $_GET['size']))) checked @endif>
                            <label for="{{$size->gender.$size->size}}">{{$size->gender.' '.$size->size}} </label>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="filter-option col-sm-12 col-xs-6">
                <h4 id="priceh4" class="toggleul ">Price</h4>
                <ul id="priceul">

                    @foreach($filters['prices'] as $price)
                        <li>
                            <input type="checkbox" class="filter" id="{{$price[0].'_'.$price[1]}}" name="price" value="{{$price[0].'_'.$price[1]}}"  @if(isset($_GET['price']) && in_array($price[0].'_'.$price[1], explode(',', $_GET['price']))) checked @endif>
                            <label for="{{$price[0].'_'.$price[1]}}">@if(is_int($price[1])){{'£'.$price[0].'-£'.$price[1]}}@else {{'£'.$price[0].'+'}} @endif</label>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="col-xs-12 no-padding">
            <div class="filter-option col-sm-12 col-xs-6">
                <h4 id="brandh4" class="toggleul ">Brands</h4>
                <ul id="brandul">
                    @foreach($filters['brands'] as $brand)
                        <li>
                            <input type="checkbox" class="filter" id="{{$brand->slug}}" value="{{$brand->id}}" @if(isset($_GET['brand']) && in_array($brand->id, explode(',', $_GET['brand']))) checked @endif>
                            <label for="{{$brand->slug}}">{{$brand->name}}</label>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="filter-option colours col-sm-12 col-xs-6">
                <h4 id="colourh4" class="toggleul">Colour</h4>
                <ul id="colourul">
                    @foreach($filters['colours'] as $colour)
                        <li>
                            <input type="checkbox" id="checkbox-{{$colour->name}}" class="filter regular-checkbox {{$colour->name}}" value="{{$colour->id}}" @if(isset($_GET['colours']) && in_array($colour->id, explode(',', $_GET['colours']))) checked @endif>
                            <label class="" for="checkbox-{{$colour->name}}">
                                <span class="colour-box {{$colour->name}}" style="background-color: {{$colour->name}}"></span>
                                <span class="text-capitalize pull-left">{{$colour->name}}</span>
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div></div>
        <div class="col-xs-12 no-padding">
            <div class="filter-option colours col-sm-12 col-xs-6">
                <h4 id="conditionh4" class="toggleul">Condition</h4>
                <div id="conditiondiv" class="col-sm-12 no-padding">
                    <input type="number" id="condition" class="rating filter rating-loading" @if(isset($_GET['condition'])) value="{{$_GET['condition']}}" @else value="0" @endif data-size="xs" data-class="filter" min="0" max="5" step="1" title="">
                </div>
            </div>
        </div>
        <a href="javascript:void(0);" class="reset_btn sole-btn bordered-grey-btn col-xs-12 text-center hidden-xs">Reset Search</a>
        <div class="clear"></div>
    </div>


@section('footer_scripts')
    <script>

        function ajax_filter(){
            var brand = [];
            var colours = [];
            var price = [];
            var size = [];

            $('#brandul .filter:checked').each(function() {
                brand.push($(this).val());
            });

            $('#colourul .filter:checked').each(function() {
                colours.push($(this).val());
            });

            $('#priceul .filter:checked').each(function() {
                price.push($(this).val());
            });

            $('#sizeul .filter:checked').each(function() {
                size.push($(this).val());
            });

            /*$('.listing-view').css('visibility','hidden');*/
            $('.ajax_loader').fadeIn();
            $('.filter').attr('disabled',true);
            $('.loader').css('display','block');
            $('.filter:checked').each(function(){
                allData = false;
            });

            if($('#condition').val()) {
                var condition = $('#condition').val();
            }

            @if (isset($show_type_filter))
                var buyChecked = $('#typebuy').is(":checked");
                var wantedChecked = $('#typewanted').is(":checked");
            @elseif (Request::is('wanted'))
                var buyChecked = false;
                var wantedChecked = true;
            @else
                var buyChecked = true;
                var wantedChecked = false;
            @endif

            var data = {
                _token:'{{csrf_token()}}',
                term:'{{session('term')}}',
                brand: brand,
                buyType: buyChecked,
                wantedType: wantedChecked,
                colours:colours,
                price:price,
                size:size,
                condition:condition,
                sortBy:$('.filter-sort').val(),
                per_page:$('#per_page').val(),
            };
            $.post(
                    "/search/filter",
                    data,
                    function(response) {
                        //console.log(response);
                        $('.filter').attr('disabled',false);
                        $('#search-results-gallery').html(response);


                        //to change the browser URL to 'pageurl'
                        var pageurl = $('#ajax_pagination').data('url');
                        if(pageurl!=window.location){
                            window.history.pushState({path:pageurl},'',pageurl);
                        }
                        $('.ajax_loader').fadeOut();
                    }
            );
        }

        $('.filter').on('click',function(){
            ajax_filter();
        });
        $('#condition').on('rating.change', function(event, value, caption) {
            ajax_filter();
        });

        $('.reset_btn').on('click', function(){
            $('#filter_option input:checkbox:checked').prop( "checked", false );
            $('#condition').rating('update', 0);
            ajax_filter();
        });

        $(document).on("change", ".filter-sort", function () {
            ajax_filter();
        });

        $('.load_more ').click(function(){
            $('.load_more ').removeClass('selected')
            $(this).addClass('selected')
            $('#per_page').val($(this).data('amount'))
            ajax_filter();
        });

    </script>
@stop
