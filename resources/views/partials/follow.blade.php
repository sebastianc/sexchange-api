@if (Auth::check() && Auth::user()->id != $user->id)
    <a
        data-user-id="{{ $user->id }}"
        class="follow_user_button col-sm-12 btn sole-btn pull-left margin-top-1 {{ $user->does_follow ? 'red-btn' : 'bordered-red-btn' }}"
    >{{ $user->does_follow ? 'Following' : 'Follow' }}</a>
@endif