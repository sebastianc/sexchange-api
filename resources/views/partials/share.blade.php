{{--TODO: Get share counts--}}
<div class="socail_share_wrap">
    <a href="javascript:share('https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}')">
        <div class="share_btn fb_share">
            <div class="icon_wrap">
                <i class="fa fa-facebook-official" aria-hidden="true"></i>
            </div>
            <div class="share_text">
                Facebook
            </div>
            <div class="share_count">
                {{$shoe->facebook_shares}}
            </div>
        </div>
    </a>
    <a href="javascript:share('https://twitter.com/home?status=Check%20out%20this%20shoe%20on%20{{Request::url()}}')" >
        <div class="share_btn twit_share">
            <div class="icon_wrap">
                <i class="fa fa-twitter" aria-hidden="true"></i>
            </div>
            <div class="share_text">
                Twitter
            </div>
            <div class="share_count">
                {{$shoe->twitter_shares}}
            </div>
        </div>
    </a>
    <a href="javascript:share('https://plus.google.com/share?url={{Request::url()}}')">
        <div class="share_btn gplus_share">
            <div class="icon_wrap">
                <i class="fa fa-google-plus" aria-hidden="true"></i>
            </div>
            <div class="share_text">
                Google+
            </div>
            <div class="share_count">
                {{$shoe->gplus_shares}}
            </div>
        </div>
    </a>
</div>