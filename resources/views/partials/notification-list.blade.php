@foreach ($notifications as $notification)
    <article class="notification-container col-sm-12 col-xs-12" data-notification-type="{{ $notification->notification_type }}">
        <a class="delete-notification" data-notification-id="{{ $notification->id }}"><img src="{{ url('assets/images/delete_notification.png') }}" alt="delete"></a>
        <div class="border-bottom col-sm-12 col-xs-12">
            <div class="col-sm-2 col-xs-3">
                <div style="background: url('{{ $notification->message['image'] }}')" class="{{ $notification->imageClass }} pull-left"></div>
            </div>
            <div class="col-sm-10 col-xs-9 no-padding-left">
                <p>{!! $notification->message['message'] !!}</p>
                <p class="grey">
                    <img src="{{ $notification->icon }}" alt="notification" class="pull-left notification-icon"> {{ $notification->time_ago }}
                </p>
            </div>
        </div>
    </article>
@endforeach