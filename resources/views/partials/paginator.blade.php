<?php

/*
 *----------------------------------------------------------------------------
 * Custom pagination template based on the default laravel/bootstrap version
 *-----------------------------------------------------------------------------
 */

$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)

?>

@if ($paginator->lastPage() > 1)
    <ul class="pagination">
        <li class="">
            Page {{$paginator->currentPage()}} of {{$paginator->lastPage()}}
        </li>
        @if($paginator->currentPage() !== 1)
            <li>
                <a href="{{ $paginator->previousPageUrl() }}">Previous</a>
            </li>
        @endif
        <li class="{{ ($paginator->currentPage() == 1) ? ' active' : '' }}">
            <a href="{{ $paginator->url(1) }}">1</a>
        </li>



        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php

            $left = $paginator->currentPage() -2;
            $right = $paginator->currentPage() + 2;

            ?>
            @if ($i >= $left && $i <= $right && $i != 1 && $i != $paginator->lastPage())
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            {{--@endif--}}
            @elseif($i == $left - 1 || $i == $right + 1)
                <li>...</li>
            @endif
        @endfor

        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' active' : '' }}">
            <a href="{{ $paginator->url($paginator->lastPage())}}">{{$paginator->lastPage()}}</a>
        </li>
        @if($paginator->currentPage() !== $paginator->lastPage())
            <li>
                <a href="{{ $paginator->nextPageUrl() }}">Next</a>
            </li>
        @endif
        @if($paginator->currentPage() !== $paginator->lastPage())
            <li >
                <a href="{{ $paginator->url($paginator->lastPage()) }}">Last</a>
            </li>
        @endif
    </ul>
@endif