<div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 bordered-box">
    <form @if(isset($shoe)) action="/shoes/{{$shoe->slug}}/edit" @else action="/upload/shoe" @endif method="POST" id="upload_form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="wanted" value="{{$wanted}}">
        @if(!$wanted && isset($wanted_id) && $wanted_id)
            <input type="hidden" name="wanted_id" value="{{$wanted_id}}">
        @endif
        @if(isset($shoe))
            <input type="hidden" name="id" value="{{$shoe->id}}">
        @endif
        <div class="col-sm-6 col-xs-12 no-padding ">
            <div class="form-group">
                <label for="" class="col-sm-12 col-xs-12 no-padding grey">Brand</label>
                <select name="brand" class="form-control cl-form-control select-control" required>
                    <option class="grey" value="" disabled selected>Choose a brand</option>
                    @foreach($filters['brands'] as $brand)
                        <option value="{{$brand->id}}" @if(isset($shoe) && $shoe->brand == $brand->id) selected @elseif(old('brand') == $brand->id) selected @endif>{{$brand->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-12 col-xs-12 no-padding grey">Shoe Name</label>
                <input type="text" name="name" class="form-control cl-form-control" value="@if(isset($shoe)){{$shoe->name}}@else{{ old('name') }}@endif" placeholder="Type here" required/>
            </div>
            @if(!$wanted)
                <div class="form-group">
                    <label for="" class="col-sm-12 col-xs-12 no-padding grey">Cost (Including P&amp;P)</label>
                    <div class="input-symbol"> <span class="grey">&pound;</span>
                        <input type="number"  step="any" name="cost" class="form-control cl-form-control" value="@if(isset($shoe)){{$shoe->cost}}@else{{ old('cost') }}@endif" placeholder="Type here" required/>
                    </div>
                </div>
            @else
                <div class="form-group col-sm-12 col-xs-12  no-padding">
                    <label for="" class="grey">Colour</label>
                    <input type="hidden" name="colours" id="selected_colours" required/>
                    <input id="colour-link" type="text" class="form-control cl-form-control pop-up-link" placeholder="Add Colour +" @if(isset($color_string)) value="{{$color_string}}" @endif readonly/>
                </div>
            @endif
            <div class="form-group">
                <label for="" class="col-sm-12 col-xs-12 no-padding grey">Size</label>
                <select class="form-control cl-form-control select-control" name="size" required>
                    <option class="grey" value="" disabled selected>Add Size</option>
                    @foreach($filters['sizes'] as $size)
                        <option value="{{$size->id}}" @if(isset($shoe) && $shoe->size == $size->id) selected @elseif(old('brand') == $size->id) selected @endif >{{$size->description}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-xs-12 col-sm-6 no-padding profile-img-holder">
            <label for="" class="col-sm-12 col-xs-12 grey no-padding-xs">Add Image</label>
            <div class="col-sm-12 col-xs-12 no-padding-xs">
                <div class="shoe_image main_image_upload" id="image_1_preview" @if(isset($shoe)) style="background-image: url({{$shoe->images->first()->large()}})" @endif>
                    <p class="upload_text tlva @if(isset($shoe)) hidden @endif " id="image_1_preview" >Main Image</p>
                    <input type="file" name="image_1" id="image_1" class="shoe_image_input" data-image="1"  @if(isset($shoe)) value="{{$shoe->getImage(1)->src()}}" @else required @endif >
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 no-padding-xs">

                @for($i = 2; $i <= 5; $i++)
                    <div class="shoe_image" id="image_{{$i}}_preview" @if(isset($shoe) && $shoe->getImage($i)) style="background-image: url({{$shoe->getImage($i)->small()}})" @endif>
                        <p class="upload_text tlva @if(isset($shoe) && $shoe->getImage($i)) hidden @endif">+</p>
                        <input type="file" name="image_{{$i}}" id="image_{{$i}}" class="shoe_image_input" data-image="{{$i}}" @if(isset($shoe) && $shoe->getImage($i)) value="{{$shoe->getImage($i)->src()}}" @endif >
                    </div>
                @endfor

            </div>
        </div>
        <div class="col-sm-12 col-xs-12 no-padding ">
            <div class="col-sm-12 col-xs-12 no-padding form-group">
                <label for="" class="grey">Description</label>
                <textarea type="text" name="description" class="form-control cl-form-control" rows="5" placeholder="Type here" required>@if(isset($shoe)){{$shoe->description}}@else{{ old('description') }}@endif</textarea>
            </div>
            @if(!$wanted)
            <div class="form-group col-sm-6 col-xs-12  no-padding">
                <label for="" class="grey">Colour</label>
                <input type="hidden" name="colours" id="selected_colours" required/>
                <input id="colour-link" type="text" class="form-control cl-form-control pop-up-link" placeholder="Add Colour +" @if(isset($color_string)) value="{{$color_string}}" @endif readonly/>
            </div>
            <div class="col-sm-6 col-xs-12 form-group no-padding-xs">
                <label for="" class="grey">Delivery Time</label>
                <select name="delivery" class="form-control cl-form-control select-control" required>
                    <option class="grey"  value="" disabled selected>Choose</option>
                    <option @if(isset($shoe) && $shoe->delivery == '1 - 3 days') selected @elseif(old('delivery') == '1 - 3 days') selected @endif >1 - 3 days</option>
                    <option @if(isset($shoe) && $shoe->delivery == '3 - 5 days') selected @elseif(old('delivery') == '3 - 5 days') selected @endif>3 - 5 days</option>
                    <option @if(isset($shoe) && $shoe->delivery == '5+ days') selected @elseif(old('delivery') == '5+ days') selected @endif>5+ days</option>
                </select>
            </div>
                <div class="form-group col-sm-6 col-xs-12  no-padding">
                    <label for="" class="grey">Minimum Offer Price (Optional)</label>
                    <div class="input-symbol"> <span class="grey">&pound;</span>
                        <input type="number" name="min_offer" class="form-control cl-form-control" value="@if(isset($shoe)){{$shoe->min_offer}}@else{{ old('min_offer') }}@endif" placeholder="Type amount here"/>
                    </div>
                </div>
            <div class="pull-right col-sm-6 col-xs-12 no-padding-xs">
                <label for="" class="grey">Condition</label>
                <input value="@if(isset($shoe)){{$shoe->condition}}@else{{ old('condition')?: 4 }}@endif" type="number" step="1" name="condition" class="rating form-control" data-size="sm" required>
            </div>
            @endif
            <div class="form-group"> <input type="submit" value="@if(isset($shoe)) Save @else Upload @endif" class="upload_btn white green-btn col-sm-12 col-xs-12 btn margin-top-1"/></div>
            {{--</form>--}}
        </div>
    </form>
</div>