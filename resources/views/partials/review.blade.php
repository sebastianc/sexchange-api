<div id="review" class="pop-up white-bg hidden">
    <a class="close pull-right"></a>
    <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 white-bg">
        <div class="col-xs-12 col-sm-3 col-md-2 no-padding">
            <div class="img-circle margin-top-1 img-responsive profile-img" id="review-shoe-image"></div>
        </div>
        <div class="pull-left col-xs-12 col-sm-9">
            <h2>Review</h2>
            <div class="col-sm-12 no-padding margin-bottom-1">
                <div class="pull-left grey" id="review-shoe-username"></div>
            </div>
        </div>
        <div class="col-sm-12 margin-bottom-1 no-padding">
            <h4 class="col-sm-12 text-left">Rate the seller and how they acted throughout this sale</h4>
        </div>
        <div class="col-sm-12">
            <form action="{{ url('account/transactions/review') }}" method="post" id="review-form">
                {{ csrf_field() }}
                <input type="hidden" name="shoe_id" id="review-shoe-id">
                <div class="form-group">
                    <input name="rating" type="number" class="rating" min="0" max="5" step="1" data-size="sm">
                </div>
                <div class="form-group">
                    <label for="review-text" class="grey">What was the service like?</label>
                    <textarea name="text" id="review-text" class="form-control cl-form-control" rows="10" placeholder="Type Here"></textarea>
                </div>
                <input type="submit" class="btn sole-btn red-btn col-xs-12" value="Submit">
            </form>
        </div>
    </div>
</div>