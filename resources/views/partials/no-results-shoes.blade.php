<div class="col-sm-12 text-center no_results">
    <img src="{{asset('assets/images/shoe.png')}}" alt="" class="img-responsive">
    <h4 class="grey">We Couldn't find any shoes matching "{{session('term')}}" </h4>
    <p class="grey">Try searching for <a href="{{$user_url}}">users</a> or something else?</p>
</div>
