<div class="search-user-box">
    <a href="/profile/{{$user->username}}">
        <div class="col-xs-2 col-sm-4 no-padding">
            <div style="background-image: url('{{$user->image}}')" class="img-responsive img-circle profile-img">
            </div>
        </div>
        <div class="pull-left col-xs-7 col-sm-8">
            <span class="dark-grey">{{$user->username}}</span>
            <div class="col-sm-12 no-padding margin-bottom-1">
                <div class="pull-left margin-right-1 margin-top-quarter grey">
                    <img src="{{asset('/assets/images/location_icon.png')}}" alt="location icon" class="pull-left location-icon"/>
                    Manchester
                </div>
                <div class="pull-left hidden-xs margin-top-quarter">
                    <input value="4" type="hidden" class="rating" data-size="s" disabled/>
                </div>
            </div>
        </div>
    </a>
    @include('partials.follow', ['user' => $user])
</div>