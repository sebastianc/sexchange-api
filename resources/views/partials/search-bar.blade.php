
<div id="search" class="pop-up hidden white-bg search_pp" >
    <div class="search_arrow"></div>
    <a class="close pull-right"></a>
    <div id="search_inner" class="inner-hide-scroll">
        <div class="height10 margin_bottom_20"></div>
        <div class="col-sm-12 text-center grey middle-border-title hide_sm"> <span id="search_title" class="srch_tl">Type in your search and press enter</span></div>
        <div class="height20"></div>
        <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin_bottom_20">
            <form method="get" id="searchform" class="searchform" action="/search" role="search">
                <fieldset>
                    <div class="form-group">
                        <!--<input id="input_search" class="form-control search-form-control" type="text" name="search" placeholder="Search items" > -->
                        <input type="search" class="form-control search-form-control field text-center" name="s" value="" id="s" placeholder="Search items" autofocus/>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="height20"></div>

    </div>
</div>


