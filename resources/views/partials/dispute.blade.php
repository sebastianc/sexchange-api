<div id="dispute" class="pop-up white-bg hidden">
    <div class="col-sm-12 margin-bottom-1 no-padding">
        <h4 class="col-sm-12 text-left margin-bottom-1">Make a Dispute</h4>
    </div>
    <div class="col-sm-12 margin-bottom-1 no-padding">
        <p class="col-sm-12 text-left grey">Have you got an issue with this shoe? Let us know what's up and we'll do everything we can to help you. Fill in your details below.</p>
    </div>
    <div class="col-sm-12 margin-top-1">
        <form action="{{ url('account/transactions/dispute') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="shoe_id" id="dispute-shoe-id"/>
            <div class="form-group">
                <label for="dispute-message" class="grey">What is wrong?</label>
                <textarea name="message" id="dispute-message" class="form-control cl-form-control" rows="10" placeholder="Type Here"></textarea>
            </div>
            <input type="submit" class="btn sole-btn red-btn col-xs-12" value="Send" />
        </form>
    </div>
</div>