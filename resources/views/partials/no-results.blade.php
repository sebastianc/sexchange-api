<div class="col-sm-12 text-center no_results">
    <img src="{{asset('assets/images/shoe.png')}}" alt="" class="img-responsive">
    <h4 class="grey">{{$text}}</h4>
    @if (isset($sub_text))
        <p class="grey">{!! $sub_text  !!}</p>
    @endif
</div>