<li>
    <article class="col-xs-6 col-sm-4 col-md-3 products">
        <figure class="featured-thumbnail thumbnail product-box "> <a href="/shoes/{{$shoe->slug}}">
            <div class="mk-image-overlay">
            </div>
            <img width="300" height="181" src="{{$shoe->images->first()->medium()}}" class="" alt=""> </a>
            @if ($shoe->wanted)<span class="wanted-badge btn sole-btn bordered-red-btn white-bg">Wanted</span>@endif
        </figure>
        <div class="prod-info">
            <div class="pricebar cf">
                <h2>
                    <a href="" class="prod_title_holder">
                        <div class="prod_title release_dates_title">{{$shoe->name}}</div>
                        @if (!$shoe->wanted) <span class="price pull-right red">£{{$shoe->cost}}</span> @endif
                    </a>
                </h2>
                <a href=""><span class="prod_size grey pull-left"> @if(isset($shoe->sizes)) {{$shoe->sizes->description}} @endif</span></a>
            </div>
        </div>
    </article>
</li>


