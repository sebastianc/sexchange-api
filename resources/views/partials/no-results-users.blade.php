<div class="col-sm-12 text-center no_results">
    <img src="{{asset('assets/images/shoe.png')}}" alt="" class="img-responsive">
    <h4 class="grey">We Couldn't find any users matching "{{session('term')}}" </h4>
    <p class="grey">Try searching for <a href="{{$shoe_url}}">shoes</a> or something else?</p>
</div>