
<!doctype html>
<html lang="en">

    <head>
        <title>@yield('title')Sole Marketplace</title>

        <link rel="shortcut icon" href="{{ url('favicon.ico') }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{--Meta OG tags--}}
        <meta property="og:site_name" content="The Sole Marketplace">
        @yield('meta')

        {{-- TODO: Remove no follow --}}
        <meta NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

        @if (Request::is('/'))
            {{-- TODO: UPDATE META --}}
            <meta name="description" content="Sole Marketplace">
            <meta name="keywords" content="Sole Marketplace">
        @endif

        @include('assets.styles')

    </head>

    <body>
        <div class="page-wrapper">
            <header id="header" role="banner">
                <div class="container">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header"> <a id="search-link" class=" grey pop-up-link visible-mobile search-top pull-right"></a>
                                <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                <div class="navbar-brand logo"> <a href="/"></a></div>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <nav id="m-site-navigation" class="main-nav hidden" role="navigation">
                                    <div class="menu_arrow"></div>
                                    <div class="navbar_inner">
                                        <ul class="nav navbar-nav navbar-right sole-login">
                                            <li> <a href="/" class="col-sm-12" >Home</a> </li>
                                            <li> <a href="/" class="col-sm-12" >Buy</a> </li>
                                            <li> <a href="/wanted" class="col-sm-12" >Wanted</a> </li>
                                            @if(Auth::check())
                                                <li> <a href="notifications.php" class="col-sm-12" >Notifications</a> </li>
                                                <li> <a href="/upload/shoe" class="col-sm-12" >Upload Shoe</a> </li>
                                                <li role="separator" class="col-sm-12 border-bottom margin-bottom-1 margin-top-1"></li>
                                                <li><a>Profile</a></li>
                                                <li class="col-sm-12 no-padding-right-xs"> <a href="{{ url('/account') }}" class="col-sm-12" >Account</a> </li>
                                                <li class="col-sm-12 no-padding-right-xs"> <a href="{{ url('/account/transactions/purchases') }}" class="col-sm-12" >Transactions</a> </li>
                                                @if(Auth::user()->stripeAccount)
                                                    <li class="col-sm-12 no-padding-right-xs"> <a href="{{ url('/payment/edit-bank') }}" class="col-sm-12" >Edit Bank Details</a></li>
                                                @endif
                                                <li class="col-sm-12 no-padding-right-xs"> <a href="{{ url('/profile/' . Auth::user()->username . '/reviews') }}" class="col-sm-12" >My Reviews</a> </li>
                                                <li class="col-sm-12 no-padding-right-xs"> <a href="{{ url('/account/offers/received') }}" class="col-sm-12" >Offers</a> </li>
                                                <li class="col-sm-12 no-padding-right-xs"> <a href="/logout" class="col-sm-12" >Logout</a> </li>
                                            @else
                                                <li> <a id="register-link" href="#" class="pop-up-link">Login/Register</a> </li>

                                            @endif
                                        </ul>
                                    </div>
                                </nav>

                                <nav id="site-navigation" class="main-nav hidden-xs hidden-sm" role="navigation">
                                    <div class="navbar_inner">
                                        <ul class="nav navbar-nav navbar-right sole-login col-sm-6">
                                            @if(Auth::check())
                                                <li class="pull-right">
                                                    <div class="user_box pull-right">
                                                        <a href="#" onclick="return false" class="popover-exclusive pull-right" data-toggle="popover" data-trigger="click" data-content="<ul><li class='border-bottom'><a href='{{ url('/account') }}'>Account</a></li><li><a href='{{ url('account/transactions/purchases') }}'>Transactions</a></li>@if(Auth::user()->stripeAccount)<li><a href='{{ url('/payment/edit-bank') }}'>Edit Bank Account</a></li>@endif<li><a href='{{ url('/profile/' . Auth::user()->username . '/reviews') }}'>My Reviews</a></li><li class='border-bottom'><a href='{{ url('account/offers/received') }}'>Offers</a></li><li><a href='/logout'>Logout</a></li></ul>" data-placement="bottom">
                                                            <div data-toggle="popover" style="background-image: url('{{Auth::user()->thumb()}}')" class="profile-image profile-image-header pull-right"></div>
                                                            <span data-toggle="popover">{{Auth::user()->username}}</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            @else
                                                <li class="pull-right"> <a id="register-link" class="btn sole-btn bordered-btn pop-up-link" >Login/Register</a> </li>
                                            @endif

                                            <li class="pull-right"> <a href="{{url('upload/shoe')}}" class="btn sole-btn green-btn white" >Upload Shoe</a> </li>
                                            @if (Auth::check())
                                                <li class="pull-right big-popover"> <a data-toggle="popover" class="popover-exclusive header-notifications" data-trigger="click" data-content="
                                                    @forelse ($header_notifications as $notification)
                                                        <div class='border-bottom col-sm-12 no-padding'>
                                                            <div class='col-sm-2 no-padding hidden-xs notification-img'>
                                                                <div style='background: url({{ $notification->message['image'] }})' class='{{ $notification->imageClass }} pull-left'></div>
                                                            </div>
                                                            <div class='col-sm-10 margin-bottom-1'>
                                                                <p>{{ $notification->message['message'] }}
                                                                <small class='grey col-sm-12 no-padding'><img src='{{ $notification->icon }}' alt='notification' class='pull-left notification-icon'> {{ $notification->time_ago }}</small></p>
                                                            </div>
                                                        </div>
                                                    @empty
                                                        <div class='margin-top-1 margin-bottom-1 text-center grey'>You've got no notifications at the moment.</div>
                                                    @endforelse
                                                    <a href='{{ url('notifications') }}' class='col-sm-12 btn sole-btn grey margin-bottom-1 margin-top-1'>See All Notifications</a>

                                                " data-placement="bottom">@if ($header_notification_count != 0)<div class="notification_badge">{{ $header_notification_count }}</div>@endif Notifications</a> </li>
                                            @endif
                                            <li class="pull-right"> <a id="search-link" class="search-top grey pop-up-link">Search</a> </li>
                                        </ul>
                                    </div>
                                </nav>
                                <!-- #site-navigation -->

                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                    <div class="clear"></div>
                </div>
            </header>
            <!-- #masthead -->
            <!-- AJAX Loader -->
            <div class="ajax_loader">
                <img src="{{asset('assets/images/loader.svg')}}"/>
            </div>
            <!-- Popups -->
            <div class="box-overlay hidden">
            </div>
            @include('popups.login')
            @include('popups.register')
            @include('popups.errors')
            @include('partials.search-bar')
            @yield('popups')
            @yield('popover')
            <div id="main" class="site-main">
                <div class="container">

                    @yield('content')

                </div>
            </div>
        </div>

        @include('templates.footer')
        @include('assets.scripts')
        @yield('footer_scripts')

    </body>

</html>
