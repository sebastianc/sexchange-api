
<!-- Footer -->

<footer>
    <div class="footer-wrap-border">
        <div class="footer-content border-bottom-dark hidden-xs hidden-sm">
            <div class="container">
                <ul class="footer-navbar nav navbar-nav sole-login col-sm-12">
                    <li class="pull-left no-padding text-left"> <a href="http://thesolesupplier.co.uk/terms/">Terms</a> </li>
                    <li class="pull-left hidden-xs no-padding text-center"> <a href="http://thesolesupplier.co.uk/privacy/">Privacy</a> </li>
                    <li class="pull-left hidden-xs no-padding text-center"> <a href="http://thesolesupplier.co.uk/about/">About</a> </li>
                    <li class="pull-left hidden-xs no-padding text-center"> <a href="/faq">FAQ</a> </li>
                    <li class="pull-left hidden-xs no-padding text-center"> <a href="/support">Support</a> </li>
                    <ul class="pull-right nav navbar">
                        @if(Auth::check())
                            <li class="pull-right">
                                <div class="user_box pull-right">
                                    <a href="#" onclick="return false" class="popover-exclusive pull-right" data-toggle="popover" data-trigger="click" data-content="<ul><li class='border-bottom'><a href='{{ url('/account') }}'>Account</a></li><li><a href='{{ url('account/transactions/purchases') }}'>Transactions</a></li>@if(Auth::user()->stripeAccount)<li><a href='{{ url('/payment/edit-bank') }}'>Edit Bank Account</a></li>@endif<li><a href='{{ url('/profile/' . Auth::user()->username . '/reviews') }}'>My Reviews</a></li><li class='border-bottom'><a href='{{ url('account/offers/received') }}'>Offers</a></li><li><a href='/logout'>Logout</a></li></ul>" data-placement="bottom">
                                        <div data-toggle="popover" style="background-image: url('{{Auth::user()->thumb()}}')" class="profile-image profile-image-header pull-right"></div>
                                        <span data-toggle="popover">{{Auth::user()->username}}</span>
                                    </a>
                                </div>
                            </li>
                        @else
                            <li class="pull-right"> <a id="register-link" class="btn sole-btn bordered-btn pop-up-link" >Login/Register</a> </li>
                        @endif
                        <li class="pull-right"> <a class="btn sole-btn green-btn white" >Upload Shoe</a> </li>
                        <li class="pull-right"> <a class="" >Notifications</a> </li>
                        <li class="pull-right search_footer"> <a id="search-link-footer" class="search-top grey pop-up-link">Search</a> </li>
                    </ul>
                </ul>
            </div>
        </div>
        <div class="footer-wrap border-bottom-dark">
            <div class="footer-content container margin-bottom-1">
                <ul class="visible-xs visible-sm row mobile-footer-menu">
                    <li><a id="register-link-footer" class="pop-up-link" >Login/Register</a></li>
                    <li><a href="/apps/">Download our Apps</a></li>
                    <li><a href="/terms/">Terms</a></li>
                    <li><a href="/privacy/">Privacy</a></li>
                    <li><a href="/about/">About</a></li>
                </ul>
                <div class="col-md-4 col-sm-4 col-xs-12 no-padding-left hidden-xs hidden-sm">
                    <h6>Trending</h6>
                    <p class="grey">Check out some of the latest most popular sneakers on the market.</p>
                    <!--<figure class="featured-thumbnail thumbnail product-box col-xs-12"> <a href=""><img class="img-responsive center-block product-img col-xs-12 no-padding" src="{{asset('assets/images/shoe_thumb.png')}}"> </a><a href="" class="box_overlay box_overlay_dark">
                            <div class="col-sm-12 text-center btn_holder_middle "> <span class="btn green-btn">View Stockists</span></div>
                        </a> </figure>-->
                </div>
                <div class="col-md-4 col-sm-8 col-xs-12 hidden-xs hidden-sm">
                    <h6>Download our Apps</h6>
                    <p class="grey">Enter your email address and we'll send you the link to download our iPhone or Android App.</p>
                    <div class="col-md-12 col-sm-6 col-xs-12 no-padding margin-bottom-1">
                        <div class="col-xs-2 no-padding"><img src="{{asset('assets/images/sole-red.png')}}" alt="" class="img-responsive"></div>
                        <div class="col-xs-10"> <span>Sole Supplier</span>
                            <p>Keep updated with the latest sneaker release dates and news on your smartphone.</p>
                            <a class="col-xs-6 col-sm-5 no-padding-left" href="//itunes.apple.com/gb/app/sole-supplier/id967172411?mt=8" target="_blank"><img src="{{asset('assets/images/app-store.svg')}}" alt="" class="img-responsive"></a> </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12 no-padding">
                        <div class="col-xs-2 no-padding"><img src="{{asset('assets/images/sole-grey.png')}}" alt="" class="img-responsive"></div>
                        <div class="col-xs-10"> <span>Sole Exchange</span>
                            <p>The UK's leading dedicated sneaker / footwear marketplace available on iOs and Android</p>
                            <a class="col-xs-6 col-sm-5 no-padding-left" href="//itunes.apple.com/gb/app/sole-exchange-buy-sell-footwear/id1034411068?mt=8" target="_blank"><img src="{{asset('assets/images/app-store.svg')}}" alt="" class="img-responsive"></a> <a class="col-xs-6 col-sm-5 no-padding-left" href="//play.google.com/store/apps/details?id=uk.co.thesolesupplier.soleexchange" target="_blank"><img src="{{asset('assets/images/googleplay.png')}}" alt="" class="img-responsive"></a> </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 no-padding-right hidden-xs hidden-sm">
                    <h6>Our Brands</h6>
                    <p class="grey">Check out some of the hottest sneakers right now.</p>
                    <ul class="col-xs-12 no-padding">
                        <li class="cat-item"><a href="http://thesolesupplier.co.uk/release-dates/nike/">Nike</a> </li>
                        <li class="cat-item"><a href="http://thesolesupplier.co.uk/release-dates/adidas/">Adidas</a> </li>
                        <li class="cat-item"><a href="http://thesolesupplier.co.uk/release-dates/asics/">ASICS</a> </li>
                        <li class="cat-item"><a href="http://thesolesupplier.co.uk/release-dates/saucony/">Saucony</a> </li>
                        <li class="cat-item"><a href="http://thesolesupplier.co.uk/release-dates/new-balance/">New Balance</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <span class="visible-xs"></span>
        <div class="footer-wrap bottom-info">
            <div class="footer-content container margin-bottom-1">
                <div class="grey">&copy; All rights Reserved. Sole Supplier &reg; | <b>Telephone </b> 0203 488 0372 | <b>Email </b> <a href="mailto:info@thesolesupplier.co.uk" class="grey">info@thesolesupplier.co.uk</a> | <b>Company Reg No. </b> 09098756 | SEO by SEO Enterprise Limited</div>
            </div>
        </div>
    </div>
    @yield('footer')
</footer>


