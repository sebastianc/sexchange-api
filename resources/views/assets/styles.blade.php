<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/star-rating.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/lightbox.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/galleriffic-2.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.css')}}" />