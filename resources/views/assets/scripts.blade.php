<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-3.0.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.galleriffic.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.opacityrollover.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/dropzone.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/star-rating.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/lightslider.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/mask.jquery.js')}}"></script>
{{--<script type="text/javascript" src="assets/js/lightbox.js"></script>--}}
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
{{--<script src="{{ asset('assets/js/lightbox.js') }}"></script>--}}

<script>

    BASE_URL = '{{ url('/') }}';

    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#register_btn').click(function(e){
            e.preventDefault();
            $('.error_message').fadeOut();
            $('.register_loader').fadeIn();
            var request = $.ajax({
                type: "POST",
                url: '/register',
                dataType: 'json',
                data: {
                    username: $('#username').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    _token: '{{ csrf_token() }}'
                },
                success: function (response) {
                    if(response.status == 'success'){
                        $('.register_loader').fadeOut();
                        $('#register-form').fadeOut(500);
                        $('#upload-pic-form').delay(600).fadeIn();

                    }else{
                        $('.error_message').html(response.error);
                        $('.error_message').fadeIn().delay(4000).fadeOut(900);
                        $('.register_loader').fadeOut();


                    }

                }
            });
        });

        $("#upload_profile_image").on('change', function () {

            if (typeof (FileReader) != "undefined") {

                var image_holder = $("#preview_upload");
                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function (e) {
                    $(image_holder).css({'background-image' : 'url('+e.target.result+')', 'background-size' : 'cover'});

                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
                $('#save-pic').removeAttr('disabled');
            } else {
                alert("This browser does not support FileReader.");
            }
        });

        $('.header-notifications').click(function(e) {
            $.post(BASE_URL + '/notifications/viewed');
            $('.notification_badge').hide();
        })
    });

    $('#preview_upload').click(function(){
        $('#upload_profile_image').click();
    })
</script>