<h1>Sole Exchange</h1>
<h2>A dispute has been raised for shoe #{{ $dispute->purchase->shoe->id }} on {{ $dispute->created_at->format('d/m/Y') }}</h2>
<h3>Shoe information</h3>
<ul>
    <li><b>Name:</b> {{ $dispute->purchase->shoe->name }}</li>
    <li><b>Description:</b> {{ $dispute->purchase->shoe->sizes->description }}</li>
    <li><b>Price</b> £{{ number_format($dispute->purchase->price, 2) }}</li>
    <li><b>Transaction ID:</b> {{ $dispute->purchase->stripeTransaction->charge_id }}</li>
</ul>
<h3>Seller information</h3>
<ul>
    <li><b>Username:</b> {{ $dispute->purchase->shoe->sellerUser->username }}</li>
    <li><b>Email:</b> {{ $dispute->purchase->shoe->sellerUser->email }}</li>
    <li><b>Rating:</b> {{ $dispute->purchase->shoe->sellerUser->average_rating }}/5</li>
</ul>
<h3>Buyer information</h3>
<ul>
    <li><b>Username:</b> {{ $dispute->purchase->user->username }}</li>
    <li><b>Email:</b> {{ $dispute->purchase->user->email }}</li>
    <li><b>Rating:</b> {{ $dispute->purchase->user->average_rating }}/5</li>
</ul>
<h3>Dispute</h3>
<p>{{ $dispute->message }}</p>