jQuery(document).ready(function($){
	'use strict';

	$('.follow_user_button').click(function(e){
		var userId = $(this).data('user-id');

		var update = function(ref, following) {
			if (following) {
				$(ref).removeClass('red-btn');
				$(ref).addClass('bordered-red-btn');
				$(ref).text('Follow');
			} else {
				$(ref).addClass('red-btn');
				$(ref).removeClass('bordered-red-btn');
				$(ref).text('Following');
			}
		};

		update(this, $(this).hasClass('red-btn'));

		$.post(BASE_URL + '/profile/follow/' + userId, function(data){
			update(this, data.action == 'followed');
		});
	});

	$('.error_message.hide-after-delay, .success_message.hide-after-delay').fadeIn(450).delay(4000).fadeOut(900);

	$('.rating').click(function() {
		var link = $(this).parent().parent();
		var href = link.attr('href');

		if (typeof href !== typeof undefined && href !== false) {
			window.location = link.attr('href');
		}
	});

	jQuery(".menu-toggle").click(function(){

		jQuery("#m-site-navigation").slideToggle("slow");

	});

	jQuery(window).resize(function() {

		jQuery("#m-site-navigation").hide();

	});
	// check if mac or pc
	if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('LINUX X86_64') >= 0 ) {
		$("body").addClass("mac");
	} else {
		$("body").addClass("pc");
	}
	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];
	if (!!navigator.platform) {
		while (iDevices.length) {
			if (navigator.platform === iDevices.pop()){ $("body").addClass("mac");}
		}
	}

	// pop up windows
	$(".close").click(function(){
		$(this).parent().toggleClass("hidden");
		$(".box-overlay").toggleClass("hidden");
		var form = $(this).parent().find( $("form") );
		if(form.attr('id') !== 'colour_select'){
			form.trigger("reset");
		}
	});
	$(".box-overlay").click(function(){
		if(window.location.href.indexOf("profile") > -1) {
			$(".app-banner-container").addClass("hidden");
		}
		$(this).toggleClass("hidden");
		$(".pop-up").addClass("hidden");
		var form = $(this).parent().find( $("form") );
		form.trigger("reset");
	});
	$(".pop-up-link").click(function(){
		$(".pop-up").each(function( index ) {
			$(this).addClass("hidden");
		});
		$(".box-overlay").addClass("hidden");
		var id = $(this).attr("id");
		if(id.indexOf("-footer") > 0){
			id = id.replace("-link-footer","");
		}
		else{
			id = id.replace("-link","");
		}
		if($(this).attr('id') === 'review-link' || $(this).attr('id') === 'colour-link'){
			$(".box-overlay").addClass("box-overlay-dark");
		}
		$(".box-overlay").toggleClass("hidden");
		$("#"+id).toggleClass("hidden");
		$(":input:enabled:visible:first").focus();

	});
	$('.review-link').click(function(e){
		$('#review-shoe-username').text($(this).data('username'));
		$('#review-shoe-id').val($(this).data('shoe-id'));
		$('#review-shoe-image').css('background-image', 'url(' + $(this).data('image') + ')');
	});

	$('.dispute-link').click(function(e){
		e.preventDefault();
		$(".box-overlay").addClass("box-overlay-dark");
		$(".box-overlay").toggleClass("hidden");
		$('#dispute-shoe-id').val($(this).data('shoe-id'));
		$('#dispute').removeClass('hidden');
	});

	// register
	$("#register").click(function(){
		// $('#register-form').toggleClass("hidden");
		// $("#upload-pic-form").toggleClass("hidden");
	});

	// search pop up
	$("#input_search").on( 'keydown',function(e) {
		if(e.which === 13) {
			$("#input_search").slideUp();
			e.preventDefault();
			$("#search_title").addClass("xxl-font");
			$("#search_title").html($("#input_search").val());
			$("#results_no").fadeIn();
			$("#results").fadeIn();
		}
	});

	// end of scrolling
	jQuery(function($) {
		$('#search_inner').on('scroll', function() {
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				$("#view_more_results").fadeIn();
			}
		});
	});

	//
	var ajaxUrl = "<?php echo admin_url('admin-ajax.php', null); ?>";
	var page = 1; // What page we are on.
	var ppp = 4; // Post per page

	jQuery(".more_posts").on("click",function(){ // When btn is pressed.
		jQuery(".more_posts").attr("disabled",true); // Disable the button, temp.
		jQuery.post(ajaxUrl, {
			action:"more_post_ajax",
			offset: (page * ppp) + 1,
			ppp: ppp
		}).success(function(posts){
			page++;
			jQuery("#content").append(posts);
			jQuery(".more_posts").attr("disabled",false);
		});

	});

	// Product Edit from profile page
	jQuery(".edit_btn").on("click",function(){ // When btn is pressed.
		var overlay = jQuery(this).parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_edit").toggle();
	});
	jQuery(".cancel_edit").on("click",function(){
		var overlay = jQuery(this).parent().parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_edit").toggle();
	});

	jQuery(".mark_unsold_btn").on("click",function(){ // When btn is pressed.
		var overlay = jQuery(this).parent().parent();
		overlay.toggle();
		var confirmOverlay = overlay.next();
		confirmOverlay.show();
		confirmOverlay.find('.box_overlay_mark_sold').hide();
		var shoeId = $(this).data('shoe-id');
		$.post(BASE_URL + '/shoes/mark-sold/' + shoeId);
		createStatusMessage('Shoe marked as available.', true);
	});

	// Product Mark as Sold from profile page
	jQuery(".mark_sold_btn").on("click",function(){ // When btn is pressed.
		var overlay = jQuery(this).parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_mark_sold").toggle();
	});
	jQuery(".mark_sold_perm").on("click",function(){ // When btn is pressed.

	});
	jQuery(".cancel_mark_sold").on("click",function(){
		var overlay = jQuery(this).parent().parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_mark_sold").toggle();
	});

	jQuery(".delete_mark_sold").on("click",function(e){ // When btn is pressed.
		e.preventDefault();
		var overlay = jQuery(this).parent().parent().parent();
		var soldOverlay = overlay.prev();
		soldOverlay.show();
		var box = overlay.parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_mark_sold").toggle();
		overlay.hide();
		var shoeId = $(this).data('shoe-id');
		$.post(BASE_URL + '/shoes/mark-sold/' + shoeId);
		createStatusMessage('Shoe marked as sold.', true)
	});

	// Product Delete from profile page
	jQuery(".delete_btn").on("click",function(){ // When btn is pressed.
		var overlay = jQuery(this).parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_delete").toggle();

	});
	jQuery(".delete_perm").on("click",function(e){ // When btn is pressed.
		e.preventDefault();
		var overlay = jQuery(this).parent().parent().parent();
		var box = overlay.parent();
		var post_box = box.parent();
		post_box.hide();
		var shoeId = $(this).data('shoe-id');
		$.post(BASE_URL + '/shoes/delete/' + shoeId);
		createStatusMessage('Shoe deleted.', true)
	});
	jQuery(".cancel_del").on("click",function(){
		var overlay = jQuery(this).parent().parent().parent();
		overlay.find("div.overlay-buttons-holder").toggle();
		overlay.find("div.box_overlay_delete").toggle();
	});

	// pop over
	$('[data-toggle="popover"]').popover({html: true});
	//Only allow one popover to open at a time
	// $('.popover-exclusive').mousedown(function(){
	// 	$('.popover-exclusive').not(this).popover('hide');
	// 	$('.popover').hide();
	// 	console.log('called');
	// });
	// //dismiss popover
	// $('body').on('click', function (e) {
	// 	if ($(e.target).data('toggle') !== 'popover' && $(e.target).parents('.popover.in').length === 0) {
	// 		$('[data-toggle="popover"]').popover('hide');
	// 	}
	// });

	$('body').on('click', function (e) {
		$('[data-toggle="popover"]').each(function () {
			//Only do this for all popovers other than the current one that cause this event
			if (!($(this).is(e.target) || $(this).has(e.target).length > 0)
				&& $(this).siblings('.popover').length !== 0
				&& $(this).siblings('.popover').has(e.target).length === 0)
			{
				//Remove the popover element from the DOM
				$(this).siblings('.popover').remove();
				$(this).popover('hide');
			}
		});
	});
	// collapse filters
	if ($(window).width() < 480) {
		$('.filter-option ul').css('display', 'none');
	}

	// show active tab on reload
	if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

	// remember the hash in the URL without jumping
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		if(history.pushState) {
			history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
		} else {
			location.hash = '#'+$(e.target).attr('href').substr(1);
		}
	});

	//
	$(".collapse-btn").click(function(){
		if ($(this).text() === "View more"){
			//$(this).text("View less");
		}
		else {
			//$(this).text("View more");
		}
	});

	// select
	select_styling();

	// Auto width select
	if(window.location.href.indexOf("search_results") > -1 || window.location.href.indexOf("index") > -1) {

		var new_width = ($('.select_wrapper span').html().length * 9) + "px";
		if($('.select_wrapper span').html().length === 6){
			new_width = ($('.select_wrapper span').html().length * 14) + "px";
		}
		if($('.select_wrapper span').html().length === 10){
			new_width = ($('.select_wrapper span').html().length * 11) + "px";
		}
		if($('.select_wrapper span').html().length === 5 || $('.select_wrapper span').html().length === 7){
			new_width = ($('.select_wrapper span').html().length * 13) + "px";
		}
		$('.select_wrapper span').css('width', new_width);

		$('.select_inner li').click( function(){
			var new_width = ($('.select_wrapper span').html().length * 9) + "px";
			if($('.select_wrapper span').html().length === 6){
				new_width = ($('.select_wrapper span').html().length * 14) + "px";
			}
			if($('.select_wrapper span').html().length === 10){
				new_width = ($('.select_wrapper span').html().length * 11) + "px";
			}
			if($('.select_wrapper span').html().length === 5 || $('.select_wrapper span').html().length === 7){
				new_width = ($('.select_wrapper span').html().length * 13) + "px";
			}
			$('.select_wrapper span').css('width', new_width);
		});
	}

	// Filters
	$(".filter").click( function () {
		var filterText = $(this).attr('data-filter').replace('.','');
		$("#search-results-gallery").hide();
		$("#search-results-gallery li" ).each(function() {
			var article = $(this).find('article');
			if (article.hasClass(filterText)){
				$(this).show();
			}
			else{
				$(this).hide();
			}
		});
		var count=0;
		$(".filter").each(function() {
			if ($(this).parent().hasClass('checked')){
				count++;
			}
		});
		if(count === 0){
			$("#search-results-gallery li" ).each(function() {
				$(this).show();
			});
		}

		$("#search-results-gallery").fadeIn(1000);
	});
	$(".reset_btn").click( function () {
		$("#search-results-gallery").hide();
		$(".filter").each(function() {
			$(this).parent().removeClass('checked');
		});
		$("#search-results-gallery li" ).each(function() {
			$(this).show();
		});

		$("#search-results-gallery").fadeIn(1000);
	});
	if($(window).width() <= 376) {
		jQuery('#wantedh4').addClass('filter_collapsed');
		jQuery('#wantedul').css('display', 'none');
		jQuery('#searchh4').addClass('filter_collapsed');
		jQuery('#searchul').css('display', 'none');
		jQuery('#sizeh4').addClass('filter_collapsed');
		jQuery('#sizeul').css('display', 'none');
		jQuery('#styleh4').addClass('filter_collapsed');
		jQuery('#styleul').css('display', 'none');
		jQuery('#priceh4').addClass('filter_collapsed');
		jQuery('#priceul').css('display', 'none');
		jQuery('#brandh4').addClass('filter_collapsed');
		jQuery('#brandul').css('display', 'none');
		jQuery('#colourh4').addClass('filter_collapsed');
		jQuery('#colourul').css('display', 'none');
		jQuery('#conditionh4').addClass('filter_collapsed');
		jQuery('#conditiondiv').css('display', 'none');


		$(".info-collapse" ).each(function() {
			$(this).attr('aria-expanded','false');
			$(this).addClass('collapse');
		});
	}

	// Expand textarea
	$('textarea.expand').focus(function () {
		$(this).animate({ height: "4em" }, 500).promise().done(function () {
			$('#comment_input').attr('data-toggle', false);
		});

	});
	$('textarea.expand').focusout(function () {
		// $(this).animate({ height: "2.2em" }, 500);
		// $('#demo').removeClass('in');
		// $('#demo').addClass('collapse');
	});



	// mobile menu
	$('#m-site-navigation ul.navbar-nav li a').click( function () {
		// location.reload();
	});

	// slider
	// Initially set opacity on thumbs and add  additional styling for hover effect on thumbs
	var onMouseOutOpacity = 0.67;
	$('#thumbs ul.thumbs li').opacityrollover({
		mouseOutOpacity:   onMouseOutOpacity,
		mouseOverOpacity:  1.0,
		fadeSpeed:         'fast',
		exemptionSelector: '.selected'
	});
	// Initialize Advanced Galleriffic Gallery
	var gallery = $('#thumbs').galleriffic({
		delay:                     2500,
		numThumbs:                 9,
		preloadAhead:              10,
		enableTopPager:            false,
		enableBottomPager:         false,
		maxPagesToShow:            7,
		imageContainerSel:         '#slideshow',
		controlsContainerSel:      '#controls',
		captionContainerSel:       '#caption',
		loadingContainerSel:       '#loading',
		renderSSControls:          true,
		renderNavControls:         true,
		playLinkText:              'Play Slideshow',
		pauseLinkText:             'Pause Slideshow',
		prevLinkText:              '',
		nextLinkText:              '',
		enableHistory:             false,
		autoStart:                 false,
		syncTransitions:           true,
		defaultTransitionDuration: 900,
		onSlideChange:             function(prevIndex, nextIndex) {
			// 'this' refers to the gallery, which is an extension of $('#thumbs')
			this.find('ul.thumbs').children()
				.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
				.eq(nextIndex).fadeTo('fast', 1.0);
		},
		onPageTransitionOut:       function(callback) {
			this.fadeTo('fast', 0.0, callback);
		},
		onPageTransitionIn:        function() {
			this.fadeTo('fast', 1.0);
		}
	});

	//dynamic selection
	document.getElementById('dynamic_select').onchange = function () {
		var selectedValue = this.options[this.selectedIndex].value;
		$(".tab-pane").each(function() {
			$(this).removeClass('in');
			$(this).removeClass('active');
		});
		$("#"+selectedValue).addClass('in');
		$("#"+selectedValue).addClass('active');
	};


});


document.addEventListener("DOMContentLoaded", function() {
	"use strict";

	// Toggle checkbox checked
	var inputs = document.getElementsByTagName('input');
	for(var i = 0; i < inputs.length; i++) {
		if(inputs[i].type.toLowerCase() === 'checkbox') {
			inputs[i].addEventListener("click",toggleChecked, false);
		}
	}
});

// Toggle checked
function toggleChecked() {
	"use strict";
	var el = this.parentNode;
	if (el.classList.contains('checked')){
		el.classList.remove('checked');
	}
	else{
		el.classList.add('checked');
	}
}
// select styling
function select_styling(){
	'use strict';
	$('select.style-select').wrap('<div class="select_wrapper"></div>');
	var title = $('select.style-select').parent().find(':selected').text();
	if(title.indexOf("Sort") > -1){
		if($(window).width() <= 376){
			title = "Sort";
		}
		else{
			title = "Sort Results";
		}
	}
	$('select.style-select').parent().prepend('<span>'+ title +'</span>');
	$('select.style-select').parent().children('span').width($('select').width());
	$('select.style-select').css('display', 'none');
	$('select.style-select').parent().append('<div class="menu_arrow hidden"></div><ul class="select_inner"></ul>');
	$('select.style-select').children().each(function(){
		var opttext = $(this).text();
		var optval = $(this).val();
		$('select.style-select').parent().children('.select_inner').append('<li id="' + optval + '">' + opttext + '</li>');
	});

	$('select.style-select').parent().find('li').on('click', function (){
		var cur = $(this).attr('id');
		$('select.style-select').parent().children('span').text($(this).text());
		$('select.style-select').children().removeAttr('selected');
		$('select.style-select').children('[value="'+cur+'"]').attr('selected','selected');
		$('select.style-select').change();
	});
	$('select.style-select').parent().on('click', function (){
		$(this).find('ul').slideToggle('fast', function() {
			// Animation complete.
			$('.menu_arrow').toggleClass('hidden');
		});
	});
	$('.nav-tabs li a').on('click', function (){
		$('ul.select_inner').slideUp('fast', function() {
			// Animation complete.
			$('.menu_arrow').addClass('hidden');
		});
	});
}

jQuery('#wantedh4').click(function () {
	jQuery('#wantedul').slideToggle();
	if(jQuery('#wantedh4').hasClass('filter_collapsed')){
		jQuery('#wantedh4').removeClass('filter_collapsed');
	}else{
		jQuery('#wantedh4').addClass('filter_collapsed');
	}
});


jQuery('#searchh4').click(function () {
	jQuery('#searchul').slideToggle();
	if(jQuery('#searchh4').hasClass('filter_collapsed')){
		jQuery('#searchh4').removeClass('filter_collapsed');
	}else{
		jQuery('#searchh4').addClass('filter_collapsed');
	}
});

jQuery('#sizeh4').click(function () {
	jQuery('#sizeul').slideToggle();
	if(jQuery('#sizeh4').hasClass('filter_collapsed')){
		jQuery('#sizeh4').removeClass('filter_collapsed');
	}else{
		jQuery('#sizeh4').addClass('filter_collapsed');
	}
});

jQuery('#styleh4').click(function () {
	jQuery('#styleul').slideToggle();
	if(jQuery('#styleh4').hasClass('filter_collapsed')){
		jQuery('#styleh4').removeClass('filter_collapsed');
	}else{
		jQuery('#styleh4').addClass('filter_collapsed');
	}
});

jQuery('#priceh4').click(function () {
	jQuery('#priceul').slideToggle();
	if(jQuery('#priceh4').hasClass('filter_collapsed')){
		jQuery('#priceh4').removeClass('filter_collapsed');
	}else{
		jQuery('#priceh4').addClass('filter_collapsed');
	}
});

jQuery('#brandh4').click(function () {
	jQuery('#brandul').slideToggle();
	if(jQuery('#brandh4').hasClass('filter_collapsed')){
		jQuery('#brandh4').removeClass('filter_collapsed');
	}else{
		jQuery('#brandh4').addClass('filter_collapsed');
	}
});

jQuery('#colourh4').click(function () {
	jQuery('#colourul').slideToggle();
	if(jQuery('#colourh4').hasClass('filter_collapsed')){
		jQuery('#colourh4').removeClass('filter_collapsed');
	}else{
		jQuery('#colourh4').addClass('filter_collapsed');
	}
});

jQuery('#conditionh4').click(function () {
	jQuery('#conditiondiv').slideToggle();
	if(jQuery('#conditionh4').hasClass('filter_collapsed')){
		jQuery('#conditionh4').removeClass('filter_collapsed');
	}else{
		jQuery('#conditionh4').addClass('filter_collapsed');
	}
});

jQuery('.set_reminder').click(function(){
	jQuery(this).toggleClass('.reminder_set');
});

function createStatusMessage(message, success) {
	var className = success ? 'success_message' : 'error_message';
	var alert = $('<div></div>').text(message).addClass(className);
	$('body').append(alert);
	alert.fadeIn(450).delay(4000).fadeOut(900)
}

function share(url) {
	var winTop = (screen.height / 2) - (350 / 2);
	var winLeft = (screen.width / 2) - (520 / 2);
	window.open( url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=520,height=350');
}